<?php

/* TwigBundle:Exception:error.html.twig */
class __TwigTemplate_418ca1e6ee2ee099b28daaafd50885ad77ba2fbd30e6544562716b1f833078fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content_content($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"jumbotron\">
        <h1>Error: <small>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "html", null, true);
        echo " </small></h1>
        <i class=\"fa fa-times-circle fa-6\"></i> Error: \"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "html", null, true);
        echo "\" <h4><strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "message"), "html", null, true);
        echo "</strong></h4>
        <div class=\"block-separator col-md-12\"></div>
        <h3>Regresar A <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn btn-success btn-sm\">Inicio</a></h3>
    </div>
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  38 => 7,  34 => 6,  31 => 5,  28 => 4,);
    }
}
