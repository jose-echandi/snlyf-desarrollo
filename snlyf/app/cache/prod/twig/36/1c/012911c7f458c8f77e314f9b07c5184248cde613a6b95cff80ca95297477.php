<?php

/* MopaBootstrapBundle::base.html.twig */
class __TwigTemplate_361c012911c7f458c8f77e314f9b07c5184248cde613a6b95cff80ca95297477 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'html_tag' => array($this, 'block_html_tag'),
            'head' => array($this, 'block_head'),
            'head_style' => array($this, 'block_head_style'),
            'head_script' => array($this, 'block_head_script'),
            'title' => array($this, 'block_title'),
            'head_bottom' => array($this, 'block_head_bottom'),
            'body_tag' => array($this, 'block_body_tag'),
            'body_start' => array($this, 'block_body_start'),
            'body' => array($this, 'block_body'),
            'navbar' => array($this, 'block_navbar'),
            'container' => array($this, 'block_container'),
            'container_div_start' => array($this, 'block_container_div_start'),
            'container_class' => array($this, 'block_container_class'),
            'header' => array($this, 'block_header'),
            'content_div_start' => array($this, 'block_content_div_start'),
            'page_header' => array($this, 'block_page_header'),
            'flashes' => array($this, 'block_flashes'),
            'content_row' => array($this, 'block_content_row'),
            'content' => array($this, 'block_content'),
            'content_content' => array($this, 'block_content_content'),
            'content_sidebar' => array($this, 'block_content_sidebar'),
            'content_div_end' => array($this, 'block_content_div_end'),
            'footer_tag_start' => array($this, 'block_footer_tag_start'),
            'footer' => array($this, 'block_footer'),
            'footer_tag_end' => array($this, 'block_footer_tag_end'),
            'container_div_end' => array($this, 'block_container_div_end'),
            'body_end_before_js' => array($this, 'block_body_end_before_js'),
            'script_base' => array($this, 'block_script_base'),
            'foot_script' => array($this, 'block_foot_script'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
            'body_end' => array($this, 'block_body_end'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["__internal_e651dead39d546c807c6b93382443d3eddd988680bf49fbad760c90e12eee866"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
        // line 2
        echo "
<!DOCTYPE html>

";
        // line 5
        $this->displayBlock('html_tag', $context, $blocks);
        // line 8
        echo "
    ";
        // line 9
        $this->displayBlock('head', $context, $blocks);
        // line 59
        echo "
    ";
        // line 60
        $this->displayBlock('body_tag', $context, $blocks);
        // line 63
        echo "
        ";
        // line 64
        $this->displayBlock('body_start', $context, $blocks);
        // line 66
        echo "
        ";
        // line 67
        $this->displayBlock('body', $context, $blocks);
        // line 190
        echo "
    ";
        // line 191
        $this->displayBlock('body_end', $context, $blocks);
        // line 193
        echo "</body>
</html>";
    }

    // line 5
    public function block_html_tag($context, array $blocks = array())
    {
        // line 6
        echo "    <html>
    ";
    }

    // line 9
    public function block_head($context, array $blocks = array())
    {
        // line 10
        echo "        <head>
            <meta charset=\"UTF-8\" />
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
            ";
        // line 13
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4dde622_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4dde622_0") : $this->env->getExtension('assets')->getAssetUrl("css/styles_bootstrap.min_1.css");
            // line 19
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
            ";
            // asset "4dde622_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4dde622_1") : $this->env->getExtension('assets')->getAssetUrl("css/styles_bootstrap-theme.min_2.css");
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
            ";
        } else {
            // asset "4dde622"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4dde622") : $this->env->getExtension('assets')->getAssetUrl("css/styles.css");
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
            ";
        }
        unset($context["asset_url"]);
        // line 21
        echo "
            <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/css/jquery-ui.css"), "html", null, true);
        echo "\" >
            <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/css/reveal.css"), "html", null, true);
        echo "\" >
            <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/css/jquery.fancybox.min.css"), "html", null, true);
        echo "\" >
            <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/css/jquery.fancybox-buttons.css"), "html", null, true);
        echo "\" >
            <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/css/jquery.fancybox-thumbs.css"), "html", null, true);
        echo "\" >
            <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/css/fonts/font-awesome.min.css"), "html", null, true);
        echo "\" >

            ";
        // line 30
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/jquery-migrate-1.1.1.min.js"), "html", null, true);
        echo "\"></script>

            ";
        // line 33
        $this->displayBlock('head_style', $context, $blocks);
        // line 40
        echo "
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

            ";
        // line 43
        $this->displayBlock('head_script', $context, $blocks);
        // line 52
        echo "
            <title>";
        // line 53
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
            <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
            ";
        // line 55
        $this->displayBlock('head_bottom', $context, $blocks);
        // line 57
        echo "        </head>
    ";
    }

    // line 33
    public function block_head_style($context, array $blocks = array())
    {
        // line 34
        echo "                ";
        // line 35
        echo "                ";
        // line 39
        echo "            ";
    }

    // line 43
    public function block_head_script($context, array $blocks = array())
    {
        // line 44
        echo "                ";
        // line 51
        echo "            ";
    }

    // line 53
    public function block_title($context, array $blocks = array())
    {
        echo "SNLYF SUNAHIP";
    }

    // line 55
    public function block_head_bottom($context, array $blocks = array())
    {
        // line 56
        echo "            ";
    }

    // line 60
    public function block_body_tag($context, array $blocks = array())
    {
        // line 61
        echo "        <body>
        ";
    }

    // line 64
    public function block_body_start($context, array $blocks = array())
    {
        // line 65
        echo "        ";
    }

    // line 67
    public function block_body($context, array $blocks = array())
    {
        // line 68
        echo "            ";
        $this->displayBlock('navbar', $context, $blocks);
        // line 71
        echo "
            ";
        // line 72
        $this->displayBlock('container', $context, $blocks);
        // line 127
        echo "
        ";
        // line 128
        $this->displayBlock('body_end_before_js', $context, $blocks);
        // line 130
        echo "
        <script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/jquery.reveal.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 133
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/jqueryui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/jqueryui/i18n/jquery-ui-i18n.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/fancybox/jquery.fancybox.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/fancybox/helpers/jquery.fancybox-buttons.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/fancybox/helpers/jquery.fancybox-media.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/fancybox/helpers/jquery.fancybox-thumbs.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/functions.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 141
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "9a4d4d2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9a4d4d2_0") : $this->env->getExtension('assets')->getAssetUrl("js/bsmain_bootstrap.min_1.js");
            // line 147
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        ";
            // asset "9a4d4d2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9a4d4d2_1") : $this->env->getExtension('assets')->getAssetUrl("js/bsmain_moment_2.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        ";
        } else {
            // asset "9a4d4d2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9a4d4d2") : $this->env->getExtension('assets')->getAssetUrl("js/bsmain.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        ";
        }
        unset($context["asset_url"]);
        // line 149
        echo "
        ";
        // line 151
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 152
        echo $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
        <script type=\"text/javascript\">
            \$(document).ready(function() {
                \$.datepicker.regional['es'] = {
                    closeText: 'Cerrar',
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    weekHeader: 'Sm',
                    dateFormat: 'dd/mm/yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                \$.datepicker.setDefaults(\$.datepicker.regional['es']);
                \$('[data-toggle=\"tooltip\"]').tooltip();
                \$('[data-toggle=\"popover\"]').popover();
            });
        </script>
        ";
        // line 177
        $this->displayBlock('script_base', $context, $blocks);
        // line 178
        echo "  
        ";
        // line 179
        $this->displayBlock('foot_script', $context, $blocks);
        // line 189
        echo "    ";
    }

    // line 68
    public function block_navbar($context, array $blocks = array())
    {
        // line 69
        echo "                <!-- No navbar included here to reduce dependencies, see https://github.com/phiamo/MopaBootstrapSandboxBundle/blob/master/Resources/views/base.html.twig for howto include it -->
            ";
    }

    // line 72
    public function block_container($context, array $blocks = array())
    {
        // line 73
        echo "                ";
        $this->displayBlock('container_div_start', $context, $blocks);
        // line 74
        echo "                    ";
        $this->displayBlock('header', $context, $blocks);
        // line 76
        echo "
                    ";
        // line 77
        $this->displayBlock('content_div_start', $context, $blocks);
        // line 78
        echo "                    ";
        $this->displayBlock('page_header', $context, $blocks);
        // line 79
        echo "
                    ";
        // line 80
        $this->displayBlock('flashes', $context, $blocks);
        // line 89
        echo "                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 90
            echo "                        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) ? $context["messages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 91
                echo "                            <div style=\"float:right;\">
                                <div class=\"flash-";
                // line 92
                echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
                echo "\">
                                    ";
                // line 93
                echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
                echo "
                                </div>
                            </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 97
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo "                    ";
        $this->displayBlock('content_row', $context, $blocks);
        // line 112
        echo "
                    ";
        // line 113
        $this->displayBlock('content_div_end', $context, $blocks);
        // line 114
        echo "
                ";
        // line 115
        $this->displayBlock('footer_tag_start', $context, $blocks);
        // line 118
        echo "
                    ";
        // line 119
        $this->displayBlock('footer', $context, $blocks);
        // line 121
        echo "
                    ";
        // line 122
        $this->displayBlock('footer_tag_end', $context, $blocks);
        // line 125
        echo "                ";
        $this->displayBlock('container_div_end', $context, $blocks);
        // line 126
        echo "            ";
    }

    // line 73
    public function block_container_div_start($context, array $blocks = array())
    {
        echo "<div class=\"";
        $this->displayBlock('container_class', $context, $blocks);
        echo "\">";
    }

    public function block_container_class($context, array $blocks = array())
    {
        echo "container";
    }

    // line 74
    public function block_header($context, array $blocks = array())
    {
        // line 75
        echo "                    ";
    }

    // line 77
    public function block_content_div_start($context, array $blocks = array())
    {
        echo "<div class=\"content\">";
    }

    // line 78
    public function block_page_header($context, array $blocks = array())
    {
    }

    // line 80
    public function block_flashes($context, array $blocks = array())
    {
        // line 81
        echo "                        ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "peekAll")) > 0)) {
            // line 82
            echo "                            <div style=\"float:right;\">
                                <div class=\"col-sm-12\">
                                    ";
            // line 84
            echo $context["__internal_e651dead39d546c807c6b93382443d3eddd988680bf49fbad760c90e12eee866"]->getsession_flash();
            echo "
                                </div>
                            </div>
                        ";
        }
        // line 88
        echo "                    ";
    }

    // line 98
    public function block_content_row($context, array $blocks = array())
    {
        // line 99
        echo "                        <div id=\"\" class=\"row\">
                            ";
        // line 100
        $this->displayBlock('content', $context, $blocks);
        // line 110
        echo "                        </div>
                    ";
    }

    // line 100
    public function block_content($context, array $blocks = array())
    {
        // line 101
        echo "                                <div class=\"col-md-9\">
                                    ";
        // line 102
        $this->displayBlock('content_content', $context, $blocks);
        // line 104
        echo "                                </div>
                                <div class=\"col-sm-3\">
                                    ";
        // line 106
        $this->displayBlock('content_sidebar', $context, $blocks);
        // line 108
        echo "                                </div>
                            ";
    }

    // line 102
    public function block_content_content($context, array $blocks = array())
    {
        // line 103
        echo "                                    ";
    }

    // line 106
    public function block_content_sidebar($context, array $blocks = array())
    {
        // line 107
        echo "                                    ";
    }

    // line 113
    public function block_content_div_end($context, array $blocks = array())
    {
        echo "</div>";
    }

    // line 115
    public function block_footer_tag_start($context, array $blocks = array())
    {
        // line 116
        echo "                    <footer>
                    ";
    }

    // line 119
    public function block_footer($context, array $blocks = array())
    {
        // line 120
        echo "                    ";
    }

    // line 122
    public function block_footer_tag_end($context, array $blocks = array())
    {
        // line 123
        echo "                    </footer>
                ";
    }

    // line 125
    public function block_container_div_end($context, array $blocks = array())
    {
        echo "</div><!-- /container -->";
    }

    // line 128
    public function block_body_end_before_js($context, array $blocks = array())
    {
        // line 129
        echo "        ";
    }

    // line 177
    public function block_script_base($context, array $blocks = array())
    {
        // line 178
        echo "        ";
    }

    // line 179
    public function block_foot_script($context, array $blocks = array())
    {
        // line 180
        echo "            ";
        // line 184
        echo "            ";
        $this->displayBlock('foot_script_assetic', $context, $blocks);
        // line 186
        echo "

        ";
    }

    // line 184
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 185
        echo "            ";
    }

    // line 191
    public function block_body_end($context, array $blocks = array())
    {
        // line 192
        echo "    ";
    }

    public function getTemplateName()
    {
        return "MopaBootstrapBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  633 => 192,  630 => 191,  626 => 185,  623 => 184,  617 => 186,  614 => 184,  612 => 180,  609 => 179,  605 => 178,  602 => 177,  598 => 129,  595 => 128,  589 => 125,  584 => 123,  581 => 122,  577 => 120,  574 => 119,  569 => 116,  566 => 115,  560 => 113,  556 => 107,  553 => 106,  549 => 103,  546 => 102,  541 => 108,  539 => 106,  535 => 104,  533 => 102,  530 => 101,  527 => 100,  522 => 110,  520 => 100,  517 => 99,  514 => 98,  510 => 88,  503 => 84,  499 => 82,  496 => 81,  493 => 80,  488 => 78,  482 => 77,  478 => 75,  475 => 74,  462 => 73,  458 => 126,  455 => 125,  453 => 122,  450 => 121,  448 => 119,  445 => 118,  443 => 115,  440 => 114,  438 => 113,  435 => 112,  432 => 98,  426 => 97,  416 => 93,  412 => 92,  409 => 91,  404 => 90,  399 => 89,  397 => 80,  394 => 79,  391 => 78,  389 => 77,  386 => 76,  383 => 74,  380 => 73,  377 => 72,  372 => 69,  369 => 68,  365 => 189,  363 => 179,  360 => 178,  358 => 177,  330 => 152,  325 => 151,  322 => 149,  302 => 147,  298 => 141,  294 => 140,  290 => 139,  286 => 138,  282 => 137,  278 => 136,  274 => 135,  270 => 134,  265 => 133,  261 => 131,  258 => 130,  256 => 128,  253 => 127,  251 => 72,  248 => 71,  245 => 68,  242 => 67,  238 => 65,  235 => 64,  230 => 61,  227 => 60,  223 => 56,  220 => 55,  214 => 53,  210 => 51,  208 => 44,  205 => 43,  201 => 39,  199 => 35,  197 => 34,  194 => 33,  189 => 57,  187 => 55,  183 => 54,  179 => 53,  176 => 52,  174 => 43,  169 => 40,  167 => 33,  162 => 31,  157 => 30,  152 => 27,  148 => 26,  144 => 25,  140 => 24,  132 => 22,  129 => 21,  109 => 19,  105 => 13,  100 => 10,  97 => 9,  92 => 6,  89 => 5,  84 => 193,  82 => 191,  79 => 190,  77 => 67,  74 => 66,  72 => 64,  69 => 63,  67 => 60,  64 => 59,  62 => 9,  59 => 8,  57 => 5,  52 => 2,  50 => 1,  225 => 100,  222 => 99,  218 => 101,  215 => 99,  212 => 98,  173 => 64,  170 => 63,  158 => 55,  153 => 51,  147 => 52,  145 => 51,  141 => 49,  139 => 48,  136 => 23,  133 => 45,  130 => 44,  115 => 33,  101 => 22,  98 => 21,  95 => 20,  91 => 19,  88 => 18,  81 => 15,  78 => 14,  75 => 13,  66 => 10,  46 => 8,  41 => 4,  47 => 9,  38 => 3,  34 => 6,  31 => 5,  28 => 4,);
    }
}
