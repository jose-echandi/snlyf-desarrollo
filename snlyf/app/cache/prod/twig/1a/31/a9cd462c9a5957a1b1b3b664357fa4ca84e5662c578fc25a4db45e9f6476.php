<?php

/* MopaBootstrapBundle::flash.html.twig */
class __TwigTemplate_1a31a9cd462c9a5957a1b1b3b664357fa4ca84e5662c578fc25a4db45e9f6476 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 13
        echo "
";
        // line 32
        echo "
";
    }

    // line 1
    public function getflash($_type = null, $_message = null, $_close = null, $_use_raw = null, $_class = null, $_domain = null)
    {
        $context = $this->env->mergeGlobals(array(
            "type" => $_type,
            "message" => $_message,
            "close" => $_close,
            "use_raw" => $_use_raw,
            "class" => $_class,
            "domain" => $_domain,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "    <div class=\"alert";
            echo twig_escape_filter($this->env, (((isset($context["type"]) ? $context["type"] : null)) ? ((" alert-" . (isset($context["type"]) ? $context["type"] : null))) : ("")), "html", null, true);
            echo " fade in ";
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter((isset($context["class"]) ? $context["class"] : null), "")) : ("")), "html", null, true);
            echo " ";
            if (((array_key_exists("close", $context)) ? (_twig_default_filter((isset($context["close"]) ? $context["close"] : null), false)) : (false))) {
                echo "alert-dismissable";
            }
            echo "\">
    ";
            // line 3
            if (((array_key_exists("close", $context)) ? (_twig_default_filter((isset($context["close"]) ? $context["close"] : null), false)) : (false))) {
                // line 4
                echo "        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
    ";
            }
            // line 6
            echo "    ";
            if (((array_key_exists("use_raw", $context)) ? (_twig_default_filter((isset($context["use_raw"]) ? $context["use_raw"] : null), false)) : (false))) {
                // line 7
                echo "        ";
                echo $this->env->getExtension('translator')->trans((isset($context["message"]) ? $context["message"] : null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter((isset($context["domain"]) ? $context["domain"] : null), "messages")) : ("messages")));
                echo "
    ";
            } else {
                // line 9
                echo "        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["message"]) ? $context["message"] : null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter((isset($context["domain"]) ? $context["domain"] : null), "messages")) : ("messages"))), "html", null, true);
                echo "
    ";
            }
            // line 11
            echo "    </div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 14
    public function getadvanced_flash($_type = null, $_heading = null, $_message = null, $_close_tag = null, $_use_raw = null, $_class = null, $_domain = null)
    {
        $context = $this->env->mergeGlobals(array(
            "type" => $_type,
            "heading" => $_heading,
            "message" => $_message,
            "close_tag" => $_close_tag,
            "use_raw" => $_use_raw,
            "class" => $_class,
            "domain" => $_domain,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 15
            echo "    <div class=\"alert";
            echo twig_escape_filter($this->env, (((isset($context["type"]) ? $context["type"] : null)) ? ((" alert-" . (isset($context["type"]) ? $context["type"] : null))) : ("")), "html", null, true);
            echo " fade in ";
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter((isset($context["class"]) ? $context["class"] : null), "")) : ("")), "html", null, true);
            echo " ";
            if (((array_key_exists("close_tag", $context)) ? (_twig_default_filter((isset($context["close_tag"]) ? $context["close_tag"] : null), false)) : (false))) {
                echo "alert-dismissable";
            }
            echo "\">
    ";
            // line 16
            if (((array_key_exists("close_tag", $context)) ? (_twig_default_filter((isset($context["close_tag"]) ? $context["close_tag"] : null), false)) : (false))) {
                // line 17
                echo "        ";
                if (((isset($context["close_tag"]) ? $context["close_tag"] : null) == true)) {
                    // line 18
                    echo "            ";
                    $context["close_tag"] = "a";
                    // line 19
                    echo "        ";
                }
                // line 20
                echo "        <";
                echo twig_escape_filter($this->env, (isset($context["close_tag"]) ? $context["close_tag"] : null), "html", null, true);
                echo " class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\" ";
                if (((isset($context["close_tag"]) ? $context["close_tag"] : null) == "a")) {
                    echo "href=\"#\"";
                } elseif (((isset($context["close_tag"]) ? $context["close_tag"] : null) == "button")) {
                    echo "type=\"button\"";
                }
                echo ">&times;</";
                echo twig_escape_filter($this->env, (isset($context["close_tag"]) ? $context["close_tag"] : null), "html", null, true);
                echo ">
    ";
            }
            // line 22
            echo "    ";
            if (((array_key_exists("heading", $context)) ? (_twig_default_filter((isset($context["heading"]) ? $context["heading"] : null), false)) : (false))) {
                // line 23
                echo "    <h4 class=\"alert-heading\">";
                echo twig_escape_filter($this->env, (isset($context["heading"]) ? $context["heading"] : null), "html", null, true);
                echo "</h4>
    ";
            }
            // line 25
            echo "    ";
            if (((array_key_exists("use_raw", $context)) ? (_twig_default_filter((isset($context["use_raw"]) ? $context["use_raw"] : null), false)) : (false))) {
                // line 26
                echo "        ";
                echo $this->env->getExtension('translator')->trans((isset($context["message"]) ? $context["message"] : null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter((isset($context["domain"]) ? $context["domain"] : null), "messages")) : ("messages")));
                echo "
    ";
            } else {
                // line 28
                echo "        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["message"]) ? $context["message"] : null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter((isset($context["domain"]) ? $context["domain"] : null), "messages")) : ("messages"))), "html", null, true);
                echo "
    ";
            }
            // line 30
            echo "    </div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 33
    public function getsession_flash($_close = null, $_use_raw = null, $_class = null, $_domain = null)
    {
        $context = $this->env->mergeGlobals(array(
            "close" => $_close,
            "use_raw" => $_use_raw,
            "class" => $_class,
            "domain" => $_domain,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 34
            echo "    ";
            $context["flash_messages"] = $this;
            // line 35
            echo "
    ";
            // line 36
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "peekAll")) > 0)) {
                // line 37
                echo "        ";
                $context["mapping"] = twig_array_merge($this->env->getExtension('mopa_bootstrap_flash')->getMapping(), array("fos_user_success" => "success"));
                // line 38
                echo "        ";
                $context["flashes"] = array();
                // line 39
                echo "
        ";
                // line 40
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["mapping"]) ? $context["mapping"] : null)));
                foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
                    // line 41
                    echo "            ";
                    $context["flashes"] = twig_array_merge((isset($context["flashes"]) ? $context["flashes"] : null), array((isset($context["type"]) ? $context["type"] : null) => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "get", array(0 => (isset($context["type"]) ? $context["type"] : null)), "method")));
                    // line 42
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "
        ";
                // line 44
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["flashes"]) ? $context["flashes"] : null));
                foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                    // line 45
                    echo "            ";
                    if (((isset($context["type"]) ? $context["type"] : null) == "fos_user_success")) {
                        // line 46
                        echo "                ";
                        $context["domain"] = "FOSUserBundle";
                        // line 47
                        echo "            ";
                    }
                    // line 48
                    echo "            ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) ? $context["messages"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                        // line 49
                        echo "                ";
                        echo $context["flash_messages"]->getflash($this->getAttribute((isset($context["mapping"]) ? $context["mapping"] : null), (isset($context["type"]) ? $context["type"] : null), array(), "array"), (isset($context["message"]) ? $context["message"] : null), (isset($context["close"]) ? $context["close"] : null), (isset($context["use_raw"]) ? $context["use_raw"] : null), (isset($context["class"]) ? $context["class"] : null), (isset($context["domain"]) ? $context["domain"] : null));
                        echo "
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 51
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "    ";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "MopaBootstrapBundle::flash.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 51,  241 => 49,  236 => 48,  233 => 47,  211 => 41,  207 => 40,  204 => 39,  198 => 37,  196 => 36,  193 => 35,  190 => 34,  164 => 30,  149 => 25,  143 => 23,  126 => 20,  123 => 19,  120 => 18,  117 => 17,  104 => 15,  87 => 14,  63 => 7,  60 => 6,  56 => 4,  54 => 3,  43 => 2,  27 => 1,  22 => 32,  19 => 13,  633 => 192,  630 => 191,  626 => 185,  623 => 184,  617 => 186,  614 => 184,  612 => 180,  609 => 179,  605 => 178,  602 => 177,  598 => 129,  595 => 128,  589 => 125,  584 => 123,  581 => 122,  577 => 120,  574 => 119,  569 => 116,  566 => 115,  560 => 113,  556 => 107,  553 => 106,  549 => 103,  546 => 102,  541 => 108,  539 => 106,  535 => 104,  533 => 102,  530 => 101,  527 => 100,  522 => 110,  520 => 100,  517 => 99,  514 => 98,  510 => 88,  503 => 84,  499 => 82,  496 => 81,  493 => 80,  488 => 78,  482 => 77,  478 => 75,  475 => 74,  462 => 73,  458 => 126,  455 => 125,  453 => 122,  450 => 121,  448 => 119,  445 => 118,  443 => 115,  440 => 114,  438 => 113,  435 => 112,  432 => 98,  426 => 97,  416 => 93,  412 => 92,  409 => 91,  404 => 90,  399 => 89,  397 => 80,  394 => 79,  391 => 78,  389 => 77,  386 => 76,  383 => 74,  380 => 73,  377 => 72,  372 => 69,  369 => 68,  365 => 189,  363 => 179,  360 => 178,  358 => 177,  330 => 152,  325 => 151,  322 => 149,  302 => 147,  298 => 141,  294 => 140,  290 => 139,  286 => 138,  282 => 137,  278 => 136,  274 => 135,  270 => 134,  265 => 133,  261 => 131,  258 => 130,  256 => 52,  253 => 127,  251 => 72,  248 => 71,  245 => 68,  242 => 67,  238 => 65,  235 => 64,  230 => 46,  227 => 45,  223 => 44,  220 => 43,  214 => 42,  210 => 51,  208 => 44,  205 => 43,  201 => 38,  199 => 35,  197 => 34,  194 => 33,  189 => 57,  187 => 55,  183 => 54,  179 => 53,  176 => 33,  174 => 43,  169 => 40,  167 => 33,  162 => 31,  157 => 30,  152 => 26,  148 => 26,  144 => 25,  140 => 22,  132 => 22,  129 => 21,  109 => 19,  105 => 13,  100 => 10,  97 => 9,  92 => 6,  89 => 5,  84 => 193,  82 => 191,  79 => 190,  77 => 67,  74 => 66,  72 => 64,  69 => 9,  67 => 60,  64 => 59,  62 => 9,  59 => 8,  57 => 5,  52 => 2,  50 => 1,  225 => 100,  222 => 99,  218 => 101,  215 => 99,  212 => 98,  173 => 64,  170 => 63,  158 => 28,  153 => 51,  147 => 52,  145 => 51,  141 => 49,  139 => 48,  136 => 23,  133 => 45,  130 => 44,  115 => 16,  101 => 22,  98 => 21,  95 => 20,  91 => 19,  88 => 18,  81 => 15,  78 => 14,  75 => 11,  66 => 10,  46 => 8,  41 => 4,  47 => 9,  38 => 3,  34 => 6,  31 => 5,  28 => 4,);
    }
}
