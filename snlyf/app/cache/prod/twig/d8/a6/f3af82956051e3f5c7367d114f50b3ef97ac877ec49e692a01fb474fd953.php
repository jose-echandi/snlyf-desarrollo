<?php

/* NewTemplateBundle:Menu:Lateral.html.twig */
class __TwigTemplate_d8a6f3af82956051e3f5c7367d114f50b3ef97ac877ec49e692a01fb474fd953 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user")) {
            // line 2
            echo "                <div id=\"lateral\" >
                    <h2 class=\"text-center\">Menú Principal</h2>
                    <div id=\"\" class=\"col-md-12  sidebar\">
                        <p class=\"text-center\">Bienvenido, <strong>";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "fullname"), "html", null, true);
            echo "</strong><br/>
                            ";
            // line 6
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_CENTRO_HIPICO")) {
                // line 7
                echo "                                Centro Hípico
                            ";
                // line 9
                echo "                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_OPERADOR")) {
                // line 10
                echo "                                Operadora
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_ADMIN")) {
                // line 12
                echo "                                Administrador
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_FISCAL")) {
                // line 14
                echo "                                Fiscal
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_ASESOR")) {
                // line 16
                echo "                                Asesor Legal
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_SUPER")) {
                // line 18
                echo "                                Super Intendente
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_GERENTE")) {
                // line 20
                echo "                                Gerente
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_COORDINADOR")) {
                // line 22
                echo "                                Coordinador
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "roles"), 0) == "ROLE_SUPERINTENDENTE")) {
                // line 24
                echo "                                Superintendente
                            ";
            }
            // line 26
            echo "                        </p><br/>
                        ";
            // line 28
            echo "                        <ul class=\"menuu\">
                            <li><img src=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/images/notificacion.png"), "html", null, true);
            echo "\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("notification");
            echo "\">Notificaciones <span class=\"notif\" id=\"notificationCount\">0</span></a></li>
                            <li><img src=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/images/contrasena.png"), "html", null, true);
            echo "\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
            echo "\">Cambiar Contrase&ntilde;a</a></li>
                        </ul>
                         <h3 ><a href=\"";
            // line 32
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-default btn-small\">Cerrar sesión</a></h3>
                    </div><br/><p></p>
                    ";
            // line 34
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                echo " ";
                // line 35
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_admin.html.twig")->display($context);
                // line 36
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_GERENTE")) {
                // line 37
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_gerente.html.twig")->display($context);
                // line 38
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_LICENCIA")) {
                // line 39
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_licencia.html.twig")->display($context);
                // line 40
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_OPERADOR")) {
                // line 41
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_operadora.html.twig")->display($context);
                // line 42
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_FISCAL")) {
                // line 43
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_fiscal.html.twig")->display($context);
                // line 44
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_ASESOR")) {
                // line 45
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_asesor.html.twig")->display($context);
                // line 46
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_CENTRO_HIPICO")) {
                // line 47
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_usuario.html.twig")->display($context);
                // line 48
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_SUPERINTENDENTE")) {
                // line 49
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_superintendente.html.twig")->display($context);
                // line 50
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_COORDINADOR")) {
                // line 51
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_coordinador.html.twig")->display($context);
                // line 52
                echo "                    ";
            }
            // line 53
            echo "                </div>
    ";
        }
        // line 55
        echo "<script>
    function getNotifications()
    {
        var url = Routing.generate('notification_count');
        \$.get( url, function( data ) {
            //console.info(\"notificaciones-\",data);
            \$(\"#notificationCount\").text(data);
        });
    }
    \$(document).ready(function() {
        getNotifications();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:Lateral.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 55,  151 => 53,  142 => 50,  127 => 45,  124 => 44,  121 => 43,  118 => 42,  112 => 40,  106 => 38,  103 => 37,  94 => 34,  76 => 29,  73 => 28,  70 => 26,  58 => 20,  42 => 12,  35 => 9,  32 => 7,  30 => 6,  26 => 5,  21 => 2,  250 => 51,  241 => 49,  236 => 48,  233 => 47,  211 => 41,  207 => 40,  204 => 39,  198 => 37,  196 => 36,  193 => 35,  190 => 34,  164 => 30,  149 => 25,  143 => 23,  126 => 20,  123 => 19,  120 => 18,  117 => 17,  104 => 15,  87 => 14,  63 => 7,  60 => 6,  56 => 4,  54 => 18,  43 => 2,  27 => 1,  22 => 32,  19 => 1,  633 => 192,  630 => 191,  626 => 185,  623 => 184,  617 => 186,  614 => 184,  612 => 180,  609 => 179,  605 => 178,  602 => 177,  598 => 129,  595 => 128,  589 => 125,  584 => 123,  581 => 122,  577 => 120,  574 => 119,  569 => 116,  566 => 115,  560 => 113,  556 => 107,  553 => 106,  549 => 103,  546 => 102,  541 => 108,  539 => 106,  535 => 104,  533 => 102,  530 => 101,  527 => 100,  522 => 110,  520 => 100,  517 => 99,  514 => 98,  510 => 88,  503 => 84,  499 => 82,  496 => 81,  493 => 80,  488 => 78,  482 => 77,  478 => 75,  475 => 74,  462 => 73,  458 => 126,  455 => 125,  453 => 122,  450 => 121,  448 => 119,  445 => 118,  443 => 115,  440 => 114,  438 => 113,  435 => 112,  432 => 98,  426 => 97,  416 => 93,  412 => 92,  409 => 91,  404 => 90,  399 => 89,  397 => 80,  394 => 79,  391 => 78,  389 => 77,  386 => 76,  383 => 74,  380 => 73,  377 => 72,  372 => 69,  369 => 68,  365 => 189,  363 => 179,  360 => 178,  358 => 177,  330 => 152,  325 => 151,  322 => 149,  302 => 147,  298 => 141,  294 => 140,  290 => 139,  286 => 138,  282 => 137,  278 => 136,  274 => 135,  270 => 134,  265 => 133,  261 => 131,  258 => 130,  256 => 52,  253 => 127,  251 => 72,  248 => 71,  245 => 68,  242 => 67,  238 => 65,  235 => 64,  230 => 46,  227 => 45,  223 => 44,  220 => 43,  214 => 42,  210 => 51,  208 => 44,  205 => 43,  201 => 38,  199 => 35,  197 => 34,  194 => 33,  189 => 57,  187 => 55,  183 => 54,  179 => 53,  176 => 33,  174 => 43,  169 => 40,  167 => 33,  162 => 31,  157 => 30,  152 => 26,  148 => 52,  144 => 25,  140 => 22,  132 => 22,  129 => 21,  109 => 39,  105 => 13,  100 => 36,  97 => 35,  92 => 6,  89 => 32,  84 => 193,  82 => 30,  79 => 190,  77 => 67,  74 => 66,  72 => 64,  69 => 9,  67 => 60,  64 => 59,  62 => 22,  59 => 8,  57 => 5,  52 => 2,  50 => 16,  225 => 100,  222 => 99,  218 => 101,  215 => 99,  212 => 98,  173 => 64,  170 => 63,  158 => 28,  153 => 51,  147 => 52,  145 => 51,  141 => 49,  139 => 49,  136 => 48,  133 => 47,  130 => 46,  115 => 41,  101 => 22,  98 => 21,  95 => 20,  91 => 19,  88 => 18,  81 => 15,  78 => 14,  75 => 11,  66 => 24,  46 => 14,  41 => 4,  47 => 9,  38 => 10,  34 => 6,  31 => 5,  28 => 4,);
    }
}
