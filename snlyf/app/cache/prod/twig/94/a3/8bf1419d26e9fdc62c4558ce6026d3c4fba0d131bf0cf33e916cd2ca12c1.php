<?php

/* NewTemplateBundle::base.html.twig */
class __TwigTemplate_94a38bf1419d26e9fdc62c4558ce6026d3c4fba0d131bf0cf33e916cd2ca12c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("MopaBootstrapBundle::base.html.twig");

        $this->blocks = array(
            'head_style' => array($this, 'block_head_style'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_start' => array($this, 'block_body_start'),
            'header' => array($this, 'block_header'),
            'page_header' => array($this, 'block_page_header'),
            'content' => array($this, 'block_content'),
            'content_content' => array($this, 'block_content_content'),
            'footer' => array($this, 'block_footer'),
            'script_base' => array($this, 'block_script_base'),
            'foot_script' => array($this, 'block_foot_script'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MopaBootstrapBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head_style($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "cb5be35_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cb5be35_0") : $this->env->getExtension('assets')->getAssetUrl("css/cb5be35_style_1.css");
            // line 8
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
            // asset "cb5be35_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cb5be35_1") : $this->env->getExtension('assets')->getAssetUrl("css/cb5be35_genstyles_2.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
        } else {
            // asset "cb5be35"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_cb5be35") : $this->env->getExtension('assets')->getAssetUrl("css/cb5be35.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
        }
        unset($context["asset_url"]);
        // line 10
        echo "  ";
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 13
    public function block_body_start($context, array $blocks = array())
    {
        // line 14
        echo "        <div id=\"cinta_gob\" class=\"row-fluid \">        
            <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/newtemplate/images/cintillo.png"), "html", null, true);
        echo "\" height=\"53\" width=\"1140\" alt=\"Gobierno Bolivariano\"> 
       </div>
  ";
    }

    // line 18
    public function block_header($context, array $blocks = array())
    {
        // line 19
        echo "   ";
    }

    // line 20
    public function block_page_header($context, array $blocks = array())
    {
        // line 21
        echo "    <div id=\"header\" class=\"row-fluid\">
            <div id=\"\" class=\"col-md-4\"> <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/newtemplate/images/logo.png"), "html", null, true);
        echo "\" alt=\"SUNAHIP\" title=\"SUNAHIP\"> </div>
            <div id=\"tit_pag\" class=\"col-md-6 col-offset-2 \">
              <span style=\"font-family: Times New Roman;font-size: 26px;color: #fff\"> Registro Nacional de Licencias y Fiscalización <span> </span>
            </div>
     </div>
     <div id=\"header2\"class=\"row-fluid\">
\t<div id=\"fecha\" class=\"fecha col-md-4\">
            Fecha
        </div>
        <div class=\"col-md-7 col-offset-2\">
            <ul id=\"menu1\">    
                <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\">Inicio</a></li>
                <li> | </li>
                <li><a href=\"#\">Recaudos</a></li>
                <li> | </li>
                <li><a href=\"#\">Tipo de Licencias</a></li>
                <li> | </li>
                <li><a href=\"#\">Preguntas Frecuentes</a></li>
            </ul>
        </div>   
      </div>
";
    }

    // line 44
    public function block_content($context, array $blocks = array())
    {
        // line 45
        echo "<div id=\"main_content\" clas=\"row-fluid\">
        ";
        // line 47
        echo "    <div id=\"\" class=\"col-md-3\">
        ";
        // line 48
        $this->env->loadTemplate("NewTemplateBundle:Menu:Lateral.html.twig")->display($context);
        // line 49
        echo "    </div> 
    <div id=\"right_side\" class=\"col-md-9\">     
        ";
        // line 51
        $this->displayBlock('content_content', $context, $blocks);
        // line 52
        echo "    </div>   
</div>        
";
    }

    // line 51
    public function block_content_content($context, array $blocks = array())
    {
    }

    // line 55
    public function block_footer($context, array $blocks = array())
    {
        echo " 
      <div class=\"row-fluid\">
            <div id=\"footer\">
                <p>SUNAHIP - Superintendencia Nacional de Actividades Hípicas 2015</p>
            </div>    
      </div>      
  ";
    }

    // line 63
    public function block_script_base($context, array $blocks = array())
    {
        // line 64
        echo "       <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/newtemplate/js/func.js"), "html", null, true);
        echo "\"></script>
       <script type=\"text/javascript\" >
           \$(document).ready(function(){
                \$.datepicker.setDefaults(\$.datepicker.regional[\"es\"]);
                 var string = \$.datepicker.formatDate('DD, dd --  MM --  yy', new Date(),{
                     dayNames: \$.datepicker.regional[ \"es\" ].dayNames,
                     onthNames: \$.datepicker.regional[ \"es\" ].monthNames
                 });
                    string= string.charAt(0).toUpperCase() + string.slice(1);
                    string=string.replace(\"--\", \"de\");
                    string=string.replace(\"--\", \"de\");
                \$(\".fecha\").html(string);
                 //DataTable.defaults={};
           });

           function load(url) {
               \$.ajax({
                   type: \"GET\",
                   url: url,
                   crossDomain: false,
                   data: {ajax: 1},
                   dataType: \"html\",
                   success: function (data) {
                       window.history.pushState(\"\", \"\", url);
                       \$(\"#right_side\").html(data);
                   },
                   error: function (data) {
                       var message = \"No se pudo completar la acci&oacute;n\";
                       \$(\"#right_side\").html(\"<div class='alert alert-info fade in'><i class='fa-fw fa fa-info'></i><strong>Info: </strong>\" + message + \"</div>\");
                   }
               });
           }
       </script>
  ";
    }

    // line 98
    public function block_foot_script($context, array $blocks = array())
    {
        // line 99
        echo "            ";
        $this->displayBlock('foot_script_assetic', $context, $blocks);
        // line 101
        echo "  ";
    }

    // line 99
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 100
        echo "            ";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 100,  222 => 99,  218 => 101,  215 => 99,  212 => 98,  173 => 64,  170 => 63,  158 => 55,  153 => 51,  147 => 52,  145 => 51,  141 => 49,  139 => 48,  136 => 47,  133 => 45,  130 => 44,  115 => 33,  101 => 22,  98 => 21,  95 => 20,  91 => 19,  88 => 18,  81 => 15,  78 => 14,  75 => 13,  66 => 10,  46 => 8,  41 => 4,  47 => 9,  38 => 3,  34 => 6,  31 => 5,  28 => 4,);
    }
}
