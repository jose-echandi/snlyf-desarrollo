<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/fonts/glyphicons-halflings-regular')) {
            // _assetic_fonts_glyphicons_eot
            if ($pathinfo === '/fonts/glyphicons-halflings-regular.eot') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_eot',  'pos' => NULL,  '_format' => 'eot',  '_route' => '_assetic_fonts_glyphicons_eot',);
            }

            // _assetic_fonts_glyphicons_eot_0
            if ($pathinfo === '/fonts/glyphicons-halflings-regular_glyphicons-halflings-regular_1.eot') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_eot',  'pos' => 0,  '_format' => 'eot',  '_route' => '_assetic_fonts_glyphicons_eot_0',);
            }

            // _assetic_fonts_glyphicons_svg
            if ($pathinfo === '/fonts/glyphicons-halflings-regular.svg') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_svg',  'pos' => NULL,  '_format' => 'svg',  '_route' => '_assetic_fonts_glyphicons_svg',);
            }

            // _assetic_fonts_glyphicons_svg_0
            if ($pathinfo === '/fonts/glyphicons-halflings-regular_glyphicons-halflings-regular_1.svg') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_svg',  'pos' => 0,  '_format' => 'svg',  '_route' => '_assetic_fonts_glyphicons_svg_0',);
            }

            // _assetic_fonts_glyphicons_ttf
            if ($pathinfo === '/fonts/glyphicons-halflings-regular.ttf') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_ttf',  'pos' => NULL,  '_format' => 'ttf',  '_route' => '_assetic_fonts_glyphicons_ttf',);
            }

            // _assetic_fonts_glyphicons_ttf_0
            if ($pathinfo === '/fonts/glyphicons-halflings-regular_glyphicons-halflings-regular_1.ttf') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_ttf',  'pos' => 0,  '_format' => 'ttf',  '_route' => '_assetic_fonts_glyphicons_ttf_0',);
            }

            // _assetic_fonts_glyphicons_woff
            if ($pathinfo === '/fonts/glyphicons-halflings-regular.woff') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_woff',  'pos' => NULL,  '_format' => 'woff',  '_route' => '_assetic_fonts_glyphicons_woff',);
            }

            // _assetic_fonts_glyphicons_woff_0
            if ($pathinfo === '/fonts/glyphicons-halflings-regular_glyphicons-halflings-regular_1.woff') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'fonts_glyphicons_woff',  'pos' => 0,  '_format' => 'woff',  '_route' => '_assetic_fonts_glyphicons_woff_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css')) {
            if (0 === strpos($pathinfo, '/css/cb5be35')) {
                // _assetic_cb5be35
                if ($pathinfo === '/css/cb5be35.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cb5be35',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_cb5be35',);
                }

                if (0 === strpos($pathinfo, '/css/cb5be35_')) {
                    // _assetic_cb5be35_0
                    if ($pathinfo === '/css/cb5be35_style_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'cb5be35',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_cb5be35_0',);
                    }

                    // _assetic_cb5be35_1
                    if ($pathinfo === '/css/cb5be35_genstyles_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'cb5be35',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_cb5be35_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/615c560')) {
                // _assetic_615c560
                if ($pathinfo === '/css/615c560.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '615c560',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_615c560',);
                }

                if (0 === strpos($pathinfo, '/css/615c560_')) {
                    // _assetic_615c560_0
                    if ($pathinfo === '/css/615c560_tboverride_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '615c560',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_615c560_0',);
                    }

                    // _assetic_615c560_1
                    if ($pathinfo === '/css/615c560_style_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '615c560',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_615c560_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/5302d9e')) {
                // _assetic_5302d9e
                if ($pathinfo === '/css/5302d9e.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5302d9e',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_5302d9e',);
                }

                // _assetic_5302d9e_0
                if ($pathinfo === '/css/5302d9e_style_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5302d9e',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_5302d9e_0',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/bmain')) {
            // _assetic_0b098ee
            if ($pathinfo === '/js/bmain.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0b098ee',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_0b098ee',);
            }

            // _assetic_0b098ee_0
            if ($pathinfo === '/js/bmain_jquery.reveal_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0b098ee',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_0b098ee_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/styles')) {
            // _assetic_4dde622
            if ($pathinfo === '/css/styles.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '4dde622',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_4dde622',);
            }

            if (0 === strpos($pathinfo, '/css/styles_bootstrap')) {
                // _assetic_4dde622_0
                if ($pathinfo === '/css/styles_bootstrap.min_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '4dde622',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_4dde622_0',);
                }

                // _assetic_4dde622_1
                if ($pathinfo === '/css/styles_bootstrap-theme.min_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '4dde622',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_4dde622_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js')) {
            if (0 === strpos($pathinfo, '/js/bsmain')) {
                // _assetic_9a4d4d2
                if ($pathinfo === '/js/bsmain.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '9a4d4d2',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_9a4d4d2',);
                }

                if (0 === strpos($pathinfo, '/js/bsmain_')) {
                    // _assetic_9a4d4d2_0
                    if ($pathinfo === '/js/bsmain_bootstrap.min_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '9a4d4d2',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_9a4d4d2_0',);
                    }

                    // _assetic_9a4d4d2_1
                    if ($pathinfo === '/js/bsmain_moment_2.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '9a4d4d2',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_9a4d4d2_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/js/15b70a9')) {
                // _assetic_15b70a9
                if ($pathinfo === '/js/15b70a9.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '15b70a9',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_15b70a9',);
                }

                // _assetic_15b70a9_0
                if ($pathinfo === '/js/15b70a9_mopabootstrap-collection_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '15b70a9',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_15b70a9_0',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css/e1efe2f')) {
            // _assetic_e1efe2f
            if ($pathinfo === '/css/e1efe2f.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'e1efe2f',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_e1efe2f',);
            }

            // _assetic_e1efe2f_0
            if ($pathinfo === '/css/e1efe2f_build_standalone_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'e1efe2f',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_e1efe2f_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js')) {
            if (0 === strpos($pathinfo, '/js/f06b435')) {
                // _assetic_f06b435
                if ($pathinfo === '/js/f06b435.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'f06b435',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_f06b435',);
                }

                // _assetic_f06b435_0
                if ($pathinfo === '/js/f06b435_bootstrap-datetimepicker_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'f06b435',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_f06b435_0',);
                }

            }

            if (0 === strpos($pathinfo, '/js/8eb30f8')) {
                // _assetic_8eb30f8
                if ($pathinfo === '/js/8eb30f8.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '8eb30f8',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_8eb30f8',);
                }

                // _assetic_8eb30f8_0
                if ($pathinfo === '/js/8eb30f8_bootstrap-datetimepicker.es_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '8eb30f8',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_8eb30f8_0',);
                }

            }

            if (0 === strpos($pathinfo, '/js/79dc8c9')) {
                // _assetic_79dc8c9
                if ($pathinfo === '/js/79dc8c9.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '79dc8c9',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_79dc8c9',);
                }

                // _assetic_79dc8c9_0
                if ($pathinfo === '/js/79dc8c9_Chart.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '79dc8c9',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_79dc8c9_0',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                if (0 === strpos($pathinfo, '/_profiler/i')) {
                    // _profiler_info
                    if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                    }

                    // _profiler_import
                    if ($pathinfo === '/_profiler/import') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:importAction',  '_route' => '_profiler_import',);
                    }

                }

                // _profiler_export
                if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]++)\\.txt$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_export')), array (  '_controller' => 'web_profiler.controller.profiler:exportAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/exportar')) {
            if (0 === strpos($pathinfo, '/exportar/pdf')) {
                // exportpdf_operafiliados
                if ($pathinfo === '/exportar/pdf/operadora-afiliados-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::operAfiliadosAction',  '_route' => 'exportpdf_operafiliados',);
                }

                // exportpdf_gerente_operadoras
                if ($pathinfo === '/exportar/pdf/gerente-operadoras-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::gerenteOperListAction',  '_route' => 'exportpdf_gerente_operadoras',);
                }

                // exportpdf_citas
                if ($pathinfo === '/exportar/pdf/citas-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::CitasListAction',  '_route' => 'exportpdf_citas',);
                }

                // exportpdf_licencias
                if (preg_match('#^/exportar/pdf/(?P<tipo>[^/\\-]++)\\-licencias\\-list$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_licencias')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::LicenciasListAction',));
                }

                // exportpdf_licencias_aprob
                if ($pathinfo === '/exportar/pdf/licencias-aprobadas-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::LicenciasListAprobAction',  '_route' => 'exportpdf_licencias_aprob',);
                }

                // exportpdf_providencia
                if ($pathinfo === '/exportar/pdf/providencias-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::ProvidenciaListAction',  '_route' => 'exportpdf_providencia',);
                }

                if (0 === strpos($pathinfo, '/exportar/pdf/fiscal')) {
                    // exportpdf_fiscales
                    if ($pathinfo === '/exportar/pdf/fiscales-list') {
                        return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::FiscalesListAction',  '_route' => 'exportpdf_fiscales',);
                    }

                    if (0 === strpos($pathinfo, '/exportar/pdf/fiscalizaciones')) {
                        // exportpdf_fiscalizacion_todos
                        if (preg_match('#^/exportar/pdf/fiscalizaciones\\-(?P<tipo>[^/\\-]++)\\-list$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_fiscalizacion_todos')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::FiscalizacionListAction',  'tipo' => 'todos',));
                        }

                        // exportpdf_fiscalizacion_citados
                        if (preg_match('#^/exportar/pdf/fiscalizaciones\\-(?P<tipo>[^/\\-]++)\\-list$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_fiscalizacion_citados')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::FiscalizacionListAction',  'tipo' => 'citados',));
                        }

                        // exportpdf_fiscalizacion_multados
                        if (preg_match('#^/exportar/pdf/fiscalizaciones\\-(?P<tipo>[^/\\-]++)\\-list$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_fiscalizacion_multados')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::FiscalizacionListAction',  'tipo' => 'multados',));
                        }

                    }

                }

                // exportpdf_pagos_MULTA
                if (preg_match('#^/exportar/pdf/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportpdf_pagos_MULTA;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_pagos_MULTA')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::PagosListAction',  'tipo' => 'MULTA',));
                }
                not_exportpdf_pagos_MULTA:

                // exportpdf_pagos_PROCESAMIENTO
                if (preg_match('#^/exportar/pdf/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportpdf_pagos_PROCESAMIENTO;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_pagos_PROCESAMIENTO')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::PagosListAction',  'tipo' => 'PROCESAMIENTO',));
                }
                not_exportpdf_pagos_PROCESAMIENTO:

                // exportpdf_pagos_OTORGAMIENTO
                if (preg_match('#^/exportar/pdf/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportpdf_pagos_OTORGAMIENTO;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_pagos_OTORGAMIENTO')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::PagosListAction',  'tipo' => 'OTORGAMIENTO',));
                }
                not_exportpdf_pagos_OTORGAMIENTO:

                // exportpdf_pagos_APORTE_MENSUAL
                if (preg_match('#^/exportar/pdf/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportpdf_pagos_APORTE_MENSUAL;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportpdf_pagos_APORTE_MENSUAL')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportPdfController::PagosListAction',  'tipo' => 'APORTE_MENSUAL',));
                }
                not_exportpdf_pagos_APORTE_MENSUAL:

            }

            if (0 === strpos($pathinfo, '/exportar/xls')) {
                // exportxls_operafilliados
                if ($pathinfo === '/exportar/xls/operadora-afiliados-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::operAfiliadosAction',  '_route' => 'exportxls_operafilliados',);
                }

                // exportxls_gerente_operadoras
                if ($pathinfo === '/exportar/xls/gerente-operadoras-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::gerenteOperListAction',  '_route' => 'exportxls_gerente_operadoras',);
                }

                // exportxls_citas
                if ($pathinfo === '/exportar/xls/citas-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::CitasListAction',  '_route' => 'exportxls_citas',);
                }

                // exportxls_licencias
                if (preg_match('#^/exportar/xls/(?P<tipo>[^/\\-]++)\\-licencias\\-list$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_licencias')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::LicenciasListAction',));
                }

                // exportxls_licencias_aprob
                if ($pathinfo === '/exportar/xls/licencias-aprobadas-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::LicenciasListAprobAction',  '_route' => 'exportxls_licencias_aprob',);
                }

                // exportxls_providencia
                if ($pathinfo === '/exportar/xls/providencias-list') {
                    return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::ProvidenciaListAction',  '_route' => 'exportxls_providencia',);
                }

                if (0 === strpos($pathinfo, '/exportar/xls/fiscal')) {
                    // exportxls_fiscales
                    if ($pathinfo === '/exportar/xls/fiscales-list') {
                        return array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::FiscalesListAction',  '_route' => 'exportxls_fiscales',);
                    }

                    if (0 === strpos($pathinfo, '/exportar/xls/fiscalizaciones')) {
                        // exportxls_fiscalizacion_todos
                        if (preg_match('#^/exportar/xls/fiscalizaciones\\-(?P<tipo>[^/\\-]++)\\-list$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_fiscalizacion_todos')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::FiscalizacionListAction',  'tipo' => 'todos',));
                        }

                        // exportxls_fiscalizacion_citados
                        if (preg_match('#^/exportar/xls/fiscalizaciones\\-(?P<tipo>[^/\\-]++)\\-list$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_fiscalizacion_citados')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::FiscalizacionListAction',  'tipo' => 'citados',));
                        }

                        // exportxls_fiscalizacion_multados
                        if (preg_match('#^/exportar/xls/fiscalizaciones\\-(?P<tipo>[^/\\-]++)\\-list$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_fiscalizacion_multados')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::FiscalizacionListAction',  'tipo' => 'multados',));
                        }

                    }

                }

                // exportxls_pagos_APORTE_MENSUAL
                if (preg_match('#^/exportar/xls/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportxls_pagos_APORTE_MENSUAL;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_pagos_APORTE_MENSUAL')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::PagosListAction',  'tipo' => 'APORTE_MENSUAL',));
                }
                not_exportxls_pagos_APORTE_MENSUAL:

                // exportxls_pagos_OTORGAMIENTO
                if (preg_match('#^/exportar/xls/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportxls_pagos_OTORGAMIENTO;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_pagos_OTORGAMIENTO')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::PagosListAction',  'tipo' => 'OTORGAMIENTO',));
                }
                not_exportxls_pagos_OTORGAMIENTO:

                // exportxls_pagos_PROCESAMIENTO
                if (preg_match('#^/exportar/xls/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportxls_pagos_PROCESAMIENTO;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_pagos_PROCESAMIENTO')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::PagosListAction',  'tipo' => 'PROCESAMIENTO',));
                }
                not_exportxls_pagos_PROCESAMIENTO:

                // exportxls_pagos_MULTA
                if (preg_match('#^/exportar/xls/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)\\-list$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_exportxls_pagos_MULTA;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'exportxls_pagos_MULTA')), array (  '_controller' => 'Sunahip\\ExportBundle\\Controller\\ExportXlsController::PagosListAction',  'tipo' => 'MULTA',));
                }
                not_exportxls_pagos_MULTA:

            }

        }

        if (0 === strpos($pathinfo, '/informe')) {
            // informe_xmes
            if (preg_match('#^/informe/(?P<reporte>[^/]++)/xmes$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'informe_xmes')), array (  '_controller' => 'Sunahip\\ReporteBundle\\Controller\\ReporteController::reportePorMesAction',));
            }

            // informe_xanio
            if (preg_match('#^/informe/(?P<reporte>[^/]++)/xanio$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'informe_xanio')), array (  '_controller' => 'Sunahip\\ReporteBundle\\Controller\\ReporteController::reportePorAnioAction',));
            }

            // informe_xestado
            if (preg_match('#^/informe/(?P<reporte>[^/]++)/xestado$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'informe_xestado')), array (  '_controller' => 'Sunahip\\ReporteBundle\\Controller\\ReporteController::reportePorEstadoAction',));
            }

            if (0 === strpos($pathinfo, '/informe/ejecutivo')) {
                // informe_ejecutivo
                if ($pathinfo === '/informe/ejecutivo') {
                    return array (  '_controller' => 'Sunahip\\ReporteBundle\\Controller\\ReporteController::ejecutivoAction',  '_route' => 'informe_ejecutivo',);
                }

                // imprimir_ejecutivo
                if ($pathinfo === '/informe/ejecutivo/imprimir') {
                    return array (  '_controller' => 'Sunahip\\ReporteBundle\\Controller\\ReporteController::imprimirEjecutivoAction',  '_route' => 'imprimir_ejecutivo',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/banco')) {
            // banco
            if (rtrim($pathinfo, '/') === '/banco') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_banco;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'banco');
                }

                return array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::indexAction',  '_route' => 'banco',);
            }
            not_banco:

            // banco_create
            if ($pathinfo === '/banco/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_banco_create;
                }

                return array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::createAction',  '_route' => 'banco_create',);
            }
            not_banco_create:

            // banco_new
            if ($pathinfo === '/banco/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_banco_new;
                }

                return array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::newAction',  '_route' => 'banco_new',);
            }
            not_banco_new:

            // banco_show
            if (preg_match('#^/banco/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_banco_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'banco_show')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::showAction',));
            }
            not_banco_show:

            // banco_edit
            if (preg_match('#^/banco/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_banco_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'banco_edit')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::editAction',));
            }
            not_banco_edit:

            // banco_update
            if (preg_match('#^/banco/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_banco_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'banco_update')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::updateAction',));
            }
            not_banco_update:

            // banco_delete
            if (preg_match('#^/banco/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_banco_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'banco_delete')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\BancoController::deleteAction',));
            }
            not_banco_delete:

        }

        if (0 === strpos($pathinfo, '/p')) {
            if (0 === strpos($pathinfo, '/pago')) {
                // pago
                if (rtrim($pathinfo, '/') === '/pago') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pago;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'pago');
                    }

                    return array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::indexAction',  '_route' => 'pago',);
                }
                not_pago:

                // pago_create
                if ($pathinfo === '/pago/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_pago_create;
                    }

                    return array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::createAction',  '_route' => 'pago_create',);
                }
                not_pago_create:

                // pago_new
                if ($pathinfo === '/pago/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pago_new;
                    }

                    return array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::newAction',  '_route' => 'pago_new',);
                }
                not_pago_new:

                // pago_show
                if (preg_match('#^/pago/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pago_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pago_show')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::showAction',));
                }
                not_pago_show:

                // pago_edit
                if (preg_match('#^/pago/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pago_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pago_edit')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::editAction',));
                }
                not_pago_edit:

                // pago_update
                if (preg_match('#^/pago/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_pago_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pago_update')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::updateAction',));
                }
                not_pago_update:

                // pago_delete
                if (preg_match('#^/pago/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_pago_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pago_delete')), array (  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::deleteAction',));
                }
                not_pago_delete:

                // pago_info
                if (0 === strpos($pathinfo, '/pago/info') && preg_match('#^/pago/info(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_pago_info;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pago_info')), array (  'id' => NULL,  '_controller' => 'Sunahip\\PagosBundle\\Controller\\PagosController::getInfoAction',));
                }
                not_pago_info:

            }

            if (0 === strpos($pathinfo, '/providencia')) {
                // providencia
                if (rtrim($pathinfo, '/') === '/providencia') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'providencia');
                    }

                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::indexAction',  '_route' => 'providencia',);
                }

                // providencia_show
                if (preg_match('#^/providencia/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_show')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::showAction',));
                }

                // providencia_new
                if ($pathinfo === '/providencia/new') {
                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::newAction',  '_route' => 'providencia_new',);
                }

                // providencia_create
                if ($pathinfo === '/providencia/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_providencia_create;
                    }

                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::createAction',  '_route' => 'providencia_create',);
                }
                not_providencia_create:

                // providencia_edit
                if (preg_match('#^/providencia/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_edit')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::editAction',));
                }

                // providencia_update
                if (preg_match('#^/providencia/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_providencia_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_update')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::updateAction',));
                }
                not_providencia_update:

                // providencia_delete
                if (preg_match('#^/providencia/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_providencia_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_delete')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::deleteAction',));
                }
                not_providencia_delete:

                // providencia_estado
                if (preg_match('#^/providencia/(?P<id>[^/]++)/estado$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_providencia_estado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_estado')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::estadoAction',));
                }
                not_providencia_estado:

                // providencia_municipio
                if (preg_match('#^/providencia/(?P<id>[^/]++)/municipio$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_providencia_municipio;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_municipio')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::municipioAction',));
                }
                not_providencia_municipio:

                // providencia_operadora
                if (preg_match('#^/providencia/(?P<id>[^/]++)/operadora$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_providencia_operadora;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_operadora')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::operadoraAction',));
                }
                not_providencia_operadora:

                // providencia_fiscalizacion
                if (0 === strpos($pathinfo, '/providencia/fiscalizado') && preg_match('#^/providencia/fiscalizado/(?P<tipo>centro|operadora)/(?P<prov>\\d+)/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_providencia_fiscalizacion;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'providencia_fiscalizacion')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\ProvidenciaController::fiscalizacionAction',));
                }
                not_providencia_fiscalizacion:

            }

        }

        if (0 === strpos($pathinfo, '/fiscalizacion')) {
            // fiscalizacion
            if (rtrim($pathinfo, '/') === '/fiscalizacion') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fiscalizacion');
                }

                return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::indexAction',  '_route' => 'fiscalizacion',);
            }

            // fiscalizacion_show
            if (preg_match('#^/fiscalizacion/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscalizacion_show')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::showAction',));
            }

            // fiscalizacion_new
            if (preg_match('#^/fiscalizacion/(?P<idprov>[^/]++)/(?P<idcentro>[^/]++)/(?P<tipo>centro|operadora)/fiscalizar$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fiscalizacion_new;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscalizacion_new')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::newAction',));
            }
            not_fiscalizacion_new:

            // fiscalizacion_create
            if (preg_match('#^/fiscalizacion/(?P<idprov>[^/]++)/(?P<idcentro>[^/]++)/(?P<tipo>centro|operadora)/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fiscalizacion_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscalizacion_create')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::createAction',));
            }
            not_fiscalizacion_create:

            // fiscalizacion_edit
            if (preg_match('#^/fiscalizacion/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscalizacion_edit')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::editAction',));
            }

            // fiscalizacion_delete
            if (preg_match('#^/fiscalizacion/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_fiscalizacion_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscalizacion_delete')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::deleteAction',));
            }
            not_fiscalizacion_delete:

            if (0 === strpos($pathinfo, '/fiscalizacion/fiscalizar')) {
                // fiscalizacion_fiscalizar
                if ($pathinfo === '/fiscalizacion/fiscalizar') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fiscalizacion_fiscalizar;
                    }

                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::fiscalizarAction',  '_route' => 'fiscalizacion_fiscalizar',);
                }
                not_fiscalizacion_fiscalizar:

                // fiscalizacion_centro
                if ($pathinfo === '/fiscalizacion/fiscalizar/centro') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fiscalizacion_centro;
                    }

                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\DefaultController::centroAction',  '_route' => 'fiscalizacion_centro',);
                }
                not_fiscalizacion_centro:

            }

            // fiscalizacion_citados
            if ($pathinfo === '/fiscalizacion/citados') {
                return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::citadosAction',  '_route' => 'fiscalizacion_citados',);
            }

            // fiscalizacion_multar
            if (preg_match('#^/fiscalizacion/(?P<id>\\d+)/multar$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscalizacion_multar')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::multarAction',));
            }

            if (0 === strpos($pathinfo, '/fiscalizacion/m')) {
                // fiscalizacion_multados
                if ($pathinfo === '/fiscalizacion/multado') {
                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::multadosAction',  '_route' => 'fiscalizacion_multados',);
                }

                // fiscalizacion_mis
                if ($pathinfo === '/fiscalizacion/mis') {
                    return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\FiscalizacionController::misAction',  '_route' => 'fiscalizacion_mis',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/pagos')) {
            // pagos
            if (preg_match('#^/pagos/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_pagos;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pagos')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PagosController::indexAction',));
            }
            not_pagos:

            // pagos_pagar
            if (preg_match('#^/pagos/(?P<id>\\d+)/pagar/(?P<tipo>MULTA|PROCESAMIENTO|OTORGAMIENTO|APORTE_MENSUAL)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pagos_pagar')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PagosController::pagarAction',));
            }

            // pagos_validar
            if ($pathinfo === '/pagos/validar') {
                return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PagosController::validarAction',  '_route' => 'pagos_validar',);
            }

        }

        // citas
        if (rtrim($pathinfo, '/') === '/citas') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_citas;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'citas');
            }

            return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\CitasController::indexAction',  '_route' => 'citas',);
        }
        not_citas:

        // gerente_operadora
        if (rtrim($pathinfo, '/') === '/operadoras') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_gerente_operadora;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'gerente_operadora');
            }

            return array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\OperadoraController::indexAction',  '_route' => 'gerente_operadora',);
        }
        not_gerente_operadora:

        if (0 === strpos($pathinfo, '/pdf')) {
            // pdf_verificacion
            if (0 === strpos($pathinfo, '/pdf/verificacion') && preg_match('#^/pdf/verificacion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pdf_verificacion')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PDFController::verificacionAction',));
            }

            // pdf_aceptacion
            if (0 === strpos($pathinfo, '/pdf/aceptacion') && preg_match('#^/pdf/aceptacion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pdf_aceptacion')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PDFController::aceptacionAction',));
            }

            // pdf_citacion
            if (0 === strpos($pathinfo, '/pdf/citacion') && preg_match('#^/pdf/citacion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pdf_citacion')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PDFController::citacionAction',));
            }

            // pdf_retencion
            if (0 === strpos($pathinfo, '/pdf/retencion') && preg_match('#^/pdf/retencion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pdf_retencion')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PDFController::retencionAction',));
            }

            // pdf_show
            if (preg_match('#^/pdf/(?P<id>\\d+)/(?P<type>verificacion|citacion|retencion)/show/?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_pdf_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'pdf_show');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pdf_show')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PDFController::showAction',));
            }
            not_pdf_show:

            // pdf_mercantil
            if (0 === strpos($pathinfo, '/pdf/mercantil') && preg_match('#^/pdf/mercantil/(?P<id>\\d+)/(?P<type>verificacion|citacion|retencion)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_pdf_mercantil;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pdf_mercantil')), array (  '_controller' => 'Sunahip\\FiscalizacionBundle\\Controller\\PDFController::mercantilAction',));
            }
            not_pdf_mercantil:

        }

        if (0 === strpos($pathinfo, '/operadora')) {
            // operadora
            if (rtrim($pathinfo, '/') === '/operadora') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'operadora');
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::indexAction',  '_route' => 'operadora',);
            }

            // operadora_show
            if (preg_match('#^/operadora/(?P<id>[^/]++)/detalles$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_show')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::showAction',));
            }

            if (0 === strpos($pathinfo, '/operadora/legal')) {
                // operadora_new
                if ($pathinfo === '/operadora/legal') {
                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::newAction',  '_route' => 'operadora_new',);
                }

                // operadora_create
                if ($pathinfo === '/operadora/legal_create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_operadora_create;
                    }

                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::createAction',  '_route' => 'operadora_create',);
                }
                not_operadora_create:

            }

            // operadora_new_office
            if (0 === strpos($pathinfo, '/operadora/oficina') && preg_match('#^/operadora/oficina(?:(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_new_office')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::newOfficeAction',  'tipo' => 'P',));
            }

            // operadora_create_office
            if (0 === strpos($pathinfo, '/operadora/create_oficina_oficina') && preg_match('#^/operadora/create_oficina_oficina(?:(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_operadora_create_office;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_create_office')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::createOfficeAction',  'tipo' => 'P',));
            }
            not_operadora_create_office:

            // operadora_edit
            if (preg_match('#^/operadora/(?P<id>[^/]++)/editar_oficina$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_edit')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::editAction',));
            }

            // operadora_update
            if (preg_match('#^/operadora/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_operadora_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_update')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::updateAction',));
            }
            not_operadora_update:

            // operadora_info
            if (0 === strpos($pathinfo, '/operadora/info') && preg_match('#^/operadora/info/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_info')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::getInfoAction',));
            }

            if (0 === strpos($pathinfo, '/operadora/sucursal')) {
                // operadora_sucursales
                if ($pathinfo === '/operadora/sucursales') {
                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::sucursaleslstAction',  '_route' => 'operadora_sucursales',);
                }

                // operadora_new_sucursal
                if (0 === strpos($pathinfo, '/operadora/sucursal-nueva') && preg_match('#^/operadora/sucursal\\-nueva(?:(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_new_sucursal')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::newOfficeAction',  'tipo' => 'S',));
                }

                // operadora_create_sucursal
                if (0 === strpos($pathinfo, '/operadora/sucursal_create') && preg_match('#^/operadora/sucursal_create(?:(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_operadora_create_sucursal;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_create_sucursal')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::newOfficeAction',  'tipo' => 'S',));
                }
                not_operadora_create_sucursal:

                // operadora_delete_sucursal
                if (preg_match('#^/operadora/sucursal(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_operadora_delete_sucursal;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'operadora_delete_sucursal')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataOperadoraController::sucursalDelAction',));
                }
                not_operadora_delete_sucursal:

            }

        }

        if (0 === strpos($pathinfo, '/centrohipico')) {
            // datacentrohipico_list
            if (rtrim($pathinfo, '/') === '/centrohipico') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'datacentrohipico_list');
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::indexAction',  '_route' => 'datacentrohipico_list',);
            }

            // datacentrohipico_list_partner
            if (rtrim($pathinfo, '/') === '/centrohipico/socios') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'datacentrohipico_list_partner');
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::partnerAction',  '_route' => 'datacentrohipico_list_partner',);
            }

            // datacentrohipico_show
            if (preg_match('#^/centrohipico/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datacentrohipico_show')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::showAction',));
            }

            if (0 === strpos($pathinfo, '/centrohipico/crea')) {
                if (0 === strpos($pathinfo, '/centrohipico/crea_')) {
                    // datacentrohipico_new
                    if ($pathinfo === '/centrohipico/crea_ch') {
                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::newAction',  '_route' => 'datacentrohipico_new',);
                    }

                    // datacentrohipico_new_est
                    if (rtrim($pathinfo, '/') === '/centrohipico/crea_est') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'datacentrohipico_new_est');
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::newEstablishmentAction',  '_route' => 'datacentrohipico_new_est',);
                    }

                    // datacentrohipico_new_partner
                    if (rtrim($pathinfo, '/') === '/centrohipico/crea_socio') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'datacentrohipico_new_partner');
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::newPartnerAction',  '_route' => 'datacentrohipico_new_partner',);
                    }

                }

                if (0 === strpos($pathinfo, '/centrohipico/create_')) {
                    // datacentrohipico_create
                    if ($pathinfo === '/centrohipico/create_ch') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_datacentrohipico_create;
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::createAction',  '_route' => 'datacentrohipico_create',);
                    }
                    not_datacentrohipico_create:

                    // datacentrohipico_create_est
                    if ($pathinfo === '/centrohipico/create_est/') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_datacentrohipico_create_est;
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::createEstablishmentAction',  '_route' => 'datacentrohipico_create_est',);
                    }
                    not_datacentrohipico_create_est:

                }

            }

            // datacentrohipico_edit
            if (preg_match('#^/centrohipico/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datacentrohipico_edit')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::editAction',));
            }

            // datacentrohipico_update
            if (preg_match('#^/centrohipico/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_datacentrohipico_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datacentrohipico_update')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::updateAction',));
            }
            not_datacentrohipico_update:

            // datacentrohipico_delete
            if (preg_match('#^/centrohipico/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_datacentrohipico_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datacentrohipico_delete')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataCentrohipicoController::deleteAction',));
            }
            not_datacentrohipico_delete:

        }

        if (0 === strpos($pathinfo, '/datalegal')) {
            // datalegal
            if (rtrim($pathinfo, '/') === '/datalegal') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'datalegal');
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::indexAction',  '_route' => 'datalegal',);
            }

            // datalegal_show
            if (preg_match('#^/datalegal/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datalegal_show')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::showAction',));
            }

            // datalegal_new
            if ($pathinfo === '/datalegal/new') {
                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::newAction',  '_route' => 'datalegal_new',);
            }

            // datalegal_create
            if ($pathinfo === '/datalegal/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_datalegal_create;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::createAction',  '_route' => 'datalegal_create',);
            }
            not_datalegal_create:

            // datalegal_edit
            if (preg_match('#^/datalegal/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datalegal_edit')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::editAction',));
            }

            // datalegal_update
            if (preg_match('#^/datalegal/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_datalegal_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datalegal_update')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::updateAction',));
            }
            not_datalegal_update:

            // datalegal_delete
            if (preg_match('#^/datalegal/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_datalegal_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'datalegal_delete')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataLegalController::deleteAction',));
            }
            not_datalegal_delete:

        }

        if (0 === strpos($pathinfo, '/afiliados')) {
            if (0 === strpos($pathinfo, '/afiliados/solicitud')) {
                // Centrohipico_solicitud_afiliacion
                if ($pathinfo === '/afiliados/solicitud/afiliacion') {
                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::indexAction',  '_route' => 'Centrohipico_solicitud_afiliacion',);
                }

                // Centrohipico_solicitud_desafiliacion
                if (preg_match('#^/afiliados/solicitud/(?P<id>[^/]++)/desafiliacion$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Centrohipico_solicitud_desafiliacion')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::desafiliacionAction',));
                }

                if (0 === strpos($pathinfo, '/afiliados/solicitud/afiliacion')) {
                    // Centrohipico_solicitud_afiliacion_2
                    if (preg_match('#^/afiliados/solicitud/afiliacion/(?P<rif_type>[^/]++)/(?P<rif_number>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Centrohipico_solicitud_afiliacion_2')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::doneAction',));
                    }

                    // Centrohipico_solicitud_afiliacion_guardar
                    if (rtrim($pathinfo, '/') === '/afiliados/solicitud/afiliacion/guardar') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Centrohipico_solicitud_afiliacion_guardar');
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::saveAction',  '_route' => 'Centrohipico_solicitud_afiliacion_guardar',);
                    }

                }

                if (0 === strpos($pathinfo, '/afiliados/solicitud/listado')) {
                    // Centrohipico_solicitud_afiliacion_listado
                    if (rtrim($pathinfo, '/') === '/afiliados/solicitud/listado') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Centrohipico_solicitud_afiliacion_listado');
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::listAction',  '_route' => 'Centrohipico_solicitud_afiliacion_listado',);
                    }

                    // Centrohipico_solicitud_afiliacion_listado_PDF
                    if ($pathinfo === '/afiliados/solicitud/listado-pdf') {
                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::listPdfAction',  '_route' => 'Centrohipico_solicitud_afiliacion_listado_PDF',);
                    }

                }

                // Centrohipico_solicitud_afiliacion_rechazar
                if (rtrim($pathinfo, '/') === '/afiliados/solicitud/rechazar') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Centrohipico_solicitud_afiliacion_rechazar');
                    }

                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::rejectedAction',  '_route' => 'Centrohipico_solicitud_afiliacion_rechazar',);
                }

                if (0 === strpos($pathinfo, '/afiliados/solicitud/a')) {
                    // Centrohipico_solicitud_afiliacion_aprobar
                    if (rtrim($pathinfo, '/') === '/afiliados/solicitud/aprobar') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Centrohipico_solicitud_afiliacion_aprobar');
                        }

                        return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::approveAction',  '_route' => 'Centrohipico_solicitud_afiliacion_aprobar',);
                    }

                    // Centrohipico_solicitud_afiliacion_detalles
                    if (0 === strpos($pathinfo, '/afiliados/solicitud/afiliacion/detalles') && preg_match('#^/afiliados/solicitud/afiliacion/detalles/(?P<id>[^/]++)/?$#s', $pathinfo, $matches)) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Centrohipico_solicitud_afiliacion_detalles');
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Centrohipico_solicitud_afiliacion_detalles')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::detailsAction',));
                    }

                }

            }

            // Centrohipico_solicitud_afiliacion_buscar_rif
            if ($pathinfo === '/afiliados/buscar/rif') {
                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::findByRifAction',  '_route' => 'Centrohipico_solicitud_afiliacion_buscar_rif',);
            }

            if (0 === strpos($pathinfo, '/afiliados/data/table')) {
                // Centrohipico_solicitud_afiliacion_listado_data
                if ($pathinfo === '/afiliados/data/table') {
                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\ListadoSolicitudesController::indexAction',  '_route' => 'Centrohipico_solicitud_afiliacion_listado_data',);
                }

                // Centrohipico_solicitud_afiliacion_listado_grid
                if ($pathinfo === '/afiliados/data/table/grid') {
                    return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\ListadoSolicitudesController::gridAction',  '_route' => 'Centrohipico_solicitud_afiliacion_listado_grid',);
                }

            }

            // Centrohipico_solicitud_afiliacion_valid
            if (0 === strpos($pathinfo, '/afiliados/solicitud/afiliacion/valid') && preg_match('#^/afiliados/solicitud/afiliacion/valid/(?P<centroHipico>[^/]++)/(?P<operadora>[^/]++)/(?P<clasificacion>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Centrohipico_solicitud_afiliacion_valid')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\SolicitudAfiliacionController::isValidAction',));
            }

        }

        if (0 === strpos($pathinfo, '/solicitudes')) {
            // solicitudes_listado_dt
            if ($pathinfo === '/solicitudes/listado') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_solicitudes_listado_dt;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DatatableController::indexAction',  '_route' => 'solicitudes_listado_dt',);
            }
            not_solicitudes_listado_dt:

            // solicitudes_result
            if ($pathinfo === '/solicitudes_result') {
                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DatatableController::indexResultsAction',  '_route' => 'solicitudes_result',);
            }

            // solicitudes_bulk_delete
            if ($pathinfo === '/solicitudes/bulk/delete') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_solicitudes_bulk_delete;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DatatableController::bulkDeleteAction',  '_route' => 'solicitudes_bulk_delete',);
            }
            not_solicitudes_bulk_delete:

        }

        if (0 === strpos($pathinfo, '/bulk')) {
            // solicitudes_bulk_disable
            if ($pathinfo === '/bulk/disable') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_solicitudes_bulk_disable;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DatatableController::bulkDisableAction',  '_route' => 'solicitudes_bulk_disable',);
            }
            not_solicitudes_bulk_disable:

            // solicitudes_bulk_enable
            if ($pathinfo === '/bulk/enable') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_solicitudes_bulk_enable;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DatatableController::bulkEnableAction',  '_route' => 'solicitudes_bulk_enable',);
            }
            not_solicitudes_bulk_enable:

        }

        if (0 === strpos($pathinfo, '/empresa')) {
            // data_empresa_list
            if (rtrim($pathinfo, '/') === '/empresa/listado') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_data_empresa_list;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'data_empresa_list');
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::listAction',  '_route' => 'data_empresa_list',);
            }
            not_data_empresa_list:

            // data_empresa_new
            if (rtrim($pathinfo, '/') === '/empresa') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'data_empresa_new');
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::newAction',  '_route' => 'data_empresa_new',);
            }

            // data_empresa_save
            if ($pathinfo === '/empresa/save/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_data_empresa_save;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::saveAction',  '_route' => 'data_empresa_save',);
            }
            not_data_empresa_save:

            // data_empresa_partner_save
            if ($pathinfo === '/empresa/partner/save/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_data_empresa_partner_save;
                }

                return array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::savePartnerAction',  '_route' => 'data_empresa_partner_save',);
            }
            not_data_empresa_partner_save:

            // empresa_verificar_rif
            if (0 === strpos($pathinfo, '/empresa/verificar-rif') && preg_match('#^/empresa/verificar\\-rif/(?P<rifType>[^/]++)/(?P<rifNumber>[^/]++)/?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_empresa_verificar_rif;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'empresa_verificar_rif');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'empresa_verificar_rif')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::verifiyRifAction',));
            }
            not_empresa_verificar_rif:

            // data_empresa_detail
            if (0 === strpos($pathinfo, '/empresa/detall') && preg_match('#^/empresa/detall/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_data_empresa_detail;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'data_empresa_detail')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::showAction',));
            }
            not_data_empresa_detail:

            // empresa_agregar_form_socio
            if (0 === strpos($pathinfo, '/empresa/agregar-formulario') && preg_match('#^/empresa/agregar\\-formulario/(?P<type>[^/]++)/?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_empresa_agregar_form_socio;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'empresa_agregar_form_socio');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'empresa_agregar_form_socio')), array (  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::showPartnerFormAction',));
            }
            not_empresa_agregar_form_socio:

            // data_empresa_edit
            if (0 === strpos($pathinfo, '/empresa/editar') && preg_match('#^/empresa/editar/(?P<id>[^/]++)/?$#s', $pathinfo, $matches)) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'data_empresa_edit');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'data_empresa_edit')), array (  'id' => NULL,  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::editAction',));
            }

            // data_empresa_update
            if (0 === strpos($pathinfo, '/empresa/update') && preg_match('#^/empresa/update/(?P<id>[^/]++)/$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_data_empresa_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'data_empresa_update')), array (  'id' => NULL,  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::updateAction',));
            }
            not_data_empresa_update:

            if (0 === strpos($pathinfo, '/empresa/socios')) {
                // data_empresa_add_partner
                if (preg_match('#^/empresa/socios/(?P<id>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'data_empresa_add_partner');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'data_empresa_add_partner')), array (  'id' => NULL,  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::addPartnerAction',));
                }

                // data_empresa_save_partner
                if (0 === strpos($pathinfo, '/empresa/socios/save') && preg_match('#^/empresa/socios/save/(?P<id>[^/]++)/$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_data_empresa_save_partner;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'data_empresa_save_partner')), array (  'id' => NULL,  '_controller' => 'Sunahip\\CentrohipicoBundle\\Controller\\DataEmpresaController::addPartnerSaveAction',));
                }
                not_data_empresa_save_partner:

            }

        }

        if (0 === strpos($pathinfo, '/solicitud-licencia')) {
            if (0 === strpos($pathinfo, '/solicitud-licencia/centro-hipico')) {
                // solicitudes_list
                if (rtrim($pathinfo, '/') === '/solicitud-licencia/centro-hipico') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'solicitudes_list');
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::indexAction',  '_route' => 'solicitudes_list',);
                }

                // solicitudes_citas_create
                if ($pathinfo === '/solicitud-licencia/centro-hipico/crear-cita') {
                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::newAction',  '_route' => 'solicitudes_citas_create',);
                }

                // datasolicitudes_create
                if ($pathinfo === '/solicitud-licencia/centro-hipico/generar-cita') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_datasolicitudes_create;
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::createAction',  '_route' => 'datasolicitudes_create',);
                }
                not_datasolicitudes_create:

                if (0 === strpos($pathinfo, '/solicitud-licencia/centro-hipico/dis')) {
                    // solicitudes_events
                    if ($pathinfo === '/solicitud-licencia/centro-hipico/disponible') {
                        return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DefaultController::avaibleDaysAction',  '_route' => 'solicitudes_events',);
                    }

                    // solicitudes_datepicker
                    if ($pathinfo === '/solicitud-licencia/centro-hipico/disable') {
                        return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DefaultController::DatePickerAction',  '_route' => 'solicitudes_datepicker',);
                    }

                }

                // datasolicitudes_chlist
                if ($pathinfo === '/solicitud-licencia/centro-hipico/chlist') {
                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::chlistAction',  '_route' => 'datasolicitudes_chlist',);
                }

                // datasolicitudes_autolist
                if ($pathinfo === '/solicitud-licencia/centro-hipico/autolist') {
                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::autorizaAction',  '_route' => 'datasolicitudes_autolist',);
                }

                if (0 === strpos($pathinfo, '/solicitud-licencia/centro-hipico/clasflic')) {
                    // datasolicitudes_juegoslist
                    if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/clasflic(?P<id>[^/]++)/juegos(?:\\-(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'datasolicitudes_juegoslist')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::juegosexpAction',  'tipo' => 'create',));
                    }

                    // datasolicitudes_recaudoslist
                    if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/clasflic(?P<id>[^/]++)/recaudos(?:/(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'datasolicitudes_recaudoslist')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::recaudosAction',  'tipo' => 'create',));
                    }

                }

                // datasolicitudes_operadora_establecimiento
                if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/(?P<id>[^/]++)/(?P<idCentro>[^/]++)/solicitud\\-operadora\\-establecimiento$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'datasolicitudes_operadora_establecimiento')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::operadoraEstablecimientoAction',));
                }

                if (0 === strpos($pathinfo, '/solicitud-licencia/centro-hipico/solicitud-')) {
                    // solicitudes_citas_generada
                    if (0 === strpos($pathinfo, '/solicitud-licencia/centro-hipico/solicitud-generada') && preg_match('#^/solicitud\\-licencia/centro\\-hipico/solicitud\\-generada(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_citas_generada')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::vergeneradaAction',));
                    }

                    // solicitudes_printGen
                    if (0 === strpos($pathinfo, '/solicitud-licencia/centro-hipico/solicitud-imprimir') && preg_match('#^/solicitud\\-licencia/centro\\-hipico/solicitud\\-imprimir/(?P<id>[^/]++)(?:/(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_printGen')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::PrintGeneradaAction',  'tipo' => 1,));
                    }

                }

                // solicitudes_cambiarcita
                if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/(?P<id>[^/]++)/cambiar\\-cita$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_cambiarcita')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::showcitaAction',));
                }

                // datasolicitudes_edit
                if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/(?P<id>[^/]++)/solicitud$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'datasolicitudes_edit')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::editDSchAction',));
                }

                // solicitudes_updatecita
                if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/(?P<id>[^/]++)/actualiza\\-cita/(?P<fecha>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_updatecita')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::updatecitaAction',));
                }

                // solicitudes_updatech
                if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/(?P<idds>[^/]++)/actualiza\\-centrohipico/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_updatech')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::actualizachAction',));
                }

                // solicitudes_updatelic
                if (preg_match('#^/solicitud\\-licencia/centro\\-hipico/(?P<idds>[^/]++)/actualiza\\-licencia/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_updatelic')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::actualizaLicAction',));
                }

            }

            if (0 === strpos($pathinfo, '/solicitud-licencia/operadora')) {
                // solicitudoperadora_list
                if (rtrim($pathinfo, '/') === '/solicitud-licencia/operadora') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'solicitudoperadora_list');
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::indexOperadoraAction',  '_route' => 'solicitudoperadora_list',);
                }

                // solicitudoperadora_citas_create
                if ($pathinfo === '/solicitud-licencia/operadora/crear-cita') {
                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::newOperadoraAction',  '_route' => 'solicitudoperadora_citas_create',);
                }

                // solicitudoperadora_printGen
                if (0 === strpos($pathinfo, '/solicitud-licencia/operadora/solicitud-imprimir') && preg_match('#^/solicitud\\-licencia/operadora/solicitud\\-imprimir/(?P<id>[^/]++)(?:/(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudoperadora_printGen')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::PrintGeneradaAction',  'tipo' => 2,));
                }

                // solicitudoperadora_create
                if ($pathinfo === '/solicitud-licencia/operadora/generar-cita') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_solicitudoperadora_create;
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::createOperadoraAction',  '_route' => 'solicitudoperadora_create',);
                }
                not_solicitudoperadora_create:

                // solicitudoperadora_edit
                if (preg_match('#^/solicitud\\-licencia/operadora/(?P<id>[^/]++)/solicitud$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudoperadora_edit')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::editDSopAction',));
                }

                // solicitudoperadora_update
                if (preg_match('#^/solicitud\\-licencia/operadora/(?P<id>[^/]++)/solicitud\\-act$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_solicitudoperadora_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudoperadora_update')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::updateDSopAction',));
                }
                not_solicitudoperadora_update:

                // solicitudoperadora_hipodromos
                if (preg_match('#^/solicitud\\-licencia/operadora/(?P<id>[^/]++)/solicitud\\-operadora\\-hipodromos(?:\\-(?P<tipo>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudoperadora_hipodromos')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::hipodromosAction',  'tipo' => 'create',));
                }

                // solicitudoperadora_hipodromos_update
                if (preg_match('#^/solicitud\\-licencia/operadora/(?P<id>[^/]++)/update\\-hipodromos\\-operadora$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudoperadora_hipodromos_update')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\DataSolicitudesController::hipodromosUpdateAction',));
                }

            }

            if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados')) {
                // recaudoscargados_edit
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/edit-recaudo') && preg_match('#^/solicitud\\-licencia/recaudoscargados/edit\\-recaudo(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudoscargados_edit')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\RecaudosCargadosController::editAction',));
                }

                // recaudoscargados_update
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/actualiza-recaudo') && preg_match('#^/solicitud\\-licencia/recaudoscargados/actualiza\\-recaudo(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudoscargados_update')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\RecaudosCargadosController::updateAction',));
                }

                // recaudoscargados_show
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/vertodos-recaudo') && preg_match('#^/solicitud\\-licencia/recaudoscargados/vertodos\\-recaudo(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudoscargados_show')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\RecaudosCargadosController::showAction',));
                }

                // recaudoscargados_new
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/nuevos-recaudos') && preg_match('#^/solicitud\\-licencia/recaudoscargados/nuevos\\-recaudos\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudoscargados_new')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\RecaudosCargadosController::newAction',));
                }

                // recaudoscargados_add
                if (preg_match('#^/solicitud\\-licencia/recaudoscargados/(?P<ids>[^/]++)/agregar\\-recaudo\\-(?P<rlid>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_recaudoscargados_add;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudoscargados_add')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\RecaudosCargadosController::createAction',));
                }
                not_recaudoscargados_add:

                // recaudospago_edit
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/edit-pago') && preg_match('#^/solicitud\\-licencia/recaudoscargados/edit\\-pago(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudospago_edit')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\PagoController::editAction',));
                }

                // recaudospago_new
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/nuevo-pago') && preg_match('#^/solicitud\\-licencia/recaudoscargados/nuevo\\-pago\\-(?P<idcl>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudospago_new')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\PagoController::newAction',));
                }

                // recaudospago_create
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/cargar-pago') && preg_match('#^/solicitud\\-licencia/recaudoscargados/cargar\\-pago\\-(?P<ids>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudospago_create')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\PagoController::createAction',));
                }

                // recaudospago_update
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/actualiza-pago') && preg_match('#^/solicitud\\-licencia/recaudoscargados/actualiza\\-pago(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudospago_update')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\PagoController::updateAction',));
                }

                // recaudospago_show
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/ver-pago') && preg_match('#^/solicitud\\-licencia/recaudoscargados/ver\\-pago(?P<ids>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudospago_show')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\PagoController::showAction',));
                }

                // recaudos_remove
                if (0 === strpos($pathinfo, '/solicitud-licencia/recaudoscargados/remover-recaudo') && preg_match('#^/solicitud\\-licencia/recaudoscargados/remover\\-recaudo(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recaudos_remove')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\RecaudosCargadosController::removeRecaudosAction',));
                }

            }

            if (0 === strpos($pathinfo, '/solicitud-licencia/juegosexplotados')) {
                // juegosexplotados_show
                if (0 === strpos($pathinfo, '/solicitud-licencia/juegosexplotados/ver-juegos') && preg_match('#^/solicitud\\-licencia/juegosexplotados/ver\\-juegos(?P<ids>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'juegosexplotados_show')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\JuegosExplotadosController::showAction',));
                }

                // juegosexplotados_create
                if (0 === strpos($pathinfo, '/solicitud-licencia/juegosexplotados/cargar-juegos') && preg_match('#^/solicitud\\-licencia/juegosexplotados/cargar\\-juegos(?P<ids>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'juegosexplotados_create')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\JuegosExplotadosController::createAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/adm')) {
            if (0 === strpos($pathinfo, '/admfechascitas')) {
                // nocitafecha
                if (rtrim($pathinfo, '/') === '/admfechascitas') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'nocitafecha');
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::indexAction',  '_route' => 'nocitafecha',);
                }

                // nocitafecha_show
                if (preg_match('#^/admfechascitas/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'nocitafecha_show')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::showAction',));
                }

                // nocitafecha_new
                if ($pathinfo === '/admfechascitas/new') {
                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::newAction',  '_route' => 'nocitafecha_new',);
                }

                // nocitafecha_create
                if ($pathinfo === '/admfechascitas/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_nocitafecha_create;
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::createAction',  '_route' => 'nocitafecha_create',);
                }
                not_nocitafecha_create:

                // nocitafecha_edit
                if (preg_match('#^/admfechascitas/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'nocitafecha_edit')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::editAction',));
                }

                // nocitafecha_update
                if (preg_match('#^/admfechascitas/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_nocitafecha_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'nocitafecha_update')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::updateAction',));
                }
                not_nocitafecha_update:

                // nocitafecha_delete
                if (preg_match('#^/admfechascitas/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_nocitafecha_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'nocitafecha_delete')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::deleteAction',));
                }
                not_nocitafecha_delete:

                // nocitafecha_maxcita
                if (0 === strpos($pathinfo, '/admfechascitas/maxcitaxdia') && preg_match('#^/admfechascitas/maxcitaxdia(?:/(?P<max>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'nocitafecha_maxcita')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdmFechasCitasController::maxcitaAction',  'max' => 5,));
                }

            }

            if (0 === strpos($pathinfo, '/admin')) {
                // fiscal_citas_listado
                if (0 === strpos($pathinfo, '/admin/licencias') && preg_match('#^/admin/licencias/(?P<tipo>[^/\\-]++)\\-listado$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscal_citas_listado')), array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminCitasController::listAction',));
                }

                if (0 === strpos($pathinfo, '/admin/citas_asignadas')) {
                    // fiscal_citas_modal_info
                    if (0 === strpos($pathinfo, '/admin/citas_asignadas/info') && preg_match('#^/admin/citas_asignadas/info(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscal_citas_modal_info')), array (  'id' => NULL,  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminCitasController::getModalInfoAction',));
                    }

                    // fiscal_citas_guardar_status
                    if (0 === strpos($pathinfo, '/admin/citas_asignadas/reacudos_status') && preg_match('#^/admin/citas_asignadas/reacudos_status(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscal_citas_guardar_status')), array (  'id' => NULL,  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminCitasController::setStatusDocumentAction',));
                    }

                }

                // gerente_consulta_licencia
                if (rtrim($pathinfo, '/') === '/admin/licencias/consulta') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'gerente_consulta_licencia');
                    }

                    return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminCitasController::searchAction',  '_route' => 'gerente_consulta_licencia',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/gerente/licencias')) {
            if (0 === strpos($pathinfo, '/gerente/licencias_')) {
                if (0 === strpos($pathinfo, '/gerente/licencias_revisadas')) {
                    // gerente_modal_info
                    if (0 === strpos($pathinfo, '/gerente/licencias_revisadas/info') && preg_match('#^/gerente/licencias_revisadas/info(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerente_modal_info')), array (  'id' => NULL,  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminLicenciasController::getModalInfoAction',));
                    }

                    // gerente_guardar_status
                    if (0 === strpos($pathinfo, '/gerente/licencias_revisadas/reacudos_status') && preg_match('#^/gerente/licencias_revisadas/reacudos_status(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerente_guardar_status')), array (  'id' => NULL,  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminLicenciasController::setStatusDocumentAction',));
                    }

                }

                // asesor_guardar_status
                if (0 === strpos($pathinfo, '/gerente/licencias_aprobadas/reacudos_status') && preg_match('#^/gerente/licencias_aprobadas/reacudos_status(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'asesor_guardar_status')), array (  'id' => NULL,  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminLicenciasController::setStatusAsesorAction',));
                }

            }

            // solicitudes_aprobadas_listado
            if ($pathinfo === '/gerente/licencias/listado/aprobadas') {
                return array (  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminLicenciasController::listAction',  '_route' => 'solicitudes_aprobadas_listado',);
            }

            // solicitudes_aprobadas_imprimir
            if (0 === strpos($pathinfo, '/gerente/licencias/aprobada/imprimir') && preg_match('#^/gerente/licencias/aprobada/imprimir(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'solicitudes_aprobadas_imprimir')), array (  'id' => NULL,  '_controller' => 'Sunahip\\SolicitudesCitasBundle\\Controller\\AdminLicenciasController::printAction',));
            }

        }

        if (0 === strpos($pathinfo, '/licencia')) {
            if (0 === strpos($pathinfo, '/licencia/adm')) {
                if (0 === strpos($pathinfo, '/licencia/admclasf')) {
                    if (0 === strpos($pathinfo, '/licencia/admclasfestab')) {
                        // admclasfestab
                        if (rtrim($pathinfo, '/') === '/licencia/admclasfestab') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasfestab;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'admclasfestab');
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::indexAction',  '_route' => 'admclasfestab',);
                        }
                        not_admclasfestab:

                        // admclasfestab_create
                        if ($pathinfo === '/licencia/admclasfestab/') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_admclasfestab_create;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::createAction',  '_route' => 'admclasfestab_create',);
                        }
                        not_admclasfestab_create:

                        // admclasfestab_new
                        if ($pathinfo === '/licencia/admclasfestab/new') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasfestab_new;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::newAction',  '_route' => 'admclasfestab_new',);
                        }
                        not_admclasfestab_new:

                        // admclasfestab_show
                        if (preg_match('#^/licencia/admclasfestab/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasfestab_show;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasfestab_show')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::showAction',));
                        }
                        not_admclasfestab_show:

                        // admclasfestab_edit
                        if (preg_match('#^/licencia/admclasfestab/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasfestab_edit;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasfestab_edit')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::editAction',));
                        }
                        not_admclasfestab_edit:

                        // admclasfestab_update
                        if (preg_match('#^/licencia/admclasfestab/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_admclasfestab_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasfestab_update')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::updateAction',));
                        }
                        not_admclasfestab_update:

                        // admclasfestab_delete
                        if (preg_match('#^/licencia/admclasfestab/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_admclasfestab_delete;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasfestab_delete')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfEstabController::deleteAction',));
                        }
                        not_admclasfestab_delete:

                    }

                    if (0 === strpos($pathinfo, '/licencia/admclasflicencias')) {
                        // admclasflicencias
                        if (rtrim($pathinfo, '/') === '/licencia/admclasflicencias') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasflicencias;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'admclasflicencias');
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::indexAction',  '_route' => 'admclasflicencias',);
                        }
                        not_admclasflicencias:

                        // admclasflicencias_create
                        if ($pathinfo === '/licencia/admclasflicencias/') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_admclasflicencias_create;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::createAction',  '_route' => 'admclasflicencias_create',);
                        }
                        not_admclasflicencias_create:

                        // admclasflicencias_new
                        if ($pathinfo === '/licencia/admclasflicencias/new') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasflicencias_new;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::newAction',  '_route' => 'admclasflicencias_new',);
                        }
                        not_admclasflicencias_new:

                        // admclasflicencias_show
                        if (preg_match('#^/licencia/admclasflicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasflicencias_show;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasflicencias_show')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::showAction',));
                        }
                        not_admclasflicencias_show:

                        // admclasflicencias_edit
                        if (preg_match('#^/licencia/admclasflicencias/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admclasflicencias_edit;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasflicencias_edit')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::editAction',));
                        }
                        not_admclasflicencias_edit:

                        // admclasflicencias_update
                        if (preg_match('#^/licencia/admclasflicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_admclasflicencias_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasflicencias_update')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::updateAction',));
                        }
                        not_admclasflicencias_update:

                        // admclasflicencias_delete
                        if (preg_match('#^/licencia/admclasflicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_admclasflicencias_delete;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admclasflicencias_delete')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmClasfLicenciasController::deleteAction',));
                        }
                        not_admclasflicencias_delete:

                    }

                }

                if (0 === strpos($pathinfo, '/licencia/admjuegosexplotados')) {
                    // admjuegosexplotados
                    if (rtrim($pathinfo, '/') === '/licencia/admjuegosexplotados') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admjuegosexplotados;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admjuegosexplotados');
                        }

                        return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::indexAction',  '_route' => 'admjuegosexplotados',);
                    }
                    not_admjuegosexplotados:

                    // admjuegosexplotados_create
                    if ($pathinfo === '/licencia/admjuegosexplotados/') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_admjuegosexplotados_create;
                        }

                        return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::createAction',  '_route' => 'admjuegosexplotados_create',);
                    }
                    not_admjuegosexplotados_create:

                    // admjuegosexplotados_new
                    if ($pathinfo === '/licencia/admjuegosexplotados/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admjuegosexplotados_new;
                        }

                        return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::newAction',  '_route' => 'admjuegosexplotados_new',);
                    }
                    not_admjuegosexplotados_new:

                    // admjuegosexplotados_show
                    if (preg_match('#^/licencia/admjuegosexplotados/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admjuegosexplotados_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admjuegosexplotados_show')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::showAction',));
                    }
                    not_admjuegosexplotados_show:

                    // admjuegosexplotados_edit
                    if (preg_match('#^/licencia/admjuegosexplotados/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admjuegosexplotados_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admjuegosexplotados_edit')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::editAction',));
                    }
                    not_admjuegosexplotados_edit:

                    // admjuegosexplotados_update
                    if (preg_match('#^/licencia/admjuegosexplotados/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_admjuegosexplotados_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admjuegosexplotados_update')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::updateAction',));
                    }
                    not_admjuegosexplotados_update:

                    // admjuegosexplotados_delete
                    if (preg_match('#^/licencia/admjuegosexplotados/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admjuegosexplotados_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admjuegosexplotados_delete')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmJuegosExplotadosController::deleteAction',));
                    }
                    not_admjuegosexplotados_delete:

                }

                if (0 === strpos($pathinfo, '/licencia/admrecaudoslicencias')) {
                    // admrecaudoslicencias
                    if (rtrim($pathinfo, '/') === '/licencia/admrecaudoslicencias') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admrecaudoslicencias;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admrecaudoslicencias');
                        }

                        return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::indexAction',  '_route' => 'admrecaudoslicencias',);
                    }
                    not_admrecaudoslicencias:

                    // admrecaudoslicencias_create
                    if ($pathinfo === '/licencia/admrecaudoslicencias/') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_admrecaudoslicencias_create;
                        }

                        return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::createAction',  '_route' => 'admrecaudoslicencias_create',);
                    }
                    not_admrecaudoslicencias_create:

                    // admrecaudoslicencias_new
                    if ($pathinfo === '/licencia/admrecaudoslicencias/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admrecaudoslicencias_new;
                        }

                        return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::newAction',  '_route' => 'admrecaudoslicencias_new',);
                    }
                    not_admrecaudoslicencias_new:

                    // admrecaudoslicencias_show
                    if (preg_match('#^/licencia/admrecaudoslicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admrecaudoslicencias_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admrecaudoslicencias_show')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::showAction',));
                    }
                    not_admrecaudoslicencias_show:

                    // admrecaudoslicencias_edit
                    if (preg_match('#^/licencia/admrecaudoslicencias/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admrecaudoslicencias_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admrecaudoslicencias_edit')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::editAction',));
                    }
                    not_admrecaudoslicencias_edit:

                    // admrecaudoslicencias_update
                    if (preg_match('#^/licencia/admrecaudoslicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_admrecaudoslicencias_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admrecaudoslicencias_update')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::updateAction',));
                    }
                    not_admrecaudoslicencias_update:

                    // admrecaudoslicencias_delete
                    if (preg_match('#^/licencia/admrecaudoslicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admrecaudoslicencias_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admrecaudoslicencias_delete')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmRecaudosLicenciasController::deleteAction',));
                    }
                    not_admrecaudoslicencias_delete:

                }

                if (0 === strpos($pathinfo, '/licencia/admtipo')) {
                    if (0 === strpos($pathinfo, '/licencia/admtipoaporte')) {
                        // admtipoaporte
                        if (rtrim($pathinfo, '/') === '/licencia/admtipoaporte') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtipoaporte;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'admtipoaporte');
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::indexAction',  '_route' => 'admtipoaporte',);
                        }
                        not_admtipoaporte:

                        // admtipoaporte_create
                        if ($pathinfo === '/licencia/admtipoaporte/') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_admtipoaporte_create;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::createAction',  '_route' => 'admtipoaporte_create',);
                        }
                        not_admtipoaporte_create:

                        // admtipoaporte_new
                        if ($pathinfo === '/licencia/admtipoaporte/new') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtipoaporte_new;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::newAction',  '_route' => 'admtipoaporte_new',);
                        }
                        not_admtipoaporte_new:

                        // admtipoaporte_show
                        if (preg_match('#^/licencia/admtipoaporte/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtipoaporte_show;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtipoaporte_show')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::showAction',));
                        }
                        not_admtipoaporte_show:

                        // admtipoaporte_edit
                        if (preg_match('#^/licencia/admtipoaporte/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtipoaporte_edit;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtipoaporte_edit')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::editAction',));
                        }
                        not_admtipoaporte_edit:

                        // admtipoaporte_update
                        if (preg_match('#^/licencia/admtipoaporte/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_admtipoaporte_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtipoaporte_update')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::updateAction',));
                        }
                        not_admtipoaporte_update:

                        // admtipoaporte_delete
                        if (preg_match('#^/licencia/admtipoaporte/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_admtipoaporte_delete;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtipoaporte_delete')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTipoAporteController::deleteAction',));
                        }
                        not_admtipoaporte_delete:

                    }

                    if (0 === strpos($pathinfo, '/licencia/admtiposlicencias')) {
                        // admtiposlicencias
                        if (rtrim($pathinfo, '/') === '/licencia/admtiposlicencias') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtiposlicencias;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'admtiposlicencias');
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::indexAction',  '_route' => 'admtiposlicencias',);
                        }
                        not_admtiposlicencias:

                        // admtiposlicencias_create
                        if ($pathinfo === '/licencia/admtiposlicencias/') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_admtiposlicencias_create;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::createAction',  '_route' => 'admtiposlicencias_create',);
                        }
                        not_admtiposlicencias_create:

                        // admtiposlicencias_new
                        if ($pathinfo === '/licencia/admtiposlicencias/new') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtiposlicencias_new;
                            }

                            return array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::newAction',  '_route' => 'admtiposlicencias_new',);
                        }
                        not_admtiposlicencias_new:

                        // admtiposlicencias_show
                        if (preg_match('#^/licencia/admtiposlicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtiposlicencias_show;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtiposlicencias_show')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::showAction',));
                        }
                        not_admtiposlicencias_show:

                        // admtiposlicencias_edit
                        if (preg_match('#^/licencia/admtiposlicencias/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_admtiposlicencias_edit;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtiposlicencias_edit')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::editAction',));
                        }
                        not_admtiposlicencias_edit:

                        // admtiposlicencias_update
                        if (preg_match('#^/licencia/admtiposlicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_admtiposlicencias_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtiposlicencias_update')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::updateAction',));
                        }
                        not_admtiposlicencias_update:

                        // admtiposlicencias_delete
                        if (preg_match('#^/licencia/admtiposlicencias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_admtiposlicencias_delete;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admtiposlicencias_delete')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\AdmTiposLicenciasController::deleteAction',));
                        }
                        not_admtiposlicencias_delete:

                    }

                }

            }

            // sunahip_licencia_default_index
            if (0 === strpos($pathinfo, '/licencia/hello') && preg_match('#^/licencia/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sunahip_licencia_default_index')), array (  '_controller' => 'Sunahip\\LicenciaBundle\\Controller\\DefaultController::indexAction',));
            }

        }

        if (0 === strpos($pathinfo, '/ciudades')) {
            // ciudades
            if (preg_match('#^/ciudades(?:/(?P<estado_id>[^/]++))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_ciudades;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ciudades')), array (  'estado_id' => NULL,  '_controller' => 'Sunahip\\CommonBundle\\Controller\\CiudadController::getCiudadesAction',));
            }
            not_ciudades:

            // ciudades_select
            if (0 === strpos($pathinfo, '/ciudades/select') && preg_match('#^/ciudades/select(?:/(?P<estado_id>[^/]++))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_ciudades_select;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ciudades_select')), array (  'estado_id' => NULL,  '_controller' => 'Sunahip\\CommonBundle\\Controller\\CiudadController::getCiudadesSelectAction',));
            }
            not_ciudades_select:

        }

        // estados
        if ($pathinfo === '/estados') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_estados;
            }

            return array (  '_controller' => 'Sunahip\\CommonBundle\\Controller\\EstadoController::getEstadosAction',  '_route' => 'estados',);
        }
        not_estados:

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'Sunahip\\CommonBundle\\Controller\\HomeController::indexAction',  '_route' => 'home',);
        }

        // municipios
        if (0 === strpos($pathinfo, '/municipios') && preg_match('#^/municipios(?:/(?P<estado_id>[^/]++))?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_municipios;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'municipios')), array (  'estado_id' => NULL,  '_controller' => 'Sunahip\\CommonBundle\\Controller\\MunicipioController::getMunicipiosAction',));
        }
        not_municipios:

        // parroquias
        if (0 === strpos($pathinfo, '/parroquias') && preg_match('#^/parroquias(?:/(?P<municipio_id>[^/]++))?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_parroquias;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'parroquias')), array (  'municipio_id' => NULL,  '_controller' => 'Sunahip\\CommonBundle\\Controller\\ParroquiaController::getParroquiasAction',));
        }
        not_parroquias:

        if (0 === strpos($pathinfo, '/listado')) {
            // listado
            if ($pathinfo === '/listado') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::listadoAction',  '_route' => 'listado',);
            }

            // listado_json
            if ($pathinfo === '/listado_json') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::listadoJsonAction',  '_route' => 'listado_json',);
            }

        }

        if (0 === strpos($pathinfo, '/nuevo-usuario')) {
            // nuevo-usuario
            if ($pathinfo === '/nuevo-usuario') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::nuevoUsuarioAction',  '_route' => 'nuevo-usuario',);
            }

            // guardar-nuevo-usuario
            if ($pathinfo === '/nuevo-usuario/guardar') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardarnuevousuario;
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::guardarAction',  '_route' => 'guardar-nuevo-usuario',);
            }
            not_guardarnuevousuario:

        }

        // editar-usuario
        if (0 === strpos($pathinfo, '/editar-usuario') && preg_match('#^/editar\\-usuario/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editar-usuario')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::editarUsuarioAction',));
        }

        // actualizar-usuario
        if (0 === strpos($pathinfo, '/actualizar-usuario') && preg_match('#^/actualizar\\-usuario/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_actualizarusuario;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'actualizar-usuario')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::actualizarUsuarioAction',));
        }
        not_actualizarusuario:

        // cambiar-estado-usuario
        if (0 === strpos($pathinfo, '/cambiar-estado-usuario') && preg_match('#^/cambiar\\-estado\\-usuario/(?P<user_id>[^/]++)/(?P<estado_id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_cambiarestadousuario;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cambiar-estado-usuario')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::cambiarEstadoAction',));
        }
        not_cambiarestadousuario:

        // eliminar-usuario
        if (0 === strpos($pathinfo, '/eliminar-usuario') && preg_match('#^/eliminar\\-usuario/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_eliminarusuario;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'eliminar-usuario')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::eliminarUsuarioAction',));
        }
        not_eliminarusuario:

        // ver_usuario
        if (0 === strpos($pathinfo, '/ver') && preg_match('#^/ver/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_ver_usuario;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ver_usuario')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\AdminController::showAction',));
        }
        not_ver_usuario:

        if (0 === strpos($pathinfo, '/us')) {
            // usuarios_listado_dt
            if ($pathinfo === '/usuarios/listado') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_usuarios_listado_dt;
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatatableController::indexAction',  '_route' => 'usuarios_listado_dt',);
            }
            not_usuarios_listado_dt:

            // user_results
            if ($pathinfo === '/user_results') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatatableController::indexResultsAction',  '_route' => 'user_results',);
            }

        }

        if (0 === strpos($pathinfo, '/bulk')) {
            if (0 === strpos($pathinfo, '/bulk/d')) {
                // user_bulk_delete
                if ($pathinfo === '/bulk/delete') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_user_bulk_delete;
                    }

                    return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatatableController::bulkDeleteAction',  '_route' => 'user_bulk_delete',);
                }
                not_user_bulk_delete:

                // user_bulk_disable
                if ($pathinfo === '/bulk/disable') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_user_bulk_disable;
                    }

                    return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatatableController::bulkDisableAction',  '_route' => 'user_bulk_disable',);
                }
                not_user_bulk_disable:

            }

            // user_bulk_enable
            if ($pathinfo === '/bulk/enable') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_bulk_enable;
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatatableController::bulkEnableAction',  '_route' => 'user_bulk_enable',);
            }
            not_user_bulk_enable:

        }

        // editar-datos-basicos
        if ($pathinfo === '/editar/datos-basicos') {
            return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatosBasicosController::editDatosBasicosAction',  '_route' => 'editar-datos-basicos',);
        }

        if (0 === strpos($pathinfo, '/datos-basicos')) {
            // datos-basicos
            if ($pathinfo === '/datos-basicos') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatosBasicosController::datosBasicosAction',  '_route' => 'datos-basicos',);
            }

            // guardar-datos-basicos
            if ($pathinfo === '/datos-basicos/guardar') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardardatosbasicos;
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\DatosBasicosController::guardarAction',  '_route' => 'guardar-datos-basicos',);
            }
            not_guardardatosbasicos:

        }

        if (0 === strpos($pathinfo, '/fiscal')) {
            // fiscal_list
            if ($pathinfo === '/fiscal/listado') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\FiscalController::listAction',  '_route' => 'fiscal_list',);
            }

            // fiscal-change-status
            if (0 === strpos($pathinfo, '/fiscal/cambiar-estado') && preg_match('#^/fiscal/cambiar\\-estado/(?P<user_id>[^/]++)/(?P<status_id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fiscalchangestatus;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscal-change-status')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\FiscalController::cambiarEstadoAction',));
            }
            not_fiscalchangestatus:

            // fiscal_list_requests
            if (rtrim($pathinfo, '/') === '/fiscal/listado/gestiones') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fiscal_list_requests');
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\FiscalController::listWithRequestsAction',  '_route' => 'fiscal_list_requests',);
            }

            if (0 === strpos($pathinfo, '/fiscal/modal-')) {
                // fiscal-modal-requests
                if (0 === strpos($pathinfo, '/fiscal/modal-gestiones') && preg_match('#^/fiscal/modal\\-gestiones/(?P<user_id>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fiscalmodalrequests;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fiscal-modal-requests');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscal-modal-requests')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\FiscalController::fiscalModalRequestsAction',));
                }
                not_fiscalmodalrequests:

                // fiscal-modal-requests2
                if (0 === strpos($pathinfo, '/fiscal/modal-fiscalizaciones') && preg_match('#^/fiscal/modal\\-fiscalizaciones/(?P<user_id>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fiscalmodalrequests2;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fiscal-modal-requests2');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fiscal-modal-requests2')), array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\FiscalController::fiscalModalRequests2Action',));
                }
                not_fiscalmodalrequests2:

            }

        }

        // notification
        if (rtrim($pathinfo, '/') === '/notificaciones') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_notification;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'notification');
            }

            return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\NotificationController::indexAction',  '_route' => 'notification',);
        }
        not_notification:

        // notification_count
        if (rtrim($pathinfo, '/') === '/usuarios/contador_notificaciones') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_notification_count;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'notification_count');
            }

            return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\NotificationController::countAction',  '_route' => 'notification_count',);
        }
        not_notification_count:

        if (0 === strpos($pathinfo, '/registro')) {
            // registro
            if ($pathinfo === '/registro') {
                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\RegistroController::signupAction',  '_route' => 'registro',);
            }

            // guardar_registro
            if ($pathinfo === '/registro/guardar') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardar_registro;
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\RegistroController::registroAction',  '_route' => 'guardar_registro',);
            }
            not_guardar_registro:

            // check_user_registro
            if ($pathinfo === '/registro/check-user') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_check_user_registro;
                }

                return array (  '_controller' => 'Sunahip\\UserBundle\\Controller\\RegistroController::checkUser',  '_route' => 'check_user_registro',);
            }
            not_check_user_registro:

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/group')) {
            // fos_user_group_list
            if ($pathinfo === '/group/list') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_list;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::listAction',  '_route' => 'fos_user_group_list',);
            }
            not_fos_user_group_list:

            // fos_user_group_new
            if ($pathinfo === '/group/new') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::newAction',  '_route' => 'fos_user_group_new',);
            }

            // fos_user_group_show
            if (preg_match('#^/group/(?P<groupName>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_group_show')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::showAction',));
            }
            not_fos_user_group_show:

            // fos_user_group_edit
            if (preg_match('#^/group/(?P<groupName>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_group_edit')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::editAction',));
            }

            // fos_user_group_delete
            if (preg_match('#^/group/(?P<groupName>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_group_delete')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::deleteAction',));
            }
            not_fos_user_group_delete:

        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_js_routing_js')), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',));
        }

        // endroid_qrcode
        if (0 === strpos($pathinfo, '/qrcode') && preg_match('#^/qrcode/(?P<text>[\\w\\W]+)\\.(?P<extension>jpg|png|gif)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'endroid_qrcode')), array (  '_controller' => 'Endroid\\Bundle\\QrCodeBundle\\Controller\\QrCodeController::generateAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
