<?php

/* SolicitudesCitasBundle:DataSolicitudes:crearOperadora.html.twig */
class __TwigTemplate_eeb72cd212047fd6f238c0588643f45618b945e7e0e48d256ce41edb98bfcbc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"contendor\">
        <div class=\"block-separator col-md-12\"></div>
\t<div class=\"tit_principal\">Solicitud Licencia Operadora</div>
\t\t<br /><br /><br />
                 <div class=\"col-md-12\">
                    <div id=\"texto\">Estimado Usuario, Para solicitar una cita, deber&aacute; antes adjuntar todos los recaudos, seg&uacute;n la licencia que solicite. Por cada Licencia deber&aacute; realizar una solicitud.</div>
                    <div id=\"texto\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios.</div>
                    <div style=\"clear:both;\"></div>
                 </div>  
           <div class=\"col-md-12\">
                <div class=\"form-group\">
                  ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessage"]) {
            // line 15
            echo "                     <div class=\"alert alert-";
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
            echo " \">
                         <button class=\"close\" data-dismiss=\"alert\" type=\"button\"></button>
                         ";
            // line 17
            if ($this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : null), "title", array(), "any", true, true)) {
                // line 18
                echo "                         <strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "title"), "html", null, true);
                echo "</strong>
                         ";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "message"), "html", null, true);
                echo "
                         ";
            } else {
                // line 21
                echo "                         ";
                echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
                echo "
                         ";
            }
            // line 23
            echo "                     </div>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                </div>
            </div>
           <div class=\"col-md-12\">
            ";
        // line 28
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "form_dslo")));
        echo "        
\t    <div id=\"accordion\" class=\"\">
\t\t<h3>1. Seleccionar Licencia para Operadora</h3>
\t\t<div>
                       <div class=\"col-md-4\">Tipo de Licencia <span class=\"oblig\">(*)</span></div>
                        <div class=\"col-md-4\">
                           ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ClasLicencia"), 'widget', array("attr" => array("class" => "licencia"), "id" => "alic"));
        echo "
                           ";
        // line 35
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ClasLicencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 36
            echo "                           <span class=\"help-block \">
                            ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ClasLicencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 40
        echo "                        </div>
\t\t</div>

\t\t<h3>2. Seleccione los Juegos a Explotar</h3>
\t\t<div>
                        <div class=\"col-md-3\">Juegos a Explotar <span class=\"oblig\">(*)</span></div>
                        <div id=\"juegos_explotar\" class=\"col-md-7\"><p>Para ver Lista: Seleccione Tipo de Licencia</p></div>
\t\t</div>
\t\t<h3>3. Seleccionar Los hipódromos Internacionales en los que prestará señal</h3>
\t\t<div id=\"data_hipointer\">
                        <div class=\"col-md-4\"><p>Para ver Seleccione Tipo de Licencia</p></div>
                        <div class=\"col-md-6\">
                            &nbsp;
                        </div>
\t\t</div>
\t\t<h3>4. Adjuntar Recaudos</h3>
\t\t<div id=\"lista_recaudos\">
                    <p>Para ver: Seleccione Tipo de Licencia </p>
\t\t</div>
\t\t<h3>5. Pago por Procesamiento</h3>
                <div id=\"pago_procesamiento\">";
        // line 61
        echo "                    <p>Para ver: Seleccione Tipo de Licencia</p>
\t\t</div>

              <h3>6. Seleccionar D&iacute;a de Cita</h3>
              <div>
                 <div class=\"col-md-3\"><label for=\"date\">Fecha de Cita <span class=\"oblig\">(*)</span> :</label> <br/>
                   <input type=\"text\" id=\"date\" name=\"datacita\" placeholder=\"Fecha dd/mm/aaaa\" class=\"\" size=\"13px;\" readonly/> <div id=\"error\"></div>
                 </div>
                 <div class=\"col-md-4 \"><div id=\"datepick\"></div> </div>
                 <div class=\"col-md-4 col-offset-2 alert-warning\">
                     Debe seleccionar alguno de los Días Activos. A partir del Día de mañana hasta 15 días para seleccionar la fecha de la cita
                 </div>
\t\t</div><!--  FIN TABS 4 -->
        </div>
        <div class=\"block-separator col-md-12\"></div>
        <div class=\"col-md-offset-8 col-md-4 form-group text-right\">
                <a href=\"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("solicitudoperadora_list");
        echo "\" class=\"btn btn-warning btn-sm\">Cancelar</a>
                <a href=\"#\" id=\"gensol\" class=\"btn btn-primary btn-sm\">Generar Solicitud</a>
        </div>
         ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
       </form>  <!-- End form-->
      </div>                 
        <div class=\"block-separator col-md-12\"></div> 
</div>
<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12 alert text-left\" id=\"myMessage\">
                        
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-primary btn-sm\" data-dismiss=\"modal\">Aceptar</button>
            </div>
        </div>
    </div>
</div>                                
";
    }

    // line 107
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 108
        echo "        ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
<script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/js/solfun.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\">
    \$(document).ready(function() {
        ActiveCalendar();
        \$( \"#accordion\" ).accordion({
            heightStyle: \"content\"
        });   
        \$(\"#gensol\").on(\"click\",function(){
            GenerarSolicitud();
        });   
        \$(\"#alic\").change(function(){
            var id=\$(\"#alic option:selected\").val();
            if(id != \"\") {         
                // Hipodromos internacionales
                getRoute(\"#data_hipointer\",'solicitudoperadora_hipodromos',id);
                // Juegos a Explotar
                getRoute(\"#juegos_explotar\",'datasolicitudes_juegoslist',id);
                //Recaudos
                getRoute(\"#lista_recaudos\",'datasolicitudes_recaudoslist',id);
            } else {
                \$(\"#juegos_explotar\").html(\"<p>Para ver Lista: Seleccione Tipo de Licencia</p>\");
                \$(\"#lista_recaudos\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                \$(\"#pago_procesamiento\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
            }
        });
    });

    function ValidaForm(){
        var oform = \$(\"#form_dslo\");
        var resp=1, msg=new Array();
        var arrdate=\$( \"#date\" ).val().split(\"/\");
              var datets = Date.parse(arrdate[2]+\"-\"+arrdate[1]+\"-\"+arrdate[0]);
              if(isNaN(datets)) {
                  msg.push(\"- Debe Seleccionar una Fecha para la Cita <br/>\");
                  //\$(\"#myMessage\").html(\"Debe Seleccionar una Fecha para la Cita\");
                  //\$(\"#myModal\").modal(\"show\");
                  //return false;  
                  resp=0;
            }        
        \$(\"input:file\").each(function(index,elem){
           if (\$(this).val()==='') {resp=0;msg.push(\"- Faltan Documentos de Recaudos<br/>\");return false;} 
        });
        /* if(\$('#juegoe').length){
            if(\$(\"#juegoe option:selected\").val()==='') resp=0;
        }*/
        if(\$('#hipointer').length){            
            if(\$(\"#hipointer\").is(\"textarea\")===true) {
                if(\$(\"#hipointer\").val()==='') {
                    resp=0;msg.push(\"- Falta Hipódromo Internacional<br/>\");
                }
            }
        }
        \$(\".date\").each(function(index,elem){
           if (!\$(this).hasClass('disabled') && \$(this).val()==='')
           {resp=0;msg.push(\"- Falta Fecha de los Recaudos<br/>\");return false;}
        });
        if(\$(\"#alic option:selected\").val()==='') {resp=0;msg.push(\"- Falta Licencia<br/>\");}
        if(\$(\"#banco option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione un Banco<br/>\");}
        if(\$(\"#reciboNP\").val()==='' || parseInt(\$(\"#reciboNP\").val())===0) {resp=0;msg.push(\"-Falta el Número del Recibo<br/>\");}

        //Dialog('<div class=\"col-md-12 alert text-left\"> Aun Faltan Campos Por Rellenar en la Solicitud:<br/>'+msg.toString().replace(\",\", \" \")+' </div>','ERROR');
        \$(\"#myMessage\").html(\"Aun Faltan Campos Por Rellenar en la Solicitud:<br/>\"+msg.toString().replace(',', '').replace(',', ''));
        \$(\"#myModal\").modal(\"show\");
        return resp===1?true:false;  
    }
</script>          
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:crearOperadora.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 109,  189 => 108,  186 => 107,  155 => 80,  149 => 77,  131 => 61,  109 => 40,  103 => 37,  100 => 36,  98 => 35,  94 => 34,  85 => 28,  80 => 25,  73 => 23,  67 => 21,  62 => 19,  57 => 18,  55 => 17,  49 => 15,  45 => 14,  32 => 3,  29 => 2,);
    }
}
