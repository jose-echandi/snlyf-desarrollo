<?php

/* SolicitudesCitasBundle:DataSolicitudes:editCH2.html.twig */
class __TwigTemplate_f4af68663686a052c307f037318624227e94ee190c7c8b05868d523c7c751b45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"contendor\" style=\"padding: 0px !important\">
        <div class=\"block-separator col-md-12\"></div>
\t<h2 class=\"tit_principal\">Solicitud Licencia Centro Hípico</h2>
\t\t<br /><br /><br />
              <div class=\"col-md-12\">
\t\t<div id=\"texto\">Estimado Usuario, Para solicitar una cita, deber&aacute; antes adjuntar todos los recaudos, seg&uacute;n la licencia que solicite. Por cada Licencia deber&aacute; realizar una solicitud.</div>
\t\t<div id=\"texto\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios.</div>
\t\t<div style=\"clear:both;\"></div>
               </div>   
           <div class=\"col-md-12\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "  
                    </div>
                     ";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessage"]) {
            // line 18
            echo "                        <div class=\"alert alert-";
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
            echo " \">
                            <button class=\"close\" data-dismiss=\"alert\" type=\"button\"></button>
                            ";
            // line 20
            if ($this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : null), "title", array(), "any", true, true)) {
                // line 21
                echo "                            <strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "title"), "html", null, true);
                echo "</strong>
                            ";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "message"), "html", null, true);
                echo "
                            ";
            } else {
                // line 24
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
                echo "
                            ";
            }
            // line 26
            echo "                        </div>
                     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "                </div>
            ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "form_dsch")));
        echo "        
\t    <div id=\"accordion\" class=\"\">
\t\t<h3>1. Seleccione el Centro H&iacute;pico</h3>
\t\t<div>
\t\t     <div class=\"text-center col-lg-4\">Centro Hipico <span class=\"oblig\">(*)</span></div>
\t\t     ";
        // line 35
        echo "                        <div class=\"text-center col-md-4\" id=\"D_centroh\" >
                            <div id=\"V_centroh\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "centroHipico"), "DenominacionComercial"), "html", null, true);
        echo "</div>&nbsp; <button class=\"btn btn-warning\" onclick=\"cambiar('centroh',true);\">Cambiar</button>
                        </div>   
                        <div class=\"text-center col-md-4\" id=\"F_centroh\" style=\"display:none;\">
                                ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centroHipico"), 'widget', array("attr" => array("class" => "centrohipico"), "id" => "dch"));
        echo "
                                ";
        // line 40
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "centroHipico", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 41
            echo "                                <span class=\"help-block \">
                                    ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centroHipico"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                                </span>
                                ";
        }
        // line 44
        echo " &nbsp;
                              <div class=\"btn-group\">  
                                <button class=\"btn btn-info\" onclick=\"actualiza('centroh','dch','solicitudes_updatech');\">Actualizar</button>
                                <button class=\"btn btn-danger\" onclick=\"cambiar('centroh',false);\">Cancelar</button>
                              </div>
                         </div>
\t\t</div>
\t\t<h3>2. Seleccionar el Tipo de Autorizaci&oacute;n</h3>
\t\t<div >
                       <div class=\"col-md-4\">Tipo de Autorizaci&oacute;n <span class=\"oblig\">(*)</span></div>
                        <div class=\"col-md-4\" id=\"D_licencia\">
                            <div id=\"V_licencia\">";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "ClasLicencia"), "clasfLicencia"), "html", null, true);
        echo "</div> &nbsp;<button class=\"btn btn-warning\" onclick=\"cambiar('licencia',true);\">Cambiar</button>
                        </div>   
                        <div class=\"col-md-4\" id=\"F_licencia\" style=\"display:none;\">
                           ";
        // line 59
        echo "                           ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ClasLicencia"), 'widget', array("attr" => array("class" => "licencia"), "id" => "alic"));
        echo "
                                ";
        // line 60
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ClasLicencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 61
            echo "                                <span class=\"help-block \">
                                    ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ClasLicencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                                </span>
                                ";
        }
        // line 64
        echo "&nbsp;
                              <div class=\"btn-group\">  
                                <button class=\"btn btn-info\" onclick=\"actualiza('licencia','alic','solicitudes_updatelic');\">Actualizar</button>
                                <button class=\"btn btn-danger\" onclick=\"cambiar('licencia',false);\">Cancelar</button>
                              </div>  
                        </div>
\t\t</div>

\t\t<h3>3. Seleccione los Juegos a Explotar</h3>
\t\t<div>
                        <div class=\"col-md-3\">Juegos a Explotar: </div>
                        <div id=\"D_juegoe\" class=\"text-center col-md-7\" >
                            ";
        // line 76
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getUrl("juegosexplotados_show", array("ids" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))));
        echo "
                        </div>
                        <div id=\"F_juegoe\" class=\"col-md-7 \" style=\"display:none;\" >
                            <p>Para ver Lista: Seleccione Tipo de Autorización y Centro hípico</p>
                        </div>
\t\t</div>
\t\t<h3>4. Adjuntar Recaudos</h3>
                  <div> 
                    <div class=\"text-center col-md-12\" id=\"D_recaudos\" >
                         ";
        // line 85
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getUrl("recaudoscargados_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))));
        echo "     
                         ";
        // line 87
        echo "                    </div>   
                    <div id=\"F_recaudos\" style=\"display:none;\">
                        <p>Para ver: Seleccione Tipo de Autorización y Centro hipico</p>
                    </div>
                    <div id=\"F_recaudos2\" style=\"display:none;\"></div>
                  </div>  
\t\t<h3>5. Pago por Procesamiento</h3>
                  <div>
                        <div id=\"D_pagoP\">
                            ";
        // line 96
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getUrl("recaudospago_show", array("ids" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))));
        echo "
                        </div>
                        <div id=\"F_pagoP\" style=\"display:none;\">
                            <p>Para ver: Seleccione Tipo de Autorización y Centro hipico</p>
                        </div>
                  </div>    
<!--  FIN TABS  -->
        </div>
        <div class=\"block-separator col-md-12\"></div>
        <div class=\"col-md-12 form-group btn-group\">
             <div style=\"float: left\">
                 <a href=\"";
        // line 107
        echo $this->env->getExtension('routing')->getPath("solicitudes_list");
        echo "\" class=\"btn btn-info btn-sm\">Volver a Listado</a>
             </div>
             ";
        // line 112
        echo "        </div>        
         <input type=\"hidden\" name=\"entityid\" id=\"entityid\" value=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
        echo "\"/>
         ";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
       </form>  <!-- End form-->
      </div>                 
        <div class=\"block-separator col-md-12\"></div> 
</div>      
<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12 alert text-left\" id=\"myMessage\">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 138
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 139
        echo "        ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
  <script type=\"text/javascript\">
      var entityid='";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
        echo "';
      var recaudoactual=1;
        \$(document).ready(function() {
            \$( \"#accordion\" ).accordion({
\t\t    heightStyle: \"content\"
\t\t});\t\t
                //\$(\"#gensol\").on(\"click\",function(){
                //    GenerarSolicitud();
                //});
               
                \$(\"#alic\").change(function(){
                    var id=\$(\"#alic option:selected\").val();
                    actualizaRecaudos(id);
                });
            Inicializar();    
       });
       
  function Inicializar()
  {
      \$(\".disabled\").prop('disabled',true);
        //\$(\".readonly\").prop('readonly',true);
        var Options={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            maxDate: '";
        // line 165
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "',
            onSelect: function(dateText, inst) { 
               var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
               //GenerarSolicitud(datets);
               \$(this).val(dateText);                
           } 
        };
        var Options2={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            minDate: '";
        // line 175
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, "now", "+30 day"), "d/m/Y"), "html", null, true);
        echo "',
            onSelect: function(dateText, inst) { 
               var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
               //GenerarSolicitud(datets);
               \$(this).val(dateText);                
           } 
        };
      \$(\".date\").each(function(index,elem){
          \$(this).datepicker(Options2);
          \$(this).attr('style','cursor: pointer !important');
      });
      \$(\".datePago\").each(function(index,elem){
          \$(this).datepicker(Options);
          \$(this).attr('style','cursor: pointer !important');
          //\$(this).prop('readonly',true);
      });   
  }
  

function getRoute(elem,route)
  {    \$(elem).html(\"Actualizando Datos.. Espere...\");
        \$.get(route)
         .success(function(data) {
             \$(elem).html(data);
         }).error(function(data, status, headers, config) {
                if (status === '500') {
                    message = \"No hay conexión con el servidor\";
                    \$(elem).html(message);
                }
           });
  }

    function GenerarSolicitud(){
        if(!ValidaForm()) {
            //Dialog('<div class=\"col-md-12 alert\"> Aun Faltan Campos Por Rellenar en la Solicitud </div>','ERROR');
            return false;
        }
        //Dialog('<div class=\"col-md-12 alert\"> Espere un momento mientras se envian los Datos </div>','Info');
        \$(\"#myMessage\").html(\"Espere un momento mientras se envian los Datos\");
        \$(\"#myModal\").modal(\"show\");
        //\$('form').submit();
        return true;
    }

function ValidaForm(){
    var resp=1,msg=new Array();
    \$(\"input:file\").each(function(index,elem){
       if (\$(this).val()===''){resp=0;msg.push(\"- Faltan Documentos de Recaudos<br/>\");return false;}
    });
    \$(\".date\").each(function(index,elem){
       if (!\$(this).hasClass('disabled') && \$(this).val()==='') 
       {resp=0;msg.push(\"- Falta Asignar Fechas de Recaudos<br/>\"); return false;}    
    });
    if(\$('#juegoe').length){
        if(\$(\"#juegoe option:selected\").val()==='') {resp=0;msg.push(\"- Debe seleccionar los Juegos<br/>\");}
    }
    if(\$(\"#dch option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione el Centro Hípico<br/>\");}
    if(\$(\"#alic option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione una Licencia<br/>\");}
    if(\$(\"#banco option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione un Banco<br/>\");}
    if(\$(\"#reciboNP\").val()==='' || parseInt(\$(\"#reciboNP\").val())===0) {resp=0;msg.push(\"- Falta el Número de Recibo de Pago<br/>\");}
    
    //Dialog('<div class=\"col-md-12 alert text-left\"> Aun Faltan Campos Por Rellenar en la Solicitud:<br/>'+msg.toString().replace(\",\", \" \")+' </div>','ERROR');
    \$(\"#myMessage\").html(\"Aun Faltan Campos Por Rellenar en la Solicitud:<br/>\"+msg.toString().replace(',', ' ').replace(',', ' '));
    \$(\"#myModal\").modal(\"show\");
    return resp===1?true:false;  
}
          
  function cambiar(elem,action){
    if(action){  
      \$(\"#D_\"+elem).hide();
      \$(\"#F_\"+elem).removeClass('hide').show();
    }else{
       \$(\"#F_\"+elem).hide();
       \$(\"#D_\"+elem).removeClass('hide').show();
    } 
  }
  function actualiza(display,getid,ruta){
      cambiar(display,false);
      id=\$(\"#\"+getid+\" option:selected\").val();
       actualizaRecaudos(id);
      //ruta actualiza centrohipico de solicitud 
      var route=Routing.generate(ruta,{id:id,idds:entityid});
      getRoute(\"#D_\"+display,route);
  }
  
  function actualizaRecaudos(id)
  {
        cambiar('juegoe',true);    
        cambiar('recaudos',false);    
        //cambiar('pagoP',true); 
        //eliminarRecaudos();
        getRoute(\"#F_juegoe\",Routing.generate('datasolicitudes_juegoslist',{id:id,tipo:'edit'}));
           //Recaudos carga pago
        getRoute(\"#D_recaudos\",Routing.generate('datasolicitudes_recaudoslist',{id:id,tipo:'edit'}));
        //cargar Pago
        //getRoute(\"#F_pagoP\",Routing.generate('recaudospago_new',{idcl:id}));
  }
     
    function postRoute(elem,url,Data)
    {  
        \$.ajax({
                type: \"POST\", 
                contentType: false ,
                processData: false,
                url: url,  
                data: Data,
                success: function( data )  
                {  if(typeof(data) ==='object'){
                        cambiar(elem,false);
                        //Cargar datos
                        \$(\"#D_\"+elem).html(data);
                    }else { \$(\"#F_\"+elem).html(data); }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {    message = \"Ha ocurrido un error:\";
                      boton='<button class=\"btn btn-sm btn-warning\" onclick=\"cambiar(\\''+elem+'\\',false);\">Cancelar</button>';
                    \$(\"#F_\"+elem).html(message+\"<br/> \"+textStatus+\"<br/>\"+errorThrown+\"<br/>\"+boton);
                }
           });
    }
// Recaudos
function CargaRecaudo(id,url,elem){
      cambiar(elem,true);
      url=Routing.generate(url,{id:id});
      getRoute(\"#F_\"+elem,url);
  }
  
    function cRecaudo(indx){
         recaudoactual=indx;
         idl=\$(\"#recaudoLicencia_\"+indx).val(); // Id Licencia
         route=Routing.generate('recaudoscargados_new',{id:idl});
         getRoute(\"#F_recaudos\",route); 
         cambiar(\"recaudos\",true);
    }
    
   // Cargar nuevos Recaudos
   function newRecaudo(RL)
    {  dataF=\$(\"#form_drecaudos\").serializeArray();
        route=Routing.generate('recaudoscargados_add',{rlid:RL,ids:entityid});
        \$(\"#form_drecaudos\").submit(function( event ) {
            event.preventDefault();
            dataF=new FormData(this);
            createRecaudo(\"#F_recaudos\",route,dataF);
          });
    }
    
  function createRecaudo(elem,url,Data)
    {  \$(elem).hide();
       \$(\"#F_recaudos2\").html('Cargando Datos, Espere....').show();
        \$.ajax({
                type: \"POST\", 
                contentType: false ,
                processData: false,
                url: url,  
                data: Data,
                success: function( data )  
                {  //cargar la data con la nueva fila de datos
                    if(data.status==='OK')
                    { \$(\"#F_recaudos2\").hide(); //Oculta mensaje
                       cambiar(\"recaudos\",false);  // Cambia las Vistas
                       MostrarDataCell(data.datos); // Muestra los Datos Cargados
                    }
                    else{\$(\"#F_recaudos2\").hide();
                        cambiar(\"recaudos\",true); 
                        alert(\"Ha ocurrido un Error en la carga de Datos \"+data.datos.message);
                     }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {    message = \"Ha ocurrido un error:\";
                      boton='<button class=\"btn btn-sm btn-warning\" onclick=\"cambiar(\\'recaudos\\',false);\">Cancelar</button>';
                    \$(elem).html('<td colspan=\"5\">'+message+\"<br/> \"+textStatus+\"<br/>\"+errorThrown+\"<br/>\"+boton+\"</td>\");
                }
           });
    }   
    
    function MostrarDataCell(data)
    {
        var \$tr=\$(\"#tr_recaudos\"+recaudoactual);
        \$tr.html('');
        \$tr.append(\$('<td>').attr('class','text-left').text(recaudoactual));
        \$.each(data,function(i,dato){
            \$tr.append(\$('<td>').attr('class','text-center').text(dato));
        });
        \$tr.append(\$('<td>').attr('class','text-center').text('Cargado'));
    }
    // Pago 
    function cargaDatosPago(route,elem,idform)
    {
        dataF=\$(\"#\"+idform).serializeArray();
        \$( \"#\"+idform).submit(function( event ) {
            event.preventDefault();
            dataF=new FormData(this);
            postRoute(elem,route,dataF);
          });
    }
    
    function cargaJuegos(elem)
    {   console.log(\"Cargar juegos\");
        route=Routing.generate('juegosexplotados_create',{ids:entityid});
         Data='juegose='+\$('#Sjuegose').val();
         \$.ajax({
                type: \"POST\", 
                url: route,  
                data: Data,
                success: function( data )  
                {  if(typeof(data) ==='object'){
                        cambiar(elem,false);
                        //Cargar datos
                        \$(\"#D_\"+elem).html(data);
                    }else { \$(\"#F_\"+elem).html(data); }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {    message = \"Ha ocurrido un error:\";
                      boton='<button class=\"btn btn-sm btn-warning\" onclick=\"cambiar(\\''+elem+'\\',false);\">Cancelar</button>';
                    \$(\"#F_\"+elem).html(message+\"<br/> \"+textStatus+\"<br/>\"+errorThrown+\"<br/>\"+boton);
                }
           });
        //postRoute(elem,route,data);
    }
    function updatePago(id)
    {   route=Routing.generate('recaudospago_update',{id:id});
        cargaDatosPago(route,'pagoP','form_pagoP');
    }

   function nuevoPago()
   {
       route=Routing.generate('recaudospago_create',{ids:entityid});
       cargaDatosPago(route,'pagoP','form_pagoP'); 
   }
    
   </script>          
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:editCH2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 175,  287 => 165,  260 => 141,  254 => 139,  251 => 138,  223 => 114,  219 => 113,  216 => 112,  211 => 107,  197 => 96,  186 => 87,  182 => 85,  170 => 76,  156 => 64,  150 => 62,  147 => 61,  145 => 60,  140 => 59,  134 => 55,  121 => 44,  115 => 42,  112 => 41,  110 => 40,  106 => 39,  100 => 36,  97 => 35,  89 => 29,  86 => 28,  79 => 26,  73 => 24,  68 => 22,  63 => 21,  61 => 20,  55 => 18,  51 => 17,  46 => 15,  32 => 3,  29 => 2,);
    }
}
