<?php

/* NewTemplateBundle:Menu:mnu_gerente.html.twig */
class __TwigTemplate_5b54ca450b8bb5946becbee9a19b290d071be5eba3a5254c64d49138680b3075 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3 >Solicitudes Licencias</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("gerente_consulta_licencia");
        echo "\" title=\"Consulta las licencias\">Consultar</a></li>
    <li><a href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("fiscal_citas_listado", array("tipo" => "Revisada"));
        echo "\" title=\"Por Aprobar\">Por Aprobar</a></li>
    <li><a href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("solicitudes_aprobadas_listado");
        echo "\" title=\"Aprobadas\">Aprobadas</a></li>
     <li><a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("fiscal_citas_listado", array("tipo" => "Rechazada"));
        echo "\" title=\"Licencias rechazadas\">Rechazadas</a></li>
</ul>

<h3>Gestión Operadoras</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("operadora");
        echo "\" title=\"Operadoras\">Operadoras</a></li>
    <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("Centrohipico_solicitud_afiliacion_listado");
        echo "\" title=\"Aprobar Incorporación de Afiliados a Operadoras\">Aprobar Incorporación de Afiliados a Operadoras</a></li>
</ul>

<h3>Control de Citas</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("citas");
        echo "\" title=\"Citas\">Citas</a></li>
    <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("nocitafecha");
        echo "\" title=\"\">Inhabilitar Fechas</a></li>
</ul>

<h3>Consulta de Pagos</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "PROCESAMIENTO"));
        echo "\" title=\"Pago de Procesamiento\">Procesamiento</a></li>
    <li><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "OTORGAMIENTO"));
        echo "\" title=\"Pago de Otorgamiento\">Otorgamiento</a></li>
    <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "APORTE_MENSUAL"));
        echo "\" title=\"Pago de Aporte Mensual\">Aporte Mensual</a></li>
    <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "MULTA"));
        echo "\" title=\"Pago de Multas\">Multa</a></li>
    <li><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("pagos_validar");
        echo "\" title=\"Validar Pagos\">Validar Pagos</a></li>
</ul>

<h3>Fiscalizaciones</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("providencia");
        echo "\" title=\"Generar Providencias\">Gestionar Providencias</a></li>
    <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("fiscalizacion_fiscalizar");
        echo "\" title=\"Fiscalización\">Generar Fiscalización</a></li>
    <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("fiscalizacion");
        echo "\" title=\"Consulta de Fiscalizaciones\">Consultar</a></li>
</ul>

<h3>Fiscales</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("fiscal_list");
        echo "\">Ficha de fiscales</a></li>
    ";
        // line 41
        echo "</ul>

<!--BLOQUE DE MENU LATERAL INFORMES-->
<h3>Informes</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("informe_xmes", array("reporte" => "licencias"));
        echo "\" title=\"Licencias\">Licencias</a></li>
    <li><a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("informe_xmes", array("reporte" => "fiscalizaciones"));
        echo "\" title=\"Fiscalización\">Fiscalización</a></li>
    <li><a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("informe_xmes", array("reporte" => "ingresos"));
        echo "\" title=\"Ingresos\">Ingresos</a></li>
    <li><a href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("informe_xmes", array("reporte" => "usuarios"));
        echo "\" title=\"Usuarios\">Usuarios</a></li>
    <li><a href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("informe_ejecutivo");
        echo "\" title=\"Informe Ejecutivo\">Informe Ejecutivo</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_gerente.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 50,  130 => 49,  126 => 48,  122 => 47,  118 => 46,  111 => 41,  107 => 39,  99 => 34,  95 => 33,  91 => 32,  83 => 27,  79 => 26,  75 => 25,  71 => 24,  67 => 23,  59 => 18,  55 => 17,  47 => 12,  43 => 11,  35 => 6,  31 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }
}
