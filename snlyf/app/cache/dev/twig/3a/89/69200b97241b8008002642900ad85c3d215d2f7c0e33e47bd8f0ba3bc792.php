<?php

/* FOSUserBundle:DatosBasicos:datosBasicos.html.twig */
class __TwigTemplate_3a8969200b97241b8008002642900ad85c3d215d2f7c0e33e47bd8f0ba3bc792 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "   <div class=\"block-separator col-sm-12\"></div>
        <div class=\"col-md-12\">
            <h1 class=\"tit_principal\">Datos Básicos </h1>
        </div>
    <div class=\"block-separator col-sm-12\"></div>

    <div class=\"col-md-12\">
        <table id=\"tabla_reporte2\" class=\"bg-white text-left table\">\t\t\t\t
            <tbody>
            <tr id=\"table_header2\">
              <td colspan=\"4\">Detalle Datos Básicos </td>
            </tr>
            <tr>
                <td><label for=\"ci_t:\">C.I</label></td>
                <td class=\"text-left\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "ci"), "html", null, true);
        echo "</td>
                <td><label for=\"rif\">RIF</label></td>
                <td>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "persJuridica"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "rif"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td><label for=\"\">Nombre</label></td>
                <td>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "nombre"), "html", null, true);
        echo "</td>
                <td><label for=\"\">Apellido</label></td>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "apellido"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td><label for=\"\">No. Teléfono Fijo</label></td>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "CodTelefonoLocal"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "TelefonoLocal", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "TelefonoLocal"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
                <td><label for=\"\">No. Fax</label></td>
                <td>";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "CodFax"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "fax", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "fax"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td><label for=\"\">No. Teléfono Celular</label></td>
                <td>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "CodTelefonoMovil"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "TelefonoMovil", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "TelefonoMovil"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
                <td><label for=\"\">Correo Electrónico</label> </td>
                <td>";
        // line 38
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "CorreoAlternativo", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "CorreoAlternativo"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td> 
            </tr>
            <tr>
                <td ><label for=\"\">Página Web</label></td>
                <td colspan=\"3\">";
        // line 42
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "SitioWeb", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "SitioWeb"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
            </tr>
   </tbody></table>
      <table id=\"tabla_reporte2\" class=\"bg-white table\">\t\t\t\t
            <tbody>
            <tr id=\"table_header2\">
              <td colspan=\"4\">Detalle Dirección </td>
            </tr>
               <tr class=\"bg-white\">
                <td><label for=\"\">Estado</label></td>
                <td>";
        // line 52
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "estado")) > 0)) {
            // line 53
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "estado"), "nombre"), "html", null, true);
            echo "
                     ";
        } else {
            // line 54
            echo " Sin Dato   
                    ";
        }
        // line 56
        echo "                </td>
                <td><label for=\"\">Municipio</label></td>
                     <td>";
        // line 58
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "municipio")) > 0)) {
            // line 59
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "municipio"), "nombre"), "html", null, true);
            echo "
                        ";
        } else {
            // line 60
            echo "Sin dato
                        ";
        }
        // line 62
        echo "                    </td>
            </tr>
            <tr class=\"bg-white\">
                <td><label for=\"\">Ciudad</label></td>
                <td>";
        // line 66
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "ciudad")) > 0)) {
            // line 67
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "ciudad"), "html", null, true);
            echo "
                     ";
        } else {
            // line 68
            echo "Sin Dato   
                    ";
        }
        // line 70
        echo "                </td>
                <td><label for=\"\">Parroquia</label></td>
                <td>";
        // line 72
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "parroquia")) > 0)) {
            // line 73
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "perfil"), 0, array(), "array"), "parroquia"), "nombre"), "html", null, true);
            echo "
                     ";
        } else {
            // line 74
            echo "Sin Dato   
                    ";
        }
        // line 76
        echo "                </td>
            </tr>
            <tr class=\"bg-white\">
                <td><label for=\"\">Urbanización/Sector</label></td>
                <td>";
        // line 80
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "urbanizacion", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "urbanizacion"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
                <td><label for=\"\">Avenida/Calle/Carrera</label></td>
                <td>";
        // line 82
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "calle", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "calle"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
            </tr>
            <tr class=\"bg-white\">
                <td><label for=\"\">Edificio/Casa</label></td>
                <td>";
        // line 86
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "apartamento", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "apartamento"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
                <td><label for=\"\">Oficina/Apto/No.</label></td>
                <td>";
        // line 88
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "apartamentoNO", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "apartamentoNO"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
            </tr>
            <tr class=\"bg-white\"> 
                <td><label for=\"\">Punto de Referencia</label></td>
                <td>";
        // line 92
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "referencia", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "referencia"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
                <td><label for=\"\">Código Postal</label></td>
                <td>";
        // line 94
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "codigoPostal", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "codigoPostal"), "Sin dato")) : ("Sin dato")), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>        

</div>
<div class=\"col-md-4 col-md-offset-8 text-right right\">
     ";
        // line 104
        echo "     <div style=\"float: right\">
         <a href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("editar-datos-basicos");
        echo "\" class=\"btn btn-primary btn-sm\">Modificar</a>
     </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:DatosBasicos:datosBasicos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 105,  223 => 104,  213 => 94,  208 => 92,  201 => 88,  196 => 86,  189 => 82,  184 => 80,  178 => 76,  174 => 74,  168 => 73,  166 => 72,  162 => 70,  158 => 68,  152 => 67,  150 => 66,  144 => 62,  140 => 60,  134 => 59,  132 => 58,  128 => 56,  124 => 54,  118 => 53,  116 => 52,  103 => 42,  96 => 38,  89 => 36,  80 => 32,  73 => 30,  66 => 26,  61 => 24,  52 => 20,  47 => 18,  31 => 4,  28 => 3,);
    }
}
