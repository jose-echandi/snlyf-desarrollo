<?php

/* SolicitudesCitasBundle::solicitud_base.html.twig */
class __TwigTemplate_46c362eb72079990d294e23d5ecd0bb7e04ff15f22757f6810991443d12e6596 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 6
    public function block_content_content($context, array $blocks = array())
    {
        echo "    
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  32 => 3,  29 => 2,  169 => 52,  165 => 50,  163 => 49,  160 => 48,  143 => 45,  135 => 42,  131 => 41,  122 => 37,  118 => 36,  110 => 33,  105 => 32,  100 => 29,  96 => 28,  90 => 27,  84 => 26,  80 => 25,  76 => 24,  72 => 23,  52 => 21,  35 => 6,  31 => 3,  28 => 2,);
    }
}
