<?php

/* SolicitudesCitasBundle:DataSolicitudes:indexOperadora.html.twig */
class __TwigTemplate_4691aa845735bd210346db13ba5956e5853decacdf817ffeeff2f55ffcf265ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"block-separator col-md-12\"></div>
    <h1 class=\"tit_principal\">Mis solicitudes de Licencia</h1>
    ";
        // line 6
        echo "         <br /><br /><br />
                <table class=\"table table-condensed table-striped\">
                <thead>
                <tr>
                            <th>Nº</th>
                            <th>Nº Localizador</th>
                            <th>Operadora </th>
                            <th>F. Solicitud </th>
                            <th>F. de Cita  &nbsp;</th>
                            <th>Lic. Tipo</th>
                            <th>Status</th>
                            <th>Acciones</th>
\t               </tr>
                </thead>
                <tbody>
              ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            echo "          
                        <tr>
                            <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "</td>
                            <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "codsolicitud"), "html", null, true);
            echo "</td>
                            <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "operadora"), "denominacionComercial"), "html", null, true);
            echo "</td>
                            <td>";
            // line 26
            if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fechaSolicitud")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fechaSolicitud"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                            <td>";
            // line 27
            if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cita"), "fechaSolicitud")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                            <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "ClasLicencia"), "clasfLicencia"), "html", null, true);
            echo "</td>
                            <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status"), "html", null, true);
            echo " </td>
                            <td>
                              ";
            // line 32
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudoperadora_printGen", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tipo" => "2")), "html", null, true);
            echo "\" target=\"_blank\" 
                                   class=\"btn btn-info\" title=\"Ver e Imprimir\" ";
            // line 33
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") != "Solicitada")) {
                echo " disabled ";
            }
            echo ">
                                    <i class=\"fa fa-print\"></i>
                                </a><!--Ver-->
                                <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudes_cambiarcita", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" 
                                   class=\"btn btn-success\" title=\"Reprogramar Cita\" ";
            // line 37
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") != "Solicitada")) {
                echo " disabled ";
            }
            echo ">
                                    <i class=\"fa fa-clock-o\"></i>
                                </a><!--Cambiar Cita-->
                               <!-- <a href=\"#\" class=\"btn btn-primary\" title=\"Cambiar Documentos\"><i class=\"fa fa-files-o\"></i></a><!--Cambiar Documentos-->
                                <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudoperadora_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" 
                                   class=\"btn btn-warning\" title=\"Cambiar Datos\" ";
            // line 42
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") != "Solicitada")) {
                echo " disabled ";
            }
            echo ">
                                    <i class=\"fa fa-edit\"></i>
                                </a><!--Cambiar Datos-->
                             ";
            // line 45
            echo " 
                            </td>
                         </tr>
                 ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo " 
                 ";
        // line 49
        if ((!(isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")))) {
            // line 50
            echo "                     <tr><td class=\"text-center\" colspan=\"8\">No Existen Registros de Solicitudes</td></tr>
               ";
        }
        // line 52
        echo "\t\t</tbody>
             </table> 
    ";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:indexOperadora.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 52,  165 => 50,  163 => 49,  160 => 48,  143 => 45,  135 => 42,  131 => 41,  122 => 37,  118 => 36,  110 => 33,  105 => 32,  100 => 29,  96 => 28,  90 => 27,  84 => 26,  80 => 25,  76 => 24,  72 => 23,  52 => 21,  35 => 6,  31 => 3,  28 => 2,);
    }
}
