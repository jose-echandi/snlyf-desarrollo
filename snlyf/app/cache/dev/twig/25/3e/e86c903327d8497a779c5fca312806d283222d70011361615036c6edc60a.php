<?php

/* CentrohipicoBundle:SolicitudAfiliacion:paso1.html.twig */
class __TwigTemplate_253ee86c903327d8497a779c5fca312806d283222d70011361615036c6edc60a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"page-header\">
        <h1>Solicitar Incorporaci&oacute;n Afiliado</h1>
    </div>

    ";
        // line 8
        if (((!array_key_exists("message", $context)) || (null === (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))))) {
            // line 9
            echo "
    <div class=\"padding-15\">
        <div id=\"texto\">Estimada Operadora, <br>Al agregar la incorporación de un nuevo afiliado, deberá adjuntar el
            contrato de Afiliado-Operadora, y esperar un lapso no mayor de 48 horas para la aprobación de la SUNAHIP.<br><br>
        </div>
        <div id=\"texto\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios</div>

        <form id =\"valida_paso1\">
            <table id=\"tabla_reporte2\">
                <tbody>
                <tr id=\"table_header2\">
                    <td colspan=\"4\">Nuevo Afiliado</td>
                </tr>
                <tr>
                    <td>Fecha de Solicitud:</td>
                    <td colspan=\"3\"><input type=\"text\" value=\"";
            // line 24
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
            echo "\" disabled=\"disabled\"></td>
                </tr>
                <tr>
                    <td>RIF Afiliado:</td>
                    <td colspan=\"3\">
                        <select name=\"rif_type\" id=\"rifType\" required=\"true\">
                            <option value=\"\"></option>
                            <option value=\"V\">V</option>
                            <option value=\"J\">J</option>
                            <option value=\"E\">E</option>
                            <option value=\"G\">G</option>
                        </select>
                        <span>-</span>
                        <input name=\"rif\" type=\"text\" id=\"rif\" required=\"true\" class=\"numeric\"
                               maxlength=\"9\" class=\"corto\"
                               title=\"No colocar ni puntos, ni guiones\" size=\"10\">

                        <span class=\"oblig\">
                            (*)
                        </span>
                        <div style=\"color: #E70202;display: none\" id=\"rifNotFound\"></div>
                    </td>
                </tr>
                <tr>
                    <td>Denominaci&oacute;n comercial</td>
                    <td>
                        <input name=\"nombre\" type=\"text\" id=\"nombreC\" required=\"true\" maxlength=\"50\" class=\"corto\" size=\"20\">
                        <span class=\"oblig\">
                            (*)
                        </span>
                        <div style=\"color: #E70202;display: none\" id=\"establecimientoHtml\"></div>
                    </td>
                </tr>
                </tr>
                </tbody>
            </table>
            <div class=\"block-separator col-md-12\"></div>
            <div class=\"col-md-offset-8 col-md-4 form-group text-right\">
                <a href=\"";
            // line 62
            echo $this->env->getExtension('routing')->getPath("notification");
            echo "\" class=\"btn btn-warning btn-sm\">Cancelar</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href=\"#\" onclick=\"find()\" class=\"btn btn-primary btn-sm nuevaafiliacion\" id=\"form_btttn\"/>Siguiente </a>
            </div>
        </form>

        ";
        } else {
            // line 69
            echo "            <div class=\"col-md-12\">
                <div id=\"notificaciones\">
                    <ul>
                        <li class=\"n1\">";
            // line 72
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "</h5></li>
                    </ul>
                </div>
                <div class=\"col-md-offset-8 col-md-4 form-group text-right\">
                    <a href=\"";
            // line 76
            echo $this->env->getExtension('routing')->getPath("notification");
            echo "\" class=\"btn btn-warning btn-sm\">Aceptar</a>
                </div>
            </div>
    ";
        }
        // line 79
        echo "        
    </div>
";
    }

    // line 83
    public function block_script_base($context, array $blocks = array())
    {
        // line 84
        echo "    <script type=\"text/javascript\">

        function find() {            
            var rifNumber = \$(\"#rif\").val();
            var ritType = \$('#rifType option:selected').val();
            if(ritType == \"V\" || ritType == \"E\"){
                var rango = 7
            }else{
                var rango = 8
            }
            if(!\$(\"#rifType\").val()) {
                \$(\"#rifNotFound\").show();
                \$(\"#rifNotFound\").html(\"Seleccione Tipo Rif\");
            } else
            if (rifNumber.length >= rango) {
                \$(\"#rifNotFound\").hide();
                if(!\$(\"#nombreC\").val()) {
                    \$(\"#establecimientoHtml\").show();
                    \$(\"#establecimientoHtml\").html(\"Ingrese nombre Centro Hípico\");
                } else {
                    \$(\"#establecimientoHtml\").hide();
                    var nombreC = \$(\"#nombreC\").val();
                    var rifType = \$(\"#rifType\").val();
                    var rif = rifType+rifNumber;
                    var url = Routing.generate('Centrohipico_solicitud_afiliacion_buscar_rif');
                    \$.post(url, { rif_type: rifType, rif_number: rifNumber, nombre: nombreC }, function (response) {
                        if(response.status == false)
                        {                            
                            \$(\"#rifNotFound\").show();
                            \$(\"#rifNotFound\").html(response.message);
                        }else if(response.status == true)
                        {
                            var url = Routing.generate('Centrohipico_solicitud_afiliacion_2', { rif_type: rifType, rif_number: rifNumber, id: response.id });
                            window.location = url;
                        } else {
                            \$(\"#rifNotFound\").show();
                            \$(\"#rifNotFound\").html(\"Error al procesar los datos.\");
                        }
                    });
                }
            }else{
                var html = \"El Rif debe contener al menos \"+rango+\" dígitos.\";
                \$(\"#rifNotFound\").show();
                \$(\"#rifNotFound\").html(html);
            }
        }

        \$(document).ready(function () {

            \$(\".numeric\").keydown(function (e) {
                if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:SolicitudAfiliacion:paso1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 84,  133 => 83,  127 => 79,  120 => 76,  113 => 72,  108 => 69,  98 => 62,  57 => 24,  40 => 9,  38 => 8,  32 => 4,  29 => 3,);
    }
}
