<?php

/* CentrohipicoBundle:DataCentrohipico:index.html.twig */
class __TwigTemplate_67e9b1ac6232c58d59d036ea6307a1e641bd776b7137297daea53299c1b0589f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    \t <div class=\"block-separator col-sm-12\"></div> 
            <div class=\"col-md-12\">
\t       <h1 class=\"tit_principal\">Mis Centros Hipicos</h1>
            </div>  
 <div class=\"col-md-12\">
     <table class=\"table table-condensed table-striped\">
         <thead>
         <tr>
                <th>Nº</th>
                <th>Rif</th>
                <th>Denominación Comercial</th> 
                <th>Clasificación de Licencia</th>                                 
            </tr>
        </thead>
        <tbody>  
        ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 19
            echo "            <tr>
                <td align=\"center\">";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rif"), "html", null, true);
            echo "</td>
                <td align=\"center\">
                    <a href=\"#\" id=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\"  data-toggle=\"modal\" data-target=\"#myModal\" class=\"show_detail\">
                        ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "denominacionComercial"), "html", null, true);
            echo "
                    </a>
                </td>
                <td>&nbsp;- &nbsp;</td>
            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "         ";
        if (twig_test_empty((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")))) {
            echo " 
                <tr>
                  <td colspan=\"4\" class=\"text-center text-uppercase text-info\">No hay Resultados</td>
                </tr>       
         ";
        }
        // line 34
        echo "      
        </tbody>
    </table>
 </div>      
    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">
                        <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>
                    </button>
                </div>
                 <div class=\"modal-body\" id=\"datos_hipicos\">Cargando....</div>
                 <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>
 ";
    }

    // line 54
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 55
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataCentroHipico/list.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataCentrohipico:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 55,  135 => 54,  113 => 34,  104 => 30,  84 => 24,  80 => 23,  73 => 21,  69 => 20,  66 => 19,  49 => 18,  32 => 3,  29 => 2,);
    }
}
