<?php

/* FOSUserBundle:Notification:index.html.twig */
class __TwigTemplate_3f9976795adad140440ec4a0e07654d81072b313ed5aeaa85bb048a9c43099ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div id=\"contenido\">
    <div class=\"tit_principal\">Notificaciones</div>

    <div id=\"notificaciones\">
        <ul>
            ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["notification"]) ? $context["notification"] : $this->getContext($context, "notification")));
        foreach ($context['_seq'] as $context["_key"] => $context["noti"]) {
            // line 11
            echo "                <li class=\"n1\">";
            echo twig_escape_filter($this->env, (isset($context["noti"]) ? $context["noti"] : $this->getContext($context, "noti")), "html", null, true);
            echo "</li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['noti'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "        </ul>
    </div>

</div>

";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Notification:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 13,  43 => 11,  39 => 10,  31 => 4,  28 => 3,);
    }
}
