<?php

/* UserBundle::base.html.twig */
class __TwigTemplate_cb229a631c39c544d66c8306ac875cf19fff929199ed3bdc1a1b981a9422cd85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content_content', $context, $blocks);
        // line 4
        echo "    
<br><br>
Importante: Recuerde también mantener actualizada la dirección de correo electrónico de su usuario con el que se registró en el sistema, ya que es la única vía para mantenerlo informado.<br><br>
Atentamente,<br>
<b>Superintendencia Nacional de Actividades Hípicas, SUNAHIP.</b><br><br>
Para mayor información consultar:  <a href=\"http://www.sunahip.com.ve\">www.sunahip.com.ve</a><br>
Contáctenos: Correo electrónico: <a href=\"mailto:info@sunahip.gob.ve\">info@sunahip.gob.ve</a><br>
Twitter: <a href=\"https://twitter.com/OficialINH\">@OficialINH</a><br>
Teléfonos: 58 (212)681 33 31<br><br>
<span style=\"font-style:italic;\">Este correo ha sido enviado automáticamente por el Sistema Nacional de Licencias y Fiscalización de la SUNAHIP. Por favor no responder éste correo.</span>";
    }

    // line 1
    public function block_content_content($context, array $blocks = array())
    {
        // line 2
        echo "    Estimado(a) ";
        echo twig_escape_filter($this->env, (isset($context["nombre"]) ? $context["nombre"] : $this->getContext($context, "nombre")), "html", null, true);
        echo ",<br><br>
    ";
        // line 3
        echo (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"));
        echo "
";
    }

    public function getTemplateName()
    {
        return "UserBundle::base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 3,  38 => 2,  35 => 1,  20 => 1,  37 => 5,  31 => 3,  28 => 2,  53 => 12,  50 => 11,  45 => 8,  39 => 4,  36 => 2,  32 => 11,  29 => 10,  27 => 8,  24 => 7,  22 => 4,);
    }
}
