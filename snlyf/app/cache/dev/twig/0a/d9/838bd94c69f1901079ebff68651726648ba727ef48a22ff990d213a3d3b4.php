<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_pagosP.html.twig */
class __TwigTemplate_0ad9838bd94c69f1901079ebff68651726648ba727ef48a22ff990d213a3d3b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>Tipo de Documento</td>
                 <td>Buscar Archivo<span class=\"oblig\">(*)</span></td>
                 <td>Fecha de Pago<span class=\"oblig\">(*)</span></td>
                 <td>N de Recibo<span class=\"oblig\">(*)</span></td>
                 <td>Banco<span class=\"oblig\">(*)</span></td>
                 <td>Monto a Pagar<span class=\"oblig\">(*)</span></td>
        </tr>
         <tr>
            <td class=\"text-left\">Pago por Procesamiento</td>
            <td class=\"text-center\">";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "archivoAdjunto"), 'widget', array("attr" => array("class" => "mediarecaudo")));
        echo "
                           ";
        // line 13
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : null), "pago", array(), "any", false, true), "archivoAdjunto", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 14
            echo "                           <span class=\"help-block \">
                               ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "archivoAdjunto"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 18
        echo "             </td>
            <td class=\"text-center\">";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "fechaDeposito"), 'widget', array("attr" => array("class" => "datePago fechapago")));
        echo "
                           ";
        // line 20
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : null), "pago", array(), "any", false, true), "fechaDeposito", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 21
            echo "                           <span class=\"help-block \">
                               ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "fechaDeposito"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 25
        echo "             </td>
            <td class=\"text-center\">";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "numReferencia"), 'widget', array("attr" => array("class" => "reciboN"), "id" => "reciboNP", "maxlength" => "15"));
        echo "
                           ";
        // line 27
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : null), "pago", array(), "any", false, true), "numReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 28
            echo "                           <span class=\"help-block \">
                               ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "numReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 32
        echo "             </td>
            <td class=\"text-center\">";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "banco"), 'widget', array("attr" => array("class" => "banco"), "id" => "banco"));
        echo "
                           ";
        // line 34
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : null), "pago", array(), "any", false, true), "banco", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 35
            echo "                           <span class=\"help-block \">
                               ";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "pago"), "banco"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 39
        echo "             </td>
            <td class=\"text-center\">
                ";
        // line 41
        echo " 
                 ";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["PP"]) ? $context["PP"] : $this->getContext($context, "PP")), "html", null, true);
        echo " Bs";
        // line 43
        echo "            </td>
        </tr>
</table>

<script type=\"text/javascript\">

    \$(\".mediarecaudo\").change(function(){
        console.info('valor', \$(this).val());
        comprueba_extension(\$(this).val(),\$(this));
    })

    function comprueba_extension(archivo, obj) {
        extensiones_permitidas = new Array(\".pdf\");
        mierror = \"\";
        if (!archivo) {
            //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
            mierror = \"No has seleccionado ningún archivo\";
        }else{
            //recupero la extensión de este nombre de archivo
            extension = (archivo.substring(archivo.lastIndexOf(\".\"))).toLowerCase();
            //compruebo si la extensión está entre las permitidas
            permitida = false;
            for (var i = 0; i < extensiones_permitidas.length; i++) {
                if (extensiones_permitidas[i] == extension) {
                    permitida = true;
                    break;
                }
            }
            if (!permitida) {
                mierror = \"Comprueba la extensión de los archivos a subir. \\nSolo se pueden subir archivos con extensiones: \" + extensiones_permitidas.join();
                obj.val('');
                alert (mierror);
            }else{
                return 1;
            }
        }

        return 0;
    }
</script>
            
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_pagosP.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 43,  108 => 42,  105 => 41,  101 => 39,  95 => 36,  92 => 35,  86 => 33,  83 => 32,  77 => 29,  74 => 28,  72 => 27,  68 => 26,  65 => 25,  59 => 22,  54 => 20,  50 => 19,  47 => 18,  41 => 15,  38 => 14,  36 => 13,  32 => 12,  217 => 107,  204 => 97,  151 => 46,  149 => 45,  145 => 43,  131 => 42,  129 => 41,  118 => 38,  114 => 36,  110 => 35,  106 => 34,  102 => 32,  100 => 31,  96 => 28,  90 => 34,  87 => 24,  85 => 23,  80 => 22,  78 => 21,  75 => 20,  69 => 17,  66 => 16,  64 => 15,  60 => 14,  56 => 21,  52 => 12,  31 => 10,  29 => 9,  19 => 1,);
    }
}
