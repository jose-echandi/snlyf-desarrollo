<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_juegos_ed.html.twig */
class __TwigTemplate_fa517d49de1635ba040fce64dffaa4fb278bf5614032320a0aaa9abd37802cf9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["juegosE"]) ? $context["juegosE"] : $this->getContext($context, "juegosE")));
        foreach ($context['_seq'] as $context["_key"] => $context["juegoe"]) {
            echo " 
    ";
            // line 2
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["juegoe"]) ? $context["juegoe"] : $this->getContext($context, "juegoe")), "juego"), "html", null, true);
            echo ", &nbsp; 
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['juegoe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3
        echo " &nbsp; 
";
        // line 4
        if ((!twig_test_empty((isset($context["juegosE"]) ? $context["juegosE"] : $this->getContext($context, "juegosE"))))) {
            echo " 
    <button class=\"btn btn-warning\" onclick=\"cambiar('juegoe',true);\">Cambiar</button> 
  ";
        } else {
            // line 7
            echo "      No Existen Juegos para Esta Licencia.
";
        }
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_juegos_ed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 7,  36 => 4,  33 => 3,  25 => 2,  19 => 1,  300 => 175,  287 => 165,  260 => 141,  254 => 139,  251 => 138,  223 => 114,  219 => 113,  216 => 112,  211 => 107,  197 => 96,  186 => 87,  182 => 85,  170 => 76,  156 => 64,  150 => 62,  147 => 61,  145 => 60,  140 => 59,  134 => 55,  121 => 44,  115 => 42,  112 => 41,  110 => 40,  106 => 39,  100 => 36,  97 => 35,  89 => 29,  86 => 28,  79 => 26,  73 => 24,  68 => 22,  63 => 21,  61 => 20,  55 => 18,  51 => 17,  46 => 15,  32 => 3,  29 => 2,);
    }
}
