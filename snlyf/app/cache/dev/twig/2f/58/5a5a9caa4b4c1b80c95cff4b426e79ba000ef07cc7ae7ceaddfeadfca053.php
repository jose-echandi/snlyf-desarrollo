<?php

/* VlabsMediaBundle:Form:vlabs_file.html.twig */
class __TwigTemplate_2f585a5a9caa4b4c1b80c95cff4b426e79ba000ef07cc7ae7ceaddfeadfca053 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'vlabs_file_widget' => array($this, 'block_vlabs_file_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('vlabs_file_widget', $context, $blocks);
    }

    public function block_vlabs_file_widget($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

    ";
        // line 4
        $context["file"] = $this->env->getExtension('vlabs_media_twig_extension')->getBaseFile($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "name"), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent"), "vars"), "data"));
        // line 5
        echo "
    ";
        // line 6
        if ((!(null === (isset($context["file"]) ? $context["file"] : $this->getContext($context, "file"))))) {
            // line 7
            echo "        ";
            if (twig_in_filter($this->getAttribute((isset($context["file"]) ? $context["file"] : $this->getContext($context, "file")), "contentType"), array(0 => "image/jpeg", 1 => "image/png", 2 => "image/gif"))) {
                // line 8
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->env->getExtension('vlabs_media_twig_extension')->filter((isset($context["file"]) ? $context["file"] : $this->getContext($context, "file")), "resize", array("width" => 90, "height" => 90)), "image"), "html", null, true);
                echo "
        ";
            } else {
                // line 10
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate((isset($context["file"]) ? $context["file"] : $this->getContext($context, "file")), "form_doc"), "html", null, true);
                echo "
        ";
            }
            // line 12
            echo "    ";
        }
        // line 13
        echo "
";
    }

    public function getTemplateName()
    {
        return "VlabsMediaBundle:Form:vlabs_file.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  54 => 12,  48 => 10,  42 => 8,  37 => 6,  34 => 5,  26 => 2,  20 => 1,  39 => 7,  216 => 119,  210 => 117,  207 => 116,  176 => 89,  170 => 86,  134 => 52,  128 => 50,  125 => 49,  123 => 48,  119 => 47,  111 => 41,  105 => 38,  102 => 37,  100 => 36,  96 => 35,  93 => 34,  85 => 28,  80 => 25,  73 => 23,  67 => 21,  62 => 19,  57 => 13,  55 => 17,  49 => 15,  45 => 14,  32 => 4,  29 => 2,);
    }
}
