<?php

/* FOSUserBundle:Admin:show.html.twig */
class __TwigTemplate_6fe9f4ce2e9ea6a3bc308a3e404585e279efd1beba02ea0f2fb7414831fc4a1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content_content($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"tit_principal\">Usuario</div>

    <div class=\"col-md-12\">
        <table class=\"record_properties table table-condensed\">
            <thead>
                <tr>
                    <th colspan=\"2\" >Datos del Usuario</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Usuario</th>
                     <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "username"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Tipo Usuario</th>
                    <td>";
        // line 22
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getRoleType", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getRoleType"), "Sin datos")) : ("Sin datos")));
        echo "</td>
                </tr>

                <tr>
                    <th>Nombre</th>
                    <td>";
        // line 27
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getNombre", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getNombre"), "Sin datos")) : ("Sin datos")));
        echo " </td>
                </tr>

                <tr>
                    <th>Apellido</th>
                    <td>";
        // line 32
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getApellido", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getApellido"), "Sin datos")) : ("Sin datos")));
        echo "</td>
                </tr>
                
                <tr>
                    <th>Cédula</th>
                    <td>";
        // line 37
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "ci", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "ci"), "Sin datos")) : ("Sin datos")));
        echo "</td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td>";
        // line 42
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "email", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "email"), "Sin datos")) : ("Sin datos")));
        echo "</td>
                </tr>

                <tr>
                    <th>Estatus</th>
                    <td>";
        // line 47
        echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "enabled") == 1)) ? ("Activo") : ("Inactivo"));
        echo "</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class=\"col-md-6 col-md-offset-2 form-group btn-group\">
        <div class=\"col-md-4\" style=\"text-align:center\"><a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("listado");
        echo "\" class=\"btn btn-primary btn-sm \">Regresar</a></div>
        ";
        // line 55
        echo "        <div class=\"col-md-4\" style=\"text-align:center\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("editar-usuario", array("user_id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
        echo "\" class=\"btn btn-success btn-sm \">Modificar</a></div>
    </div>

    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Eliminar el Registro</h4>
                </div>
                <div class=\"modal-body\">
                    Realmente desea eliminar el registro <b>\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "username"), "html", null, true);
        echo "\"</b>?
                </div>
                <div class=\"modal-footer\">
                ";
        // line 74
        echo "                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Admin:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 74,  122 => 67,  106 => 55,  102 => 53,  93 => 47,  85 => 42,  77 => 37,  69 => 32,  61 => 27,  53 => 22,  46 => 18,  31 => 5,  28 => 4,);
    }
}
