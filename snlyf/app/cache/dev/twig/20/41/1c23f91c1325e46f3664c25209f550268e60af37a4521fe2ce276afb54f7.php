<?php

/* NewTemplateBundle:Menu:mnu_operadora.html.twig */
class __TwigTemplate_20411c23f91c1325e46f3664c25209f550268e60af37a4521fe2ce276afb54f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Datos de Usuario</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("datos-basicos");
        echo "\" title=\"\">Datos Básicos</a></li>
    ";
        // line 8
        echo "</ul>       

<h3>Oficinas</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("operadora_new");
        echo "\" title=\"\">Representante Legal</a></li>
    <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("operadora_new_office");
        echo "\" title=\"\">Oficina Principal</a></li>
    <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("operadora_sucursales");
        echo "\" title=\"\">Sucursales</a></li>
</ul>

<h3>Mis Afiliados</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("Centrohipico_solicitud_afiliacion");
        echo "\" title=\"Solicitar Incorporaci&oacute;n Afiliado\">Solicitar Incorporación Afiliado</a></li>
    <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("Centrohipico_solicitud_afiliacion_listado");
        echo "\" title=\"Consultar Afiliados\">Consultar Afiliados</a></li>
    ";
        // line 22
        echo "</ul>\t
                           
<h3>Mis Solicitudes de Licencia</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("solicitudoperadora_citas_create");
        echo "\" title=\"Crear Solicitud\">Crear</a></li>
    <li><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("solicitudoperadora_list");
        echo "\" title=\"Listado de Solicitudes\">Consultar Solicitudes</a></li>
    ";
        // line 29
        echo "</ul>\t

<h3>Mis Pagos</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "APORTE_MENSUAL"));
        echo "\" title=\"Pago de Aporte Mensual\">Aporte Mensual</a></li>
    <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "PROCESAMIENTO"));
        echo "\" title=\"Pago de Procesamiento\">Procesamiento</a></li>
    <li><a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "OTORGAMIENTO"));
        echo "\" title=\"Pago de Otorgamiento\">Otorgamiento</a></li>
    <li><a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "MULTA"));
        echo "\" title=\"Pago de Multas\">Multa</a></li>
</ul>
            
<h3>Mis Fiscalizaciones</h3>
    <ul class=\"menu2\">
    <li><a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("fiscalizacion_mis");
        echo "\" title=\"Consulta de Fiscalizaciones\">Consultar</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_operadora.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 41,  89 => 36,  85 => 35,  81 => 34,  77 => 33,  71 => 29,  67 => 27,  63 => 26,  57 => 22,  53 => 20,  49 => 19,  41 => 14,  37 => 13,  33 => 12,  27 => 8,  23 => 3,  19 => 1,);
    }
}
