<?php

/* FOSUserBundle:Admin:editarUsuario.html.twig */
class __TwigTemplate_5fd74121a66d81ebf749d38ee84c5f1f52dc50ceda7e53251ec55c1ba7e9c1c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_footer' => array($this, 'block_script_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"page-header\">
        <h1>Editar usuario</h1>
    </div>

    <div class=\"padding-15\">
        ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal", "role" => "form", "id" => "form-user_edit")));
        echo "
        ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), 'errors');
        echo "
        <fieldset>
            <div class=\"form-group\">
                ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "roleType"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "roleType"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "persJuridica"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-3 padding-1\">
                        ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "persJuridica"), 'widget', array("attr" => array("readonly" => "readonly")));
        echo "
                    </div>
                    <div class=\"col-sm-9 padding-1\">
                        ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "rif"), 'widget', array("attr" => array("readonly" => "readonly")));
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "ci"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "ci"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "nombre"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "nombre"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "apellido"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "apellido"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'widget');
        echo "
                        ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'errors');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), 'widget', array("attr" => array("maxlength" => "8")));
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confirm"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confirm"), 'widget', array("attr" => array("maxlength" => "8")));
        echo "
                    </div>
                </div>
            </div>
            <div style=\"color: #E70202;display: none\" id=\"notEquals\"></div>
            <div class=\"form-group\">
                <div class=\"col-sm-2 padding-1\"></div>
                <div class=\"col-sm-10\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "enabled"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-sm-11\">
                    <div class=\"col-sm-12 padding-1\">
                        <button id=\"form_btn\" type=\"submit\" class=\"btn btn-default pull-right\">Actualizar</button>
                    </div>
                </div>
            </div>
        </fieldset>
        ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
        ";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form_perfil"]) ? $context["form_perfil"] : $this->getContext($context, "form_perfil")), "_token"), 'widget');
        echo "
        ";
        // line 100
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
";
    }

    // line 105
    public function block_script_footer($context, array $blocks = array())
    {
        // line 106
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/user/js/Admin/Editar/validate.js"), "html", null, true);
        echo "\"></script>
        <script>
            \$(document).ready(function() {

                \$(\"#form-user_edit\").on('submit', function(event) {
                    event.preventDefault();

                    var pass = \$(\"#user_edit_password\").val();
                    var pass2 = \$(\"#user_edit_confirm\").val();

                    if( pass == pass2 )
                    {
                        \$(this).unbind().submit();
                    }else{
                        var html = \"Las contrase&ntilde;as deben ser iguales\";
                        \$(\"#notEquals\").show();
                        \$(\"#notEquals\").html(html);
                    }
                });


                \$(\".persJuridica\").change(function() {
                    userType = \$('.persJuridica  option:selected').val();
                    if (userType == \"V\" || userType == \"E\") {
                        \$('.ci').removeAttr('disabled');
                    } else {
                        \$('.ci').attr('disabled', 'disabled');
                    }
                });

                \$(\".numeric\").keydown(function(e) {
                    if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                        return;
                    }

                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Admin:editarUsuario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 106,  209 => 105,  202 => 100,  198 => 99,  194 => 98,  179 => 86,  167 => 77,  161 => 74,  153 => 69,  147 => 66,  139 => 61,  135 => 60,  129 => 57,  121 => 52,  115 => 49,  107 => 44,  101 => 41,  93 => 36,  87 => 33,  79 => 28,  73 => 25,  67 => 22,  59 => 17,  53 => 14,  47 => 11,  43 => 10,  39 => 9,  32 => 4,  29 => 3,);
    }
}
