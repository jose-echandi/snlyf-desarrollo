<?php

/* FOSUserBundle:Registro:signup.html.twig */
class __TwigTemplate_4ed00ce8e8cc1731d4ec20c60de8f99e8dfda577d077e25717faf87df867f627 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"padding-15\">
    <div id=\"form_sigup_register\"></div>      
    ";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal", "role" => "form", "id" => "fos_user_signup")));
        echo "
    <fieldset>
        <legend>Introduzca sus Datos</legend>
        <div class=\"form-group\">
            ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "roleType"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "roleType"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "persJuridica"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-3 padding-1\">
                    ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "persJuridica"), 'widget', array("attr" => array("style" => "padding-left: 1px !important")));
        echo "
                </div>
                <div class=\"col-sm-9 padding-1\">
                    ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "rif"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "ci"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "ci"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "nombre"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "nombre"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "apellido"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "apellido"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confirm"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
            <div class=\"col-sm-8\">
                <div class=\"col-sm-12 padding-1\">
                    ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confirm"), 'widget');
        echo "
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            <div class=\"col-sm-11\">
                <div class=\"col-sm-12 padding-1\">
                    <button id=\"form_btn\" type=\"submit\" class=\"btn btn-default pull-right\">Registrarme</button>
                </div>
            </div>
        </div>
    </fieldset>
    ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
    ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
    ";
        // line 83
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</div>

";
        // line 86
        $this->displayBlock('javascripts', $context, $blocks);
    }

    public function block_javascripts($context, array $blocks = array())
    {
        // line 87
        echo "    <script>
        \$(document).ready(function() {
            \$(\".persJuridica\").change(function() {
                userType = \$('.persJuridica  option:selected').val();
                if (userType == \"V\" || userType ==\"E\") {
                    \$('.ci').removeAttr('disabled');
                } else {
                    \$('.ci').attr('value','');
                    \$('.ci').attr('disabled','disabled');
                }
            });

            \$(\".numeric\").keydown(function(e) {
                if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            \$('#fos_user_signup').on('submit', function(e){
                \$(\"#form_sigup_register\").html('<div align=\"center\">Espere...</div>');
                \$(\"#form_btn\").attr(\"disabled\", \"disabled\");
                var submit = false;
                
                \$.ajax({
                    async: false,
                    type: \"POST\",
                    cache: false,
                    url: '";
        // line 117
        echo $this->env->getExtension('routing')->getPath("check_user_registro");
        echo "',
                    data: \$('#fos_user_signup').serializeArray(),
                    success: function(data) {
                        if(data.error==1) {
                            \$(\"#form_sigup_register\").html('<div class=\"alert alert-danger alert-dismissible\" role=\"alert\">\\n\\
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">\\n\\
                                        <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>\\n\\
                                    </button>'+data.msj+'</div>');
                            \$(\"#form_btn\").removeAttr(\"disabled\");
                        } else if(data.error==0) {
                            submit = true;
                        } else {
                            \$(\"#form_sigup_register\").html('<div class=\"alert alert-danger alert-dismissible\" role=\"alert\">\\n\\
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">\\n\\
                                        <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>\\n\\
                                    </button>Vuelva a intentar</div>');
                            \$(\"#form_btn\").removeAttr(\"disabled\");
                        }    
                    }
                });
                return submit;
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registro:signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 117,  176 => 87,  170 => 86,  164 => 83,  160 => 82,  156 => 81,  141 => 69,  135 => 66,  127 => 61,  121 => 58,  113 => 53,  107 => 50,  99 => 45,  93 => 42,  85 => 37,  79 => 34,  71 => 29,  65 => 26,  57 => 21,  51 => 18,  45 => 15,  37 => 10,  31 => 7,  24 => 3,  20 => 1,);
    }
}
