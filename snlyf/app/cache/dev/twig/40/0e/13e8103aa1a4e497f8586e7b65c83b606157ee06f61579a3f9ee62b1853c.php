<?php

/* @WebProfiler/Collector/memory.html.twig */
class __TwigTemplate_400e13e8103aa1a4e497f8586e7b65c83b606157ee06f61579a3f9ee62b1853c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@WebProfiler/Profiler/layout.html.twig");

        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        <span>
            <img width=\"13\" height=\"28\" alt=\"Memory Usage\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAcBAMAAABITyhxAAAAJ1BMVEXNzc3///////////////////////8/Pz////////////+NjY0/Pz9lMO+OAAAADHRSTlMAABAgMDhAWXCvv9e8JUuyAAAAQ0lEQVQI12MQBAMBBmLpMwoMDAw6BxjOOABpHyCdAKRzsNDp5eXl1KBh5oHBAYY9YHoDQ+cqIFjZwGCaBgSpBrjcCwCZgkUHKKvX+wAAAABJRU5ErkJggg==\" />
            <span>";
        // line 7
        echo twig_escape_filter($this->env, sprintf("%.1f", (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "memory") / 1024) / 1024)), "html", null, true);
        echo " MB</span>
        </span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 10
        echo "    ";
        ob_start();
        // line 11
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Memory usage</b>
            <span>";
        // line 13
        echo twig_escape_filter($this->env, sprintf("%.1f", (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "memory") / 1024) / 1024)), "html", null, true);
        echo " / ";
        echo ((($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "memoryLimit") == (-1))) ? ("&infin;") : (twig_escape_filter($this->env, sprintf("%.1f", (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "memoryLimit") / 1024) / 1024)))));
        echo " MB</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 16
        echo "    ";
        $this->env->loadTemplate("@WebProfiler/Profiler/toolbar_item.html.twig")->display(array_merge($context, array("link" => false)));
    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/memory.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 16,  51 => 13,  34 => 5,  31 => 4,  806 => 488,  803 => 487,  792 => 485,  788 => 484,  784 => 482,  771 => 481,  745 => 475,  742 => 474,  723 => 472,  706 => 471,  702 => 469,  698 => 468,  694 => 467,  690 => 466,  686 => 465,  682 => 464,  678 => 463,  675 => 462,  673 => 461,  656 => 460,  645 => 458,  630 => 452,  625 => 450,  621 => 449,  618 => 448,  616 => 447,  602 => 445,  597 => 442,  563 => 410,  545 => 407,  528 => 406,  525 => 405,  523 => 404,  518 => 402,  513 => 400,  246 => 136,  202 => 94,  199 => 93,  196 => 92,  182 => 87,  176 => 86,  173 => 85,  170 => 84,  165 => 83,  163 => 82,  158 => 80,  145 => 74,  136 => 71,  123 => 61,  116 => 57,  109 => 52,  106 => 51,  98 => 45,  92 => 43,  90 => 42,  84 => 40,  68 => 30,  62 => 27,  50 => 22,  47 => 11,  28 => 3,  417 => 143,  411 => 140,  407 => 138,  405 => 137,  398 => 136,  395 => 135,  388 => 134,  384 => 132,  382 => 131,  377 => 129,  374 => 128,  371 => 127,  368 => 126,  365 => 125,  362 => 124,  359 => 123,  356 => 122,  353 => 121,  350 => 120,  347 => 119,  341 => 117,  338 => 116,  333 => 115,  328 => 113,  324 => 112,  315 => 111,  313 => 110,  308 => 109,  305 => 108,  293 => 107,  285 => 100,  281 => 98,  274 => 96,  262 => 93,  249 => 92,  237 => 91,  234 => 90,  232 => 89,  221 => 80,  213 => 78,  210 => 77,  207 => 76,  201 => 74,  186 => 72,  183 => 71,  180 => 70,  177 => 69,  172 => 68,  161 => 58,  159 => 57,  155 => 55,  152 => 54,  147 => 75,  141 => 73,  135 => 46,  133 => 45,  128 => 42,  120 => 38,  117 => 37,  114 => 36,  108 => 33,  101 => 30,  95 => 27,  78 => 19,  75 => 18,  72 => 17,  58 => 25,  44 => 10,  41 => 19,  38 => 7,  204 => 75,  191 => 77,  188 => 90,  185 => 75,  174 => 74,  171 => 73,  167 => 71,  164 => 70,  153 => 77,  138 => 47,  134 => 54,  125 => 41,  121 => 50,  118 => 49,  113 => 48,  111 => 47,  104 => 31,  102 => 41,  96 => 37,  87 => 41,  83 => 33,  64 => 23,  49 => 11,  46 => 10,  43 => 12,  35 => 6,  32 => 6,  27 => 3,  91 => 25,  88 => 24,  82 => 21,  80 => 32,  76 => 34,  73 => 33,  67 => 24,  63 => 18,  61 => 15,  55 => 24,  52 => 12,  45 => 9,  39 => 6,  36 => 5,  33 => 4,  30 => 5,);
    }
}
