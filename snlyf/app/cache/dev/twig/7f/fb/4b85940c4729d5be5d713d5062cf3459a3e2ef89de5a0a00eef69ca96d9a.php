<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_7ffb4b85940c4729d5be5d713d5062cf3459a3e2ef89de5a0a00eef69ca96d9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'content_content' => array($this, 'block_content_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'foot_script' => array($this, 'block_foot_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"block-separator col-md-12\"></div>    
    ";
        // line 6
        $this->displayBlock('content_content', $context, $blocks);
    }

    public function block_content_content($context, array $blocks = array())
    {
        // line 7
        echo "        <div class=\"col-sm-offset-3 col-sm-6\">
            ";
        // line 8
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 58
        echo "        </div>
    ";
    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 9
        echo "                <form class=\"form-horizontal\" role=\"form\" action=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

                    <h2 class=\"form-signin-heading\">Acceso de Usuarios</h2>
                    <div class=\"form-group\">
                        <label for=\"username\" class=\"col-sm-2 control-label\">RIF</label>
                        <div class=\"col-sm-10\">
                            <div class=\"col-sm-2 padding-1\">
                                <select class=\"form-control\" name=\"rif\" id=\"rif\" required=\"required\">
                                    <option value=\"\"></option>
                                    <option value=\"J\">J</option>
                                    <option value=\"G\">G</option>
                                    <option value=\"V\">V</option>
                                    <option value=\"E\">E</option>
                                </select>
                            </div>
                            <div class=\"col-sm-10 padding-1\">
                                <input type=\"hidden\" id=\"username\" name=\"_username\"  />
                                <input type=\"text\" id=\"fake_username\" class=\"form-control numeric\" onblur=\"validateRif(this)\" name=\"_fake_username\" required=\"required\" maxlength=\"9\" title=\"No colocar ni puntos, ni guiones\" />
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label for=\"password\" class=\"col-sm-2 control-label\">Contrase&ntilde;a</label>
                        <div class=\"col-sm-10\">
                            <input type=\"password\" id=\"password\" class=\"form-control\" onblur=\"validatePassword(this)\" name=\"_password\" required=\"required\" 
                                   maxlength=\"8\"/>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-sm-offset-10 col-sm-2\">
                            <button type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-default\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
                        </div>
                    </div>
                </form>

                <div class=\"page-header-border-top\">
                    <ol class=\"breadcrumb text-right\">
                        <li><a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\" data-fancybox-type=\"ajax\" class=\"fancybox\" data-fancybox-width=\"600\" data-fancybox-height=\"120\">¿Olvidó la Contraseña?</a></li>
                        <li><a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("registro");
        echo "\" data-fancybox-type=\"ajax\" class=\"fancybox\" data-fancybox-width=\"550\" data-fancybox-height=\"535\">Registrarse</a></li>
                    </ol>
                </div>
                ";
        // line 51
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 52
            echo "                    <div class=\"alert alert-danger alert-dismissible\"  role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                        <strong>Error! </strong> ";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), array(), "FOSUserBundle"), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 57
        echo "            ";
    }

    // line 62
    public function block_foot_script($context, array $blocks = array())
    {
        // line 63
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/user/js/validaPassword.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$(\".fancybox\").fancybox({
                openEffect: 'elastic',
                closeEffect: 'elastic',
                autoSize: true,
                beforeLoad: function() {
                    this.width = parseInt(this.element.data('fancybox-width'));
                    this.height = parseInt(this.element.data('fancybox-height'));
                },
                padding: 2,
                scrolling: true,
                closeClick: false,
                dataType: 'html',
                headers: {'X-fancyBox': true},
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(58, 42, 45, 0.95)'
                        }
                    }
                }
            });
            
            function result() {
                var s = \$('#rif').find(\":selected\").val();
                var i = \$('#fake_username').val();
                \$('#username').val(s + i);
            }

            \$('#rif').on('change', result);
            \$('#fake_username').on('blur', result);

            \$(\".numeric\").keydown(function(e) {
                if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 63,  130 => 62,  126 => 57,  120 => 54,  116 => 52,  114 => 51,  108 => 48,  104 => 47,  94 => 40,  61 => 10,  56 => 9,  53 => 8,  48 => 58,  46 => 8,  43 => 7,  37 => 6,  34 => 5,  31 => 4,);
    }
}
