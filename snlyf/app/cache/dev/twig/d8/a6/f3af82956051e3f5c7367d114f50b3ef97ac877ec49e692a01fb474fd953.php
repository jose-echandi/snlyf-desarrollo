<?php

/* NewTemplateBundle:Menu:Lateral.html.twig */
class __TwigTemplate_d8a6f3af82956051e3f5c7367d114f50b3ef97ac877ec49e692a01fb474fd953 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 2
            echo "                <div id=\"lateral\" >
                    <h2 class=\"text-center\">Menú Principal</h2>
                    <div id=\"\" class=\"col-md-12  sidebar\">
                        <p class=\"text-center\">Bienvenido, <strong>";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "fullname"), "html", null, true);
            echo "</strong><br/>
                            ";
            // line 6
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_CENTRO_HIPICO")) {
                // line 7
                echo "                                Centro Hípico
                            ";
                // line 9
                echo "                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_OPERADOR")) {
                // line 10
                echo "                                Operadora
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_ADMIN")) {
                // line 12
                echo "                                Administrador
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_FISCAL")) {
                // line 14
                echo "                                Fiscal
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_ASESOR")) {
                // line 16
                echo "                                Asesor Legal
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_SUPER")) {
                // line 18
                echo "                                Super Intendente
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_GERENTE")) {
                // line 20
                echo "                                Gerente
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_COORDINADOR")) {
                // line 22
                echo "                                Coordinador
                            ";
            } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "roles"), 0) == "ROLE_SUPERINTENDENTE")) {
                // line 24
                echo "                                Superintendente
                            ";
            }
            // line 26
            echo "                        </p><br/>
                        ";
            // line 28
            echo "                        <ul class=\"menuu\">
                            <li><img src=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/images/notificacion.png"), "html", null, true);
            echo "\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("notification");
            echo "\">Notificaciones <span class=\"notif\" id=\"notificationCount\">0</span></a></li>
                            <li><img src=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/images/contrasena.png"), "html", null, true);
            echo "\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
            echo "\">Cambiar Contrase&ntilde;a</a></li>
                        </ul>
                         <h3 ><a href=\"";
            // line 32
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-default btn-small\">Cerrar sesión</a></h3>
                    </div><br/><p></p>
                    ";
            // line 34
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                echo " ";
                // line 35
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_admin.html.twig")->display($context);
                // line 36
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_GERENTE")) {
                // line 37
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_gerente.html.twig")->display($context);
                // line 38
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_LICENCIA")) {
                // line 39
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_licencia.html.twig")->display($context);
                // line 40
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_OPERADOR")) {
                // line 41
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_operadora.html.twig")->display($context);
                // line 42
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_FISCAL")) {
                // line 43
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_fiscal.html.twig")->display($context);
                // line 44
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_ASESOR")) {
                // line 45
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_asesor.html.twig")->display($context);
                // line 46
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_CENTRO_HIPICO")) {
                // line 47
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_usuario.html.twig")->display($context);
                // line 48
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_SUPERINTENDENTE")) {
                // line 49
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_superintendente.html.twig")->display($context);
                // line 50
                echo "                    ";
            } elseif ($this->env->getExtension('security')->isGranted("ROLE_COORDINADOR")) {
                // line 51
                echo "                        ";
                $this->env->loadTemplate("NewTemplateBundle:Menu:mnu_coordinador.html.twig")->display($context);
                // line 52
                echo "                    ";
            }
            // line 53
            echo "                </div>
    ";
        }
        // line 55
        echo "<script>
    function getNotifications()
    {
        var url = Routing.generate('notification_count');
        \$.get( url, function( data ) {
            //console.info(\"notificaciones-\",data);
            \$(\"#notificationCount\").text(data);
        });
    }
    \$(document).ready(function() {
        getNotifications();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:Lateral.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 55,  151 => 53,  148 => 52,  145 => 51,  142 => 50,  139 => 49,  136 => 48,  133 => 47,  130 => 46,  127 => 45,  124 => 44,  121 => 43,  118 => 42,  115 => 41,  112 => 40,  109 => 39,  106 => 38,  103 => 37,  100 => 36,  97 => 35,  94 => 34,  89 => 32,  82 => 30,  76 => 29,  73 => 28,  70 => 26,  66 => 24,  62 => 22,  58 => 20,  54 => 18,  50 => 16,  46 => 14,  42 => 12,  38 => 10,  35 => 9,  32 => 7,  30 => 6,  26 => 5,  21 => 2,  19 => 1,  52 => 13,  43 => 11,  39 => 10,  31 => 4,  28 => 3,);
    }
}
