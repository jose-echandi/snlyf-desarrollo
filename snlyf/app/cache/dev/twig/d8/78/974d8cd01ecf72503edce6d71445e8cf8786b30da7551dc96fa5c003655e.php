<?php

/* LicenciaBundle:AdmTipoAporte:index.html.twig */
class __TwigTemplate_d878974d8cd01ecf72503edce6d71445e8cf8786b30da7551dc96fa5c003655e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 10
    public function block_content_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>Listado de Aportes por Licencia</h1>
    </div>
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
            <a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("admtipoaporte_new");
        echo "\" class=\"btn btn-primary btn-sm \"><i class=\"icon-plus-sign\"></i> Agregar Aporte</a>
        </div>
        <div class=\"col-sm-10 block-separator\"></div>
    </div>
   <div id=\"action\">
        <div class=\"rigth\">
            ";
        // line 24
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 25
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 26
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            echo "
                </article>
            ";
        }
        // line 29
        echo "        </div>
    </div>
    <div class=\"col-md-12\">
        <table class=\"table table-condensed table-striped\">
            <thead>
                <tr>
                    <th>Licencia</th>
                    <th>Establecimiento</th>
                    <th>Por Juego</th>
                    <th>Monto</th>
                    <th>Estatus</th>
                    <th width=\"20%\" colspan=\"2\" >Acciones</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 45
            echo "                <tr>
                    <td><a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admtipoaporte_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "admClasfLicencias"), "clasfLicencia"), "html", null, true);
            echo "</a></td>
                    <td>
                        <ul>
                        ";
            // line 49
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "admClasfEstab"));
            foreach ($context['_seq'] as $context["_key"] => $context["establecimiento"]) {
                // line 50
                echo "                            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["establecimiento"]) ? $context["establecimiento"] : $this->getContext($context, "establecimiento")), "clasificacionCentrohipico"), "html", null, true);
                echo "</li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['establecimiento'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "                        </ul>
                    </td>
                    <td>";
            // line 54
            echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "porJuego") == 1)) ? ("Si") : ("No"));
            echo "</td>
                    <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "montoAporte"), "html", null, true);
            echo "</td>
                    <td>";
            // line 56
            echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") == 1)) ? ("Activo") : ("Inactivo"));
            echo "</td>
                    <td style=\"text-align:center;\">
                        <a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admtipoaporte_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-default btn-sm\">Editar</a>
                        <a href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admtipoaporte_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" class=\"btn btn-info btn-sm\">Mostrar</a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "            </tbody>
        </table>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    ";
        // line 74
        echo "
";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmTipoAporte:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 74,  156 => 63,  146 => 59,  142 => 58,  137 => 56,  133 => 55,  129 => 54,  125 => 52,  116 => 50,  112 => 49,  104 => 46,  101 => 45,  97 => 44,  80 => 29,  74 => 26,  71 => 25,  69 => 24,  60 => 18,  51 => 11,  48 => 10,  41 => 6,  37 => 5,  32 => 4,  29 => 3,);
    }
}
