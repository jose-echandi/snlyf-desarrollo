<?php

/* FOSUserBundle:Admin:nuevoUsuario.html.twig */
class __TwigTemplate_844916b884aa3eeeb9b610def777c8b57f8f4c0f8130e2cb72e293412a8d429f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"page-header\">
        <h1>Crear nuevo usuario</h1>
    </div>

    <div class=\"padding-15\">
        ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal", "role" => "form")));
        echo "
        <fieldset>
            <div class=\"form-group\">
                ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "roleType"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "roleType"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "persJuridica"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-3 padding-1\">
                        ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "persJuridica"), 'widget');
        echo "
                    </div>
                    <div class=\"col-sm-9 padding-1\">
                        ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "rif"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "ci"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "ci"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "nombre"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "nombre"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "apellido"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "perfil"), "apellido"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confirm"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                <div class=\"col-sm-8\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confirm"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-sm-2 padding-1\"></div>
                <div class=\"col-sm-10\">
                    <div class=\"col-sm-12 padding-1\">
                        ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "enabled"), 'widget');
        echo "
                    </div>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-sm-11\">
                    <div class=\"col-sm-12 padding-1\">
                        <button id=\"form_btn\" type=\"submit\" class=\"btn btn-default pull-right\">Crear</button>
                    </div>
                </div>
            </div>
        </fieldset>
        ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
        ";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        ";
        // line 96
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>

    ";
        // line 99
        $this->displayBlock('script_base', $context, $blocks);
    }

    public function block_script_base($context, array $blocks = array())
    {
        // line 100
        echo "        <script>
            \$(document).ready(function() {
                \$(\".persJuridica\").change(function() {
                    userType = \$('.persJuridica  option:selected').val();
                    if (userType == \"V\" || userType == \"E\") {
                        \$('.ci').removeAttr('disabled');
                    } else {
                        \$('.ci').attr('disabled', 'disabled');
                    }
                });

                \$(\".numeric\").keydown(function(e) {
                    if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                        return;
                    }

                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Admin:nuevoUsuario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 100,  195 => 99,  189 => 96,  185 => 95,  181 => 94,  166 => 82,  155 => 74,  149 => 71,  141 => 66,  135 => 63,  127 => 58,  121 => 55,  113 => 50,  107 => 47,  99 => 42,  93 => 39,  85 => 34,  79 => 31,  71 => 26,  65 => 23,  59 => 20,  51 => 15,  45 => 12,  39 => 9,  32 => 4,  29 => 3,);
    }
}
