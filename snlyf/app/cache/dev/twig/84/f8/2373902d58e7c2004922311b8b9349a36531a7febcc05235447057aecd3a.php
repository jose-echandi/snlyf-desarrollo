<?php

/* CentrohipicoBundle:DataEmpresa:list.html.twig */
class __TwigTemplate_84f82373902d58e7c2004922311b8b9349a36531a7febcc05235447057aecd3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>Empresas</h1>
    </div>

    <div id=\"action\">
        <div class=\"left\">
            ";
        // line 10
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 11
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 12
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            echo "
                </article>
            ";
        }
        // line 15
        echo "        </div>
        <div class=\"right\">
            <a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("data_empresa_new");
        echo "\" class=\"btn btn-primary\" style=\"float:right; margin: 20px 25px 10px 0px;height: 32px\"> Agregar nueva empresa </a>
        </div>
    </div>

    ";
        // line 21
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 22
            echo "        </br>
        <div class=\"col-md-12\">
            <table class=\"table table-condensed table-striped\">
                <thead>
                <tr>
                    <th>No</th>
                    <th>RIF</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                ";
            // line 34
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 35
                echo "                    <tr id=\"request-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
                echo "\">
                        <td>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
                echo "</td>
                        <td align=\"center\">";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "persJuridica"), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rif"), "html", null, true);
                echo "</td>
                        <td align=\"center\">
                            <a href=\"#\" id=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
                echo "\"  data-toggle=\"modal\" data-target=\"#myModal\" class=\"show_detail\">
                                ";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "denominacionComercial"), "html", null, true);
                echo "
                            </a>
                        </td>
                        <td class=\"text-center\">
                            <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("data_empresa_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\"><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Editar\"></i></a>
                            <a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("data_empresa_add_partner", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\"><i class=\"fa fa-group\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Agregar socio\"></i></a>
                        </td>
                    </tr>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "                </tbody>
            </table>
        </div>
        <div class=\"col-md-12\">
            <div id=\"action\">
                <div class=\"left\">
                    ";
            // line 55
            if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
                // line 56
                echo "                        <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                            ";
                // line 57
                echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
                echo "
                        </article>
                    ";
            }
            // line 60
            echo "                </div>
                <div class=\"right\">
                    </br>
                </div>
            </div>
        </div>

    ";
        } else {
            // line 68
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>";
            // line 71
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 76
        echo "    <div class=\"block-separator col-sm-12\"></div>


    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">
                        <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>
                    </button>
                </div>
                <div class=\"modal-body\" id=\"datos_empresas\"> cargando...</div>
                <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>

    <script>
        \$(function() {
            \$(\".show_detail\").click(function(ev) {
                var id = \$(this).attr(\"id\");
                \$.get(Routing.generate('data_empresa_detail', {id: id}),
                        function(data) {
                            \$('#datos_empresas').html(data);// llenarel div llenar del body de la ventana modal
                        });
            });
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 76,  178 => 71,  173 => 68,  163 => 60,  157 => 57,  154 => 56,  152 => 55,  144 => 49,  126 => 45,  122 => 44,  115 => 40,  111 => 39,  104 => 37,  100 => 36,  95 => 35,  78 => 34,  64 => 22,  62 => 21,  55 => 17,  51 => 15,  45 => 12,  42 => 11,  40 => 10,  31 => 3,  28 => 2,);
    }
}
