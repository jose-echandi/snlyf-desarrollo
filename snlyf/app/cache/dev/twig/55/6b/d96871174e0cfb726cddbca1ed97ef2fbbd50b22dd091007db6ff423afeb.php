<?php

/* SolicitudesCitasBundle:AdminLicencias:listado.html.twig */
class __TwigTemplate_556bd96871174e0cfb726cddbca1ed97ef2fbbd50b22dd091007db6ff423afeb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((((isset($context["ajax"]) ? $context["ajax"] : $this->getContext($context, "ajax")) == false)) ? ("NewTemplateBundle::base.html.twig") : ("NewTemplateBundle::base_ajax.html.twig")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <style>
        #table_header td{
            color:white;
        }
    </style>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>
            Licencias aprobadas
        </h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
        </div>
        <div class=\"col-sm-10 block-separator\"></div>
    </div>

    ";
        // line 24
        if (array_key_exists("entities", $context)) {
            // line 25
            echo "
        <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
            ";
            // line 27
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            echo "
        </article>
";
            // line 29
            echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => "exportpdf_licencias_aprob", "xcel" => "exportxls_licencias_aprob"));
            echo "

        <div class=\"col-md-12\">
            <table class=\"table table-condensed table-striped\">
                <thead>
                <tr>
                    <th>Nº</th>
                    <th>RIF</th>
                    <th>Denominación Comercial</th>
                    <th>Licencia</th>
                    <th>Clasificación</th>
                    <th>No Licencia</th>
                    <th>No Providencia</th>
                    <th>F. Aprobación</th>
                    <th>F. Vencimiento</th>
                     ";
            // line 44
            if ((!$this->env->getExtension('security')->isGranted("ROLE_ASESOR"))) {
                // line 45
                echo "                         <th><a href=\"#\">Acciones</a></th>";
            }
            // line 46
            echo "                </tr>
                </thead>
                <tbody>
                ";
            // line 49
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 50
                echo "                    <tr id=\"request-";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "id"), "html", null, true);
                echo "\">
                        <td>";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
                echo "</td>
                        <td>
                            ";
                // line 53
                if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), 0, array(), "any", false, true), "solicitud", array(), "any", false, true), "operadora", array(), "any", false, true), "persJuridica", array(), "any", true, true)) {
                    // line 54
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "operadora"), "persJuridica"), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "operadora"), "rif"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 56
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "centroHipico"), "persJuridica"), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "centroHipico"), "rif"), "html", null, true);
                    echo "
                            ";
                }
                // line 58
                echo "                        </td>
                        <td>
                            ";
                // line 60
                if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), 0, array(), "any", false, true), "solicitud", array(), "any", false, true), "operadora", array(), "any", false, true), "denominacionComercial", array(), "any", true, true)) {
                    // line 61
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "operadora"), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 63
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "centroHipico"), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                }
                // line 65
                echo "                        </td>
                        <td>";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "clasLicencia"), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
                echo "</td>
                        <td>";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "clasLicencia"), "clasfLicencia"), "html", null, true);
                echo "</td>
                        <td>";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "numLicenciaAdscrita"), "html", null, true);
                echo "</td>
                        <td>";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "numProvidencia"), "html", null, true);
                echo "</td>
                        <td>";
                // line 70
                echo twig_escape_filter($this->env, _twig_default_filter(twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "solicitud"), "fechaAprobada"), "d/m/Y"), "none"), "html", null, true);
                echo "</td>
                        <td>";
                // line 71
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "fechaFin"), "d/m/Y"), "html", null, true);
                echo "</td>
                        ";
                // line 72
                if ((!$this->env->getExtension('security')->isGranted("ROLE_ASESOR"))) {
                    // line 73
                    echo "                        <td>                            
                            <a  target=\"_blank\" href=\"";
                    // line 74
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudes_aprobadas_imprimir", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), 0), "id"))), "html", null, true);
                    echo "\">Imprimir</a>                            
                        </td>
                      ";
                }
                // line 76
                echo "  
                    </tr>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                </tbody>
            </table>
        </div>
    ";
        } else {
            // line 83
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No se encontraron resultados</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 91
        echo "    <div class=\"block-separator col-sm-12\"></div>

    <div id=\"myModal\" class=\"reveal-modal\" data-animation=\"fade\" style=\"top:-100px\">
        <a class=\"close-reveal-modal\">&#215;</a>
        <div id=\"dataModal\">
            <h3> Cargando..... </h3>
        </div>
    </div>
    <script>
    </script>
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:AdminLicencias:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 91,  213 => 83,  207 => 79,  191 => 76,  185 => 74,  182 => 73,  180 => 72,  176 => 71,  172 => 70,  168 => 69,  164 => 68,  160 => 67,  156 => 66,  153 => 65,  147 => 63,  141 => 61,  139 => 60,  135 => 58,  127 => 56,  119 => 54,  117 => 53,  112 => 51,  107 => 50,  90 => 49,  85 => 46,  82 => 45,  80 => 44,  62 => 29,  57 => 27,  53 => 25,  51 => 24,  29 => 4,  26 => 3,);
    }
}
