<?php

/* SolicitudesCitasBundle:DataSolicitudes:index.html.twig */
class __TwigTemplate_1c6539e8d2131a36f41121a2577e4e219a451562329adf5e73ece18fa1b69cab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"block-separator col-md-12\"></div>
    <h1 class=\"tit_principal\">Mis solicitudes de Licencia</h1>
    ";
        // line 6
        echo "         <br /><br /><br />
    <table class=\"table table-condensed table-striped\">
        <thead>
        <tr>
                            <th>Nº</th>
                            <th>Nº Localizador</th>
                            <th>Centro Hípico</th>
                            <th>F. Solicitud </th>
                            <th>F. de Cita  &nbsp;</th>
                            <th>Lic. Tipo</th>
                            <th>Status</th>
                            <th>Acciones</th>
\t               </tr>
        </thead>
        <tbody>
              ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            echo "          
                        <tr >
                            <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "</td>
                            <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "codsolicitud"), "html", null, true);
            echo "</td>
                            <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "centroHipico"), "denominacionComercial"), "html", null, true);
            echo "</td>
                            <td>";
            // line 26
            if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fechaSolicitud")))) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fechaSolicitud"), "d-m-Y"), "html", null, true);
                echo " ";
            }
            echo "</td>
                            <td>";
            // line 27
            if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cita")))) {
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
                echo " ";
            } else {
                echo " None ";
            }
            echo "</td>
                            <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "ClasLicencia"), "clasfLicencia"), "html", null, true);
            echo "</td>
                            <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status"), "html", null, true);
            echo " </td>
                            <td>
                               ";
            // line 32
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudes_printGen", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tipo" => "1")), "html", null, true);
            echo "\" target=\"_blank\" 
                                   class=\"btn btn-info\" title=\"Ver e Imprimir\" ";
            // line 33
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") != "Solicitada")) {
                echo " disabled ";
            }
            echo ">
                                    <i class=\"fa fa-print\"></i>
                                </a><!--Ver-->
                                <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudes_cambiarcita", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" 
                                    class=\"btn btn-success\" title=\"Reprogramar Cita\" ";
            // line 37
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") != "Solicitada")) {
                echo " disabled ";
            }
            echo ">
                                    <i class=\"fa fa-clock-o\"></i>
                                </a><!--Cambiar Cita-->
                                <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("datasolicitudes_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" 
                                    class=\"btn btn-warning\" title=\"Cambiar Datos\" ";
            // line 41
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "status") != "Solicitada")) {
                echo " disabled ";
            }
            echo ">
                                    <i class=\"fa fa-edit\"></i>
                                </a><!--Cambiar Datos-->
                                ";
            // line 45
            echo "                               ";
            // line 46
            echo "                            </td>
                         </tr>
                 ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                 ";
        if ((!(isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")))) {
            // line 50
            echo "                     <tr><td class=\"text-center\" colspan=\"8\">No Existen Registros de Solicitudes</td></tr>
               ";
        }
        // line 52
        echo "\t\t</tbody>
             </table>        
    ";
    }

    // line 55
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 56
        echo "    ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        
    </script>

";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 56,  179 => 55,  173 => 52,  169 => 50,  166 => 49,  150 => 46,  148 => 45,  140 => 41,  136 => 40,  128 => 37,  124 => 36,  116 => 33,  111 => 32,  106 => 29,  102 => 28,  92 => 27,  85 => 26,  81 => 25,  77 => 24,  73 => 23,  53 => 21,  36 => 6,  32 => 3,  29 => 2,);
    }
}
