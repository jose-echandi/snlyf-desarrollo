<?php

/* VlabsMediaBundle:Form:vlabs_del_file.html.twig */
class __TwigTemplate_49464024770e5649669b8dcd509c1727fdf6f5398f6a6fbd5475445ce0fe1085 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'vlabs_del_file_widget' => array($this, 'block_vlabs_del_file_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('vlabs_del_file_widget', $context, $blocks);
    }

    public function block_vlabs_del_file_widget($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
";
    }

    public function getTemplateName()
    {
        return "VlabsMediaBundle:Form:vlabs_del_file.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  20 => 1,  39 => 6,  216 => 119,  210 => 117,  207 => 116,  176 => 89,  170 => 86,  134 => 52,  128 => 50,  125 => 49,  123 => 48,  119 => 47,  111 => 41,  105 => 38,  102 => 37,  100 => 36,  96 => 35,  93 => 34,  85 => 28,  80 => 25,  73 => 23,  67 => 21,  62 => 19,  57 => 18,  55 => 17,  49 => 15,  45 => 14,  32 => 3,  29 => 2,);
    }
}
