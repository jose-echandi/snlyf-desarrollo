<?php

/* CentrohipicoBundle::centroh_base.html.twig */
class __TwigTemplate_238ffef296b6eb9324451158af0e440fdef87e314b13f3d53d5a3dc1fe395a2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 6
    public function block_content_content($context, array $blocks = array())
    {
        echo "   
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  138 => 55,  135 => 54,  113 => 34,  104 => 30,  84 => 24,  80 => 23,  73 => 21,  69 => 20,  66 => 19,  49 => 18,  32 => 3,  29 => 2,);
    }
}
