<?php

/* @WebProfiler/Profiler/base_js.html.twig */
class __TwigTemplate_54d8683f0b9a71bc73ac6ba234a9a23a23e18a33be6d8289c29144198c57c82d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>/*<![CDATA[*/
    Sfjs = (function() {
        \"use strict\";

        var noop = function() {},

            profilerStorageKey = 'sf2/profiler/',

            request = function(url, onSuccess, onError, payload, options) {
                var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
                options = options || {};
                options.maxTries = options.maxTries || 0;
                xhr.open(options.method || 'GET', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.onreadystatechange = function(state) {
                    if (4 !== xhr.readyState) {
                        return null;
                    }

                    if (xhr.status == 404 && options.maxTries > 1) {
                        setTimeout(function(){
                            options.maxTries--;
                            request(url, onSuccess, onError, payload, options);
                        }, 500);

                        return null;
                    }

                    if (200 === xhr.status) {
                        (onSuccess || noop)(xhr);
                    } else {
                        (onError || noop)(xhr);
                    }
                };
                xhr.send(payload || '');
            },

            hasClass = function(el, klass) {
                return el.className && el.className.match(new RegExp('\\\\b' + klass + '\\\\b'));
            },

            removeClass = function(el, klass) {
                if (el.className) {
                    el.className = el.className.replace(new RegExp('\\\\b' + klass + '\\\\b'), ' ');
                }
            },

            addClass = function(el, klass) {
                if (!hasClass(el, klass)) {
                    el.className += \" \" + klass;
                }
            },

            getPreference = function(name) {
                if (!window.localStorage) {
                    return null;
                }

                return localStorage.getItem(profilerStorageKey + name);
            },

            setPreference = function(name, value) {
                if (!window.localStorage) {
                    return null;
                }

                localStorage.setItem(profilerStorageKey + name, value);
            };

        return {
            hasClass: hasClass,

            removeClass: removeClass,

            addClass: addClass,

            getPreference: getPreference,

            setPreference: setPreference,

            request: request,

            load: function(selector, url, onSuccess, onError, options) {
                var el = document.getElementById(selector);

                if (el && el.getAttribute('data-sfurl') !== url) {
                    request(
                        url,
                        function(xhr) {
                            el.innerHTML = xhr.responseText;
                            el.setAttribute('data-sfurl', url);
                            removeClass(el, 'loading');
                            (onSuccess || noop)(xhr, el);
                        },
                        function(xhr) { (onError || noop)(xhr, el); },
                        '',
                        options
                    );
                }

                return this;
            },

            toggle: function(selector, elOn, elOff) {
                var i,
                    style,
                    tmp = elOn.style.display,
                    el = document.getElementById(selector);

                elOn.style.display = elOff.style.display;
                elOff.style.display = tmp;

                if (el) {
                    el.style.display = 'none' === tmp ? 'none' : 'block';
                }

                return this;
            }
        }
    })();
/*]]>*/</script>
";
    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/base_js.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  83 => 30,  70 => 26,  42 => 12,  32 => 6,  30 => 5,  26 => 3,  24 => 2,  250 => 51,  241 => 49,  236 => 48,  233 => 47,  211 => 41,  207 => 40,  204 => 39,  198 => 37,  196 => 36,  193 => 35,  190 => 34,  164 => 30,  149 => 25,  143 => 23,  123 => 19,  117 => 17,  87 => 14,  63 => 7,  60 => 6,  54 => 3,  27 => 1,  22 => 32,  19 => 1,  633 => 192,  630 => 191,  626 => 185,  623 => 184,  617 => 186,  614 => 184,  612 => 180,  609 => 179,  605 => 178,  602 => 177,  598 => 129,  595 => 128,  589 => 125,  584 => 123,  581 => 122,  577 => 120,  574 => 119,  569 => 116,  566 => 115,  560 => 113,  556 => 107,  553 => 106,  549 => 103,  546 => 102,  541 => 108,  539 => 106,  535 => 104,  533 => 102,  530 => 101,  527 => 100,  522 => 110,  520 => 100,  517 => 99,  514 => 98,  510 => 88,  503 => 84,  499 => 82,  496 => 81,  493 => 80,  488 => 78,  482 => 77,  478 => 75,  475 => 74,  462 => 73,  458 => 126,  455 => 125,  453 => 122,  450 => 121,  448 => 119,  445 => 118,  443 => 115,  440 => 114,  438 => 113,  435 => 112,  432 => 98,  426 => 97,  416 => 93,  412 => 92,  409 => 91,  404 => 90,  399 => 89,  397 => 80,  394 => 79,  391 => 78,  389 => 77,  386 => 76,  383 => 74,  380 => 73,  377 => 72,  372 => 69,  369 => 68,  365 => 189,  363 => 179,  360 => 178,  358 => 177,  330 => 152,  325 => 151,  322 => 149,  302 => 147,  298 => 141,  294 => 140,  290 => 139,  286 => 138,  282 => 137,  278 => 136,  274 => 135,  270 => 134,  265 => 133,  261 => 131,  258 => 130,  256 => 52,  253 => 127,  251 => 72,  248 => 71,  245 => 68,  242 => 67,  238 => 65,  235 => 64,  230 => 46,  227 => 45,  223 => 44,  220 => 43,  214 => 42,  210 => 51,  208 => 44,  205 => 43,  201 => 38,  199 => 35,  197 => 34,  194 => 33,  189 => 57,  187 => 55,  183 => 54,  179 => 53,  176 => 33,  174 => 43,  169 => 40,  167 => 33,  162 => 31,  157 => 30,  152 => 26,  148 => 26,  144 => 25,  140 => 22,  132 => 22,  129 => 21,  109 => 19,  105 => 13,  100 => 10,  97 => 9,  92 => 6,  89 => 5,  84 => 193,  82 => 191,  79 => 29,  77 => 67,  74 => 66,  72 => 64,  69 => 9,  67 => 60,  64 => 59,  62 => 24,  59 => 8,  57 => 5,  52 => 2,  50 => 15,  225 => 100,  222 => 99,  218 => 101,  215 => 99,  212 => 98,  173 => 64,  170 => 63,  158 => 28,  153 => 51,  147 => 52,  145 => 51,  141 => 49,  139 => 48,  136 => 23,  115 => 16,  101 => 22,  98 => 21,  95 => 20,  91 => 35,  88 => 18,  81 => 15,  78 => 14,  75 => 28,  66 => 25,  41 => 4,  38 => 3,  133 => 45,  130 => 44,  126 => 20,  120 => 18,  116 => 52,  114 => 51,  108 => 48,  104 => 15,  94 => 40,  61 => 10,  56 => 4,  53 => 8,  48 => 58,  46 => 14,  43 => 2,  37 => 6,  34 => 5,  31 => 4,);
    }
}
