<?php

/* NewTemplateBundle:Menu:mnu_admin.html.twig */
class __TwigTemplate_2eec03650a97eaacbb91eda017273a5ea8911b0b205840a37c3d24534f8aa414 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Administración de Usuarios</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("listado");
        echo "\" title=\"Usuarios\">Usuarios</a></li>
</ul>

<h3>Administrador de Licencias</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("admtipoaporte");
        echo "\" title=\"Aportes de Licencias\">Aportes de Licencias</a></li>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("admclasflicencias");
        echo "\" title=\"Licencias\">Licencias</a></li>
    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("admtiposlicencias");
        echo "\" title=\"Tipos de Licencias\">Tipos de Licencias</a></li>
    <li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("admrecaudoslicencias");
        echo "\" title=\"Recaudos\">Recaudos</a></li>
    <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("admjuegosexplotados");
        echo "\" title=\"Juegos Explotados\">Juegos Explotados</a></li>
    <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("admclasfestab");
        echo "\" title=\"Clasificación de Establecimientos\">Clasificación de Establecimientos</a></li>    
</ul>

<h3>Bancos</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("banco");
        echo "\" title=\"Bancos\">Consultar</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 18,  51 => 13,  47 => 12,  23 => 3,  155 => 55,  151 => 53,  148 => 52,  145 => 51,  142 => 50,  139 => 49,  136 => 48,  133 => 47,  130 => 46,  127 => 45,  124 => 44,  121 => 43,  118 => 42,  115 => 41,  112 => 40,  109 => 39,  106 => 38,  103 => 37,  100 => 36,  97 => 35,  94 => 34,  89 => 32,  82 => 30,  76 => 29,  73 => 28,  70 => 26,  66 => 24,  62 => 22,  58 => 20,  54 => 18,  50 => 16,  46 => 14,  42 => 12,  38 => 10,  35 => 9,  32 => 7,  30 => 6,  26 => 5,  21 => 2,  19 => 1,  52 => 13,  43 => 11,  39 => 10,  31 => 8,  28 => 3,);
    }
}
