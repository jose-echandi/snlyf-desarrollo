<?php

/* TwigBundle:Exception:exception.html.twig */
class __TwigTemplate_886cb6bf924e0319f5da6eada89558f31d719c13d9c0baca5377be8d296f52f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"block-exception\">
    <div class=\"block-exception-detected clear-fix\">
        <div class=\"illustration-exception\">
            <img alt=\"Exception detected!\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAAB8CAMAAACG/MQCAAADAFBMVEX29vbt8eLn7tTY5rPT46nI3ZHC2oO51XDH3I3J3pPe6cHy9O7y9Ozg6sXQ4aLA2X+w0FyqzU6202jF24nV5K3k7M7q8NzS46a/2Xqv0FrD24Xb6Lvw8+jx8+rU5Ku31Gz19vTl7dHE24eszlLN4Jvj7My913js8eDo7ta813arzVC61nLz9fCuz1bZ57br8N631Wnc6L2tz1S102ay0WDh68r09fLO4J3L4JOx0V3a6bL////k78fX5bC002PI34z8/fnS5aHV5qjt9Nro8c/F3Ybp79qx0V7d67i713Gz0mGy0l/2+uzy9+Tc6raw0Vrp8tHH3ori7sPn8c2v0Fj9/vv1+erw9uDB2374+/DA2nzX56zm8Mvx9+K+2XjP45vq89PT5aPZ6bD+/v3Y6K7s9Ne82HPW56rf7LyrzlDK3pbD3IK21Gfu9dzr89XM4ZW92Ha61m/3+u7E3ITC24Cuz1jQ4535+/PO4pnG3Yj0+eje7LrJ34+51m36/PX7/ffv9t7j78XK4JHh7cHN4pfR5J+41Wvz+Obb6rTg7b7G3Iva57nP4Z+71nTU5qW+2HnI3Y/B2oHh6sfp79ju8uSz0mLf6cPv8ua/2X200mT29vbl5eW+vr6SkpJoaGhkZGREREQ/Pz9YWFh7e3uoqKjU1NTy8vK1tbVmZmZKSkqOjo7e3t7w8PCamppMTExzc3PX19fQ0NBbW1ucnJygoKBBQUFgYGDh4eHj4+NjY2OMjIyIiIh1dXV3d3e8vLxwcHDn5+f8/Pz7+/vZ2dldXV34+Pj6+vpGRkbq6ur5+fn39/f19fWGhoZRUVHb29uKiopqamrMzMxISEjp6ens7OyUlJSkpKSCgoLExMTa2trx8fGysrLCwsLr6+vBwcH09PTHx8dTU1OYmJienp5OTk6srKyvr6+AgIDt7e3KysqXl5dubm7Y2NiEhITPz8/R0dHS0tKQkJDz8/NVVVXi4uKxsbFsbGyioqJWVlarq6t+fn59fX3c3Nzv7++6urq4uLhQzU1ZAAAAAXRSTlMAQObYZgAACuNJREFUeNrtmX1cFWUWx8dcU9wyj72Brpt1215UisA24JQgYAsKggmISvhyEbkJQkgQmoCBmW3Zm9vujoo+YIj4UiqQGaVcHfSOr1ipWZZllmWbprm71lZ7nvF+7n2QvThzmfhrv3/APDPw/J5z5pznnJmRdNDliq6/6dbtyu6cHt18uva84rfSr8VVV/e6pje0pc+1111vvtoNN/pCO/j17fc7E9X6//4muDzX9BxgjtzNt1hAH71vNcHMP9wGBrB0u71jcnfcCQYZ6NO/A3HpMxCM49fVW72efuAdg27wRm6wD3iN/13G9e4OgI5w7WCjwekHHWOQsQy5JxA6StDdBvT6gQn01h86PQeCGQzpolPvXguYQ5C++3i7H5jFbXq28z92B/O4T4egD5jJ1ZfVux4M4x8c4jlwLteBDPDCoaF4v/dOfQCMMBQ4YeHDwEVgRGTUcLFCPth+xPwJDBCNMcAZMQJgZHRs3Kh4gAQkRou7aruCXcEID+GY6Mi4iMSkOIhPRmJsYgqOGz8Bk8WK3F76D+gDAimpD6d56CQmRk2a7A9TkDPGglZLOk4NyBiL04IxE2xWTAQ3j7S3x4DAMEQME0SmZwEMj7JmB4NlEhJx8RNxRk7uowB5OHQGjgeYiQn5mAqQjGJte6ydStUD3BRg+DQKipBEMjXTAr6FmA05yMl/HPOKwmZhehGfffRsWzamReETQJfHzMGxADMwBQTu8exRPyHaipEHm39JKUAqTg8pwdIwSzEOm4ux8XPxSQBfjMxAuliKGfdjWAEWRoQmYVgiFgKU0XVdYXMzuMnFkpwJ0eWWcMyIj8W0eRhlg6dwfj6Gx0AqX8t4LA0Mt4aFWa0jQ3Ge7X4kMm2QOgYgAkNBwM+j4HXgZjpyxsEoRCs+bKE5AYKwMGbSTJ4OE2HmfEyHaUg8DTk0huHR0XRJIw2TFtCxC4+F8RlwMwetsyfnBIElFhHnjyzHUkqySCwtKCiAyZhOUjMSAcqzpxYAwKPQmqeRPOvmz54ExW3NNgunATEeS0LjMNQ/Ga2RuVl5SAx9Ah/yTS0IBM+k5OQKo2c9CfYGgZmUYQX5lmmk+yjGQRalQjkkTowOHU7xMhWM8IwnQWjFk8WIGBxMN8/2EN8sQ1zJlYXJYARffYIQP2zB00GWDGiDLS4KjHCnR0FTyC2LT0nKAYGbfl3BaCwPxjIQeM6ToAXMYCFOSMFJIPC85yg1zIhL7/voGDIvAGfp2tsGgVGmoDOmbNNDC54CvsXHknn+mAR68vAFMEYiZGJU9KTsKeA/BhGtRfGFOH8mN68QR+opF9dBG4rGuWrwAlerFBLDmxmqtxERyEmAdMzLpKTNwSgLcPMSwuPBzYN6qoWTCehqkIpxes6wUVkQOJc0yvznhOO4omCM1EpwMqY9jKnAtyWwJWGgJQvc9PZcD4fApZTjKIDAouiFATAWOenUyZTEIvouwDIbpJB1kJ9pScIM61h/GE3bPBRgHMzDCCFmdHfduenlM2ndI7NJKCk/FWdFpweHZGFJ7ixcAKW8ZQvA+bxR852EmTGJVFri0EqLKYLZOFu4hTp7GsixYl4MlkIZzi9DHD0KHwdIi0nBqeUYaYEo7mxbIQYXFWNARjgS+TCCgie5iHzia3M35oPb6dqCwE1WEk4JGomFthKMycNoGIYTwBI+biiGZ8wLuVhNFlq0qFlAzkiw4gzKC7AEwCW8IBF64nSitj8lYbw1aY41wQLTcYbNF2NhLlXG7DE2mGxFTIPHIyMXWqA92m29r/IT27ZoWnCS1T8bM+cMpegch8WIUyCR51xxIsCI4OFweV6UJJ0mZmD4kzFlGAm5SUiMh4w4TBrFrQlIiUkEnQy84zKPo0FiZ0KUZAD4JpTkRY0AsskfjOIjXYaXhDqbE5ld9hR0CL/+nfwE3FPHO7Y+YB7dJB28/BiYxaDB+t7TDARzGLJI0kdXk/Qo5TtT8S9GXn/f1XGv9ukiGeEVf+gYN/1VMsbLQdAR+l5l/HPMleA1/v0kb+jnbULe5u3XkkXPeGXejQMkr3nwFjBI4CP9O/gx6EUwgMWHgrOjXPE33c706SKZQpdef9fz2NnVzK+z19/XftV6/oFFktksuqdXXw9fSnuYrSYGUb9nu93SvbuYopZXpF+fa8HNjVIncKuQeYM7Q/Bq8Qm+ExCfQnrQsFNN7Ct1Dve5WjOpFfLiJUsrli1njFUuq1rxqmyOmvgKwiIGTfXKGtaKVbWrTdP8vbPxecl1Zs3adawtr71uluQdg1q9nZTXb2D/k+UbqyWT6PccT8T+F/Xq6pknGt4wza+39xoCvTS9tawdKjeZGD33PjKA28c4b25+q5VfVzVufnudprhYlsxEfocRW7Y22e1N27YzJ/V1SpO9qXnFDjquqTZVcCc3o87uUFRFad61m2nsWa+dcDTvraTRWtlMA5fSjPvsiirTMUnsZ5yWZhrzE45ttBEcMNOp7/IJ31Nkp7zyKiOqNL2LirU0ft88QfkgzXfI4ZpQdTTQicN8Ac4VfEDjdUdME1S5Cz9U3AtQuEVHVMm9ggN04iPnAo4u+dg7Yz9ZuvQdWZuhnk+nCiuoo4xwyMIKeOTWqtrxsU8Z+8wbRfkwTbL/EzpoYZcKHqd7qkhuNMH92l/s+pwOP1W9ceQJRqyWnQdfqJK8qYrVf7mVLm2mEydpKV81sHXH+Zq4S1cpml+4OyoVbwQ/4pEgk+DXjDikStVvMuLUJ5LaoN0yeTXXYcdl+RvGOaoZy5PyHwYF5WMHKtar79FaaxUSrGXEjp3kY41v5dNaWqgyXeCWqWQxZ5P8xkeycor/iWrQOPqn+hWOtxk7o9LoEON8p65kBL+bW7SN7TRd0DiydwfjfLN6Ofta2cdYhcOg4Fltgn2Ufu8uqTp3xDnvh9/Xa+Vv51JniVh9nnGWLW5gGtveXcXY7irGlpw5UbvbyOZKN56z6uKvxSSosfl9RqxtdNWKj/9JPw8c3OMcf6O0CC1BnQFBZTdzs0XhghrnKnYc2C62Gfsb2OfcPKeg6tj5r3PMyR4j91D+orHm0MYKWuax7+0OEtQDCaoOewVZro0qjESp0mynSkeuOtzsUBTdgrIsK/9m7ALfChuPGsoLVVXUXTzpzsqyql9Qknkkn2g6c3693XDuy1sZseHEGv2CassPjNjocJBjZMOC8nLGWVdVyVycammt3nJUuPjjBmf+qLIqG9ajG7mZtWFJ8wlxuPxk85fsUtadVL0sg9WNbQVVSm43VbJS0UbvW8GbBk20H/uB0krkoCpdEAz8j/TTjtZd+KmDa/i+5q2iw960+OPzghFbaLKNrtl/5t2Hm+PnX29uauatlteKKu8C7WKctNBshz9nnO0tkvy96OC9FJtcrkNQGiqi4Lm9NGH1krq3Vpymi7RtC2zjsWkCrfNwVd1PrsX8/OmliS+ZLkgsf63uRMuZMys/o7TrBEGR/wuaL1jPRD4wS7DVnrpHHFQykU1mCX7NBE6Jg31MZI1Zgh8ygQvi4HUxEc8pZgluY24OnBWaqBq72G0dUiRzkB1vMhe1zYKDVzpOtypdZgkqx9whs1U56XJjY7PqcJt4SlElk1AdF5zxv2GvQ1X2VjoVqhVZUX906lXwR3KTkNXmbd/VNNQ0nt/loHbFcfb8azXL3v7FTgpUpL96q2Z7TdUvDm6geYpUiZua7Ly40oC6Vm0ga/52Xmqr91/J+SF/sBcvugAAAABJRU5ErkJggg==\" />
        </div>
        <div class=\"text-exception\">
            <div class=\"open-quote\">
                <img alt=\"\" src=\"data:image/gif;base64,R0lGODlhHAAWAMQQANra2+bl5s3Mzevr6/Pz8+jo6O3t7fHx8c/Oz+Pj49PS093d3djX2NXV1eDf4MrJyvb29gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABAALAAAAAAcABYAQAWWICSOEDE4AamqRuAsT5yu6hA/wNrcfNysjl5PBOAJAAUDDRLoNRKDndAHnN6k058qaH2QuNelqCAYIm45MfGmIJCkAvUIPNB1td/uAyvEz/UqB0VUagQOZTEjgzx+Kk1CEAU8DAdqB4gPCHVjNwhucphKbzefamAFdlaNEGBZd1V3r1t6fE6wqrJ5XS4Ovb69MyQnv8QhADs=\" />
            </div>

            <h1>
                ";
        // line 12
        echo $this->env->getExtension('code')->formatFileFromText(nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message"), "html", null, true)));
        echo "
            </h1>

            <div>
                <strong>";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "</strong> ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo " - ";
        echo $this->env->getExtension('code')->abbrClass($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "class"));
        echo "
            </div>

            ";
        // line 19
        $context["previous_count"] = twig_length_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "allPrevious"));
        // line 20
        echo "            ";
        if ((isset($context["previous_count"]) ? $context["previous_count"] : $this->getContext($context, "previous_count"))) {
            // line 21
            echo "                <div class=\"linked\"><span><strong>";
            echo twig_escape_filter($this->env, (isset($context["previous_count"]) ? $context["previous_count"] : $this->getContext($context, "previous_count")), "html", null, true);
            echo "</strong> linked Exception";
            echo ((((isset($context["previous_count"]) ? $context["previous_count"] : $this->getContext($context, "previous_count")) > 1)) ? ("s") : (""));
            echo ":</span>
                    <ul>
                        ";
            // line 23
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "allPrevious"));
            foreach ($context['_seq'] as $context["i"] => $context["previous"]) {
                // line 24
                echo "                            <li>
                                ";
                // line 25
                echo $this->env->getExtension('code')->abbrClass($this->getAttribute((isset($context["previous"]) ? $context["previous"] : $this->getContext($context, "previous")), "class"));
                echo " <a href=\"#traces-link-";
                echo twig_escape_filter($this->env, ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1), "html", null, true);
                echo "\" onclick=\"toggle('traces-";
                echo twig_escape_filter($this->env, ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1), "html", null, true);
                echo "', 'traces'); switchIcons('icon-traces-";
                echo twig_escape_filter($this->env, ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1), "html", null, true);
                echo "-open', 'icon-traces-";
                echo twig_escape_filter($this->env, ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1), "html", null, true);
                echo "-close');\">&#187;</a>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['previous'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                    </ul>
                </div>
            ";
        }
        // line 31
        echo "
            <div class=\"close-quote\">
                <img alt=\"\" src=\"data:image/gif;base64,R0lGODlhHAAWAMQQANra2+bl5s3Mzevr6/Pz8+jo6O3t7fHx8c/Oz+Pj49PS093d3djX2NXV1eDf4MrJyvb29gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABAALAAAAAAcABYAQAWZoCOO5ACdaECuweO+sIOiDWw36IC8wjH/kAMDVoDYbLJf7ejC/QqvJHBGeC0fAgdhOrsCfDNmFHg9lo9SmvhxRpLXTpSBx6XuXNBjoN4GoNYPaSdtVoCCEIRNhm9iiS6Hjo6BjExxOWN1KAJNQAAvJpkQLS4LVAovfqGeLggQAwlne1MGBQCbqCc2AkV8bigOAQahKQ4DW0AhADs=\" />
            </div>
        </div>
    </div>
</div>

";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray"));
        foreach ($context['_seq'] as $context["position"] => $context["e"]) {
            // line 40
            echo "    ";
            $this->env->loadTemplate("TwigBundle:Exception:traces.html.twig")->display(array("exception" => (isset($context["e"]) ? $context["e"] : $this->getContext($context, "e")), "position" => (isset($context["position"]) ? $context["position"] : $this->getContext($context, "position")), "count" => (isset($context["previous_count"]) ? $context["previous_count"] : $this->getContext($context, "previous_count"))));
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['position'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "
";
        // line 43
        if ((isset($context["logger"]) ? $context["logger"] : $this->getContext($context, "logger"))) {
            // line 44
            echo "    <div class=\"block\">
        <div class=\"logs clear-fix\">
            ";
            // line 46
            ob_start();
            // line 47
            echo "            <h2>
                Logs&nbsp;
                <a href=\"#\" onclick=\"toggle('logs'); switchIcons('icon-logs-open', 'icon-logs-close'); return false;\">
                    <img class=\"toggle\" id=\"icon-logs-open\" alt=\"+\" src=\"data:image/gif;base64,R0lGODlhEgASAMQTANft99/v+Ga44bHb8ITG52S44dXs9+z1+uPx+YvK6WC24G+944/M6W28443L6dnu+Ge54v/+/l614P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABMALAAAAAASABIAQAVS4DQBTiOd6LkwgJgeUSzHSDoNaZ4PU6FLgYBA5/vFID/DbylRGiNIZu74I0h1hNsVxbNuUV4d9SsZM2EzWe1qThVzwWFOAFCQFa1RQq6DJB4iIQA7\" style=\"display: none\" />
                    <img class=\"toggle\" id=\"icon-logs-close\" alt=\"-\" src=\"data:image/gif;base64,R0lGODlhEgASAMQSANft94TG57Hb8GS44ez1+mC24IvK6ePx+Wa44dXs92+942e54o3L6W2844/M6dnu+P/+/l614P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABIALAAAAAASABIAQAVCoCQBTBOd6Kk4gJhGBCTPxysJb44K0qD/ER/wlxjmisZkMqBEBW5NHrMZmVKvv9hMVsO+hE0EoNAstEYGxG9heIhCADs=\" style=\"display: inline\" />
                </a>
            </h2>
            ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 55
            echo "
            ";
            // line 56
            if ($this->getAttribute((isset($context["logger"]) ? $context["logger"] : $this->getContext($context, "logger")), "counterrors")) {
                // line 57
                echo "                <div class=\"error-count\">
                    <span>
                        ";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["logger"]) ? $context["logger"] : $this->getContext($context, "logger")), "counterrors"), "html", null, true);
                echo " error";
                echo ((($this->getAttribute((isset($context["logger"]) ? $context["logger"] : $this->getContext($context, "logger")), "counterrors") > 1)) ? ("s") : (""));
                echo "
                    </span>
                </div>
            ";
            }
            // line 63
            echo "        </div>

        <div id=\"logs\">
            ";
            // line 66
            $this->env->loadTemplate("TwigBundle:Exception:logs.html.twig")->display(array("logs" => $this->getAttribute((isset($context["logger"]) ? $context["logger"] : $this->getContext($context, "logger")), "logs")));
            // line 67
            echo "        </div>
    </div>
";
        }
        // line 70
        echo "
";
        // line 71
        if ((isset($context["currentContent"]) ? $context["currentContent"] : $this->getContext($context, "currentContent"))) {
            // line 72
            echo "    <div class=\"block\">
        ";
            // line 73
            ob_start();
            // line 74
            echo "        <h2>
            Content of the Output&nbsp;
            <a href=\"#\" onclick=\"toggle('output-content'); switchIcons('icon-content-open', 'icon-content-close'); return false;\">
                <img class=\"toggle\" id=\"icon-content-close\" alt=\"-\" src=\"data:image/gif;base64,R0lGODlhEgASAMQSANft94TG57Hb8GS44ez1+mC24IvK6ePx+Wa44dXs92+942e54o3L6W2844/M6dnu+P/+/l614P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABIALAAAAAASABIAQAVCoCQBTBOd6Kk4gJhGBCTPxysJb44K0qD/ER/wlxjmisZkMqBEBW5NHrMZmVKvv9hMVsO+hE0EoNAstEYGxG9heIhCADs=\" style=\"display: none\" />
                <img class=\"toggle\" id=\"icon-content-open\" alt=\"+\" src=\"data:image/gif;base64,R0lGODlhEgASAMQTANft99/v+Ga44bHb8ITG52S44dXs9+z1+uPx+YvK6WC24G+944/M6W28443L6dnu+Ge54v/+/l614P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABMALAAAAAASABIAQAVS4DQBTiOd6LkwgJgeUSzHSDoNaZ4PU6FLgYBA5/vFID/DbylRGiNIZu74I0h1hNsVxbNuUV4d9SsZM2EzWe1qThVzwWFOAFCQFa1RQq6DJB4iIQA7\" style=\"display: inline\" />
            </a>
        </h2>
        ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 82
            echo "
        <div id=\"output-content\" style=\"display: none\">
            ";
            // line 84
            echo twig_escape_filter($this->env, (isset($context["currentContent"]) ? $context["currentContent"] : $this->getContext($context, "currentContent")), "html", null, true);
            echo "
        </div>

        <div style=\"clear: both\"></div>
    </div>
";
        }
        // line 90
        echo "
";
        // line 91
        $this->env->loadTemplate("TwigBundle:Exception:traces_text.html.twig")->display(array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        // line 92
        echo "
<script type=\"text/javascript\">//<![CDATA[
    function toggle(id, clazz) {
        var el = document.getElementById(id),
            current = el.style.display,
            i;

        if (clazz) {
            var tags = document.getElementsByTagName('*');
            for (i = tags.length - 1; i >= 0 ; i--) {
                if (tags[i].className === clazz) {
                    tags[i].style.display = 'none';
                }
            }
        }

        el.style.display = current === 'none' ? 'block' : 'none';
    }

    function switchIcons(id1, id2) {
        var icon1, icon2, display1, display2;

        icon1 = document.getElementById(id1);
        icon2 = document.getElementById(id2);

        display1 = icon1.style.display;
        display2 = icon2.style.display;

        icon1.style.display = display2;
        icon2.style.display = display1;
    }
//]]></script>
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 92,  199 => 91,  196 => 90,  187 => 84,  183 => 82,  173 => 74,  171 => 73,  158 => 67,  151 => 63,  142 => 59,  138 => 57,  136 => 56,  133 => 55,  121 => 46,  117 => 44,  112 => 42,  86 => 28,  62 => 23,  51 => 20,  49 => 19,  19 => 1,  93 => 9,  78 => 40,  46 => 10,  44 => 9,  27 => 4,  57 => 12,  91 => 31,  88 => 6,  63 => 18,  389 => 160,  386 => 159,  378 => 157,  371 => 156,  367 => 155,  363 => 153,  358 => 151,  353 => 149,  345 => 147,  343 => 146,  340 => 145,  334 => 141,  331 => 140,  328 => 139,  326 => 138,  321 => 135,  309 => 129,  307 => 128,  302 => 125,  296 => 121,  293 => 120,  290 => 119,  288 => 118,  283 => 115,  281 => 114,  276 => 111,  274 => 110,  269 => 107,  265 => 105,  259 => 103,  255 => 101,  253 => 100,  235 => 89,  232 => 88,  227 => 86,  222 => 83,  210 => 77,  208 => 76,  189 => 66,  184 => 63,  175 => 58,  170 => 56,  166 => 71,  163 => 70,  155 => 47,  152 => 46,  144 => 42,  127 => 35,  109 => 27,  94 => 21,  82 => 28,  76 => 25,  61 => 17,  39 => 16,  36 => 7,  79 => 18,  72 => 13,  69 => 25,  54 => 21,  47 => 8,  42 => 7,  40 => 8,  37 => 10,  22 => 1,  164 => 58,  157 => 56,  145 => 46,  139 => 45,  131 => 42,  120 => 31,  115 => 43,  111 => 38,  108 => 37,  106 => 36,  101 => 39,  98 => 40,  92 => 29,  83 => 25,  80 => 41,  74 => 14,  66 => 24,  60 => 6,  55 => 13,  52 => 12,  50 => 14,  41 => 8,  32 => 12,  29 => 6,  462 => 202,  453 => 199,  449 => 198,  446 => 197,  441 => 196,  439 => 195,  431 => 189,  429 => 188,  422 => 184,  415 => 180,  408 => 176,  401 => 172,  394 => 168,  387 => 164,  380 => 158,  373 => 156,  361 => 152,  355 => 150,  351 => 141,  348 => 140,  342 => 137,  338 => 135,  335 => 134,  329 => 131,  325 => 129,  323 => 128,  320 => 127,  315 => 131,  312 => 130,  303 => 122,  300 => 121,  298 => 120,  289 => 113,  286 => 112,  278 => 106,  275 => 105,  270 => 102,  267 => 101,  262 => 98,  256 => 96,  248 => 97,  246 => 96,  241 => 93,  233 => 87,  229 => 87,  226 => 84,  220 => 81,  216 => 79,  213 => 78,  207 => 75,  203 => 73,  200 => 72,  197 => 69,  194 => 68,  191 => 67,  185 => 66,  181 => 65,  178 => 59,  176 => 63,  172 => 57,  168 => 72,  165 => 60,  162 => 57,  156 => 66,  153 => 56,  150 => 55,  147 => 43,  141 => 51,  134 => 39,  130 => 46,  123 => 47,  119 => 40,  116 => 39,  113 => 38,  105 => 40,  102 => 24,  99 => 23,  96 => 31,  90 => 20,  84 => 24,  81 => 23,  73 => 24,  70 => 15,  67 => 20,  64 => 19,  59 => 14,  53 => 12,  45 => 9,  43 => 8,  38 => 6,  35 => 9,  33 => 4,  30 => 3,);
    }
}
