<?php

/* SolicitudesCitasBundle:DataSolicitudes:crear.html.twig */
class __TwigTemplate_9455c40a388f5eb4a44a23451da174bb7ba983a94568de4990ff307f36d07e63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"contendor\" style=\"padding: 0px !important\">
        <div class=\"block-separator col-md-12\"></div>
\t<h2 class=\"tit_principal\">Solicitud Licencia Centro Hípico</h2>
\t\t<br /><br /><br />
              <div class=\"col-md-12\">
\t\t<div id=\"texto\">Estimado Usuario, Para solicitar una cita, deber&aacute; antes adjuntar todos los recaudos, seg&uacute;n la licencia que solicite. Por cada Licencia deber&aacute; realizar una solicitud.</div>
\t\t<div id=\"texto\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios.</div>
\t\t<div style=\"clear:both;\"></div>
               </div>   
           <div class=\"col-md-12\">
                <div class=\"form-group\">
                  ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessage"]) {
            // line 15
            echo "                     <div class=\"alert alert-";
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
            echo " \">
                         <button class=\"close\" data-dismiss=\"alert\" type=\"button\"></button>
                         ";
            // line 17
            if ($this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : null), "title", array(), "any", true, true)) {
                // line 18
                echo "                         <strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "title"), "html", null, true);
                echo "</strong>
                         ";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "message"), "html", null, true);
                echo "
                         ";
            } else {
                // line 21
                echo "                         ";
                echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
                echo "
                         ";
            }
            // line 23
            echo "                     </div>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                </div>
            </div>
            <div class=\"col-md-12\">                
            ";
        // line 28
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "form_dsch")));
        echo "        
\t    <div id=\"accordion\" class=\"\">
\t\t<h3>1. Seleccione el Centro H&iacute;pico</h3>
\t\t<div>
\t\t    <div class=\"text-center col-lg-4\">Centro Hipico <span class=\"oblig\">(*)</span></div>
\t\t    ";
        // line 34
        echo "                    <div class=\"text-center col-md-4\">
                            ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centroHipico"), 'widget', array("attr" => array("class" => "centrohipico form-control"), "id" => "dch"));
        echo "
                            ";
        // line 36
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "centroHipico", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 37
            echo "                            <span class=\"help-block \">
                                ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centroHipico"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 41
        echo "                     </div>
\t\t</div>
\t\t<h3>2. Seleccionar el Tipo de Autorizaci&oacute;n</h3>
\t\t<div>
                        <div class=\"col-md-3\">Tipo de Autorizaci&oacute;n <span class=\"oblig\">(*)</span></div>
                        <div class=\"col-md-4\">
                            ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ClasLicencia"), 'widget', array("attr" => array("class" => "licencia form-control"), "id" => "alic"));
        echo "
                            ";
        // line 48
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ClasLicencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 49
            echo "                            <span class=\"help-block \">
                                ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ClasLicencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 52
        echo "                                
                        </div>
                        <div class=\"col-md-4\" id=\"operadora_establecimiento\" >                            
                        </div>                        
\t\t</div>

\t\t<h3>3. Seleccione los Juegos a Explotar</h3>
\t\t<div>
                        <div class=\"col-md-3\">Juegos a Explotar <span class=\"oblig\">(*)</span>: </div>
                        <div id=\"juegos_explotar\" class=\"col-md-7\"><p>Para ver Lista: Seleccione Tipo de Autorización y Centro hípico</p></div>
\t\t</div>

\t\t<h3>4. Adjuntar Recaudos</h3>
\t\t<div id=\"lista_recaudos\">
                      <p>Para ver: Seleccione Tipo de Autorización y Centro hipico</p>
\t\t</div>
\t\t<h3>5. Pago por Procesamiento</h3>
                  <div id=\"pago_procesamiento\">
\t\t    <p>Para ver: Seleccione Tipo de Autorización y Centro hipico</p>
\t\t</div>

\t\t<h3>6. Seleccionar D&iacute;a de Cita</h3>
              <div>
                  <div class=\"col-md-3\"><label for=\"date\">Fecha de Cita <span class=\"oblig\">(*)</span>:</label> <br/>
                   <input type=\"text\" id=\"date\" name=\"datacita\" placeholder=\"Fecha dd/mm/aaaa\" class=\"\" size=\"13px;\"/> <div id=\"error\"></div>
                  </div>
                 <div class=\"col-md-4 \"><div id=\"datepick\"></div> </div>
                 <div class=\"col-md-4 col-offset-2 alert-warning\">
                     Debe seleccionar alguno de los Días Activos. A partir del Día de mañana hasta 15 días para seleccionar la fecha de la cita
                 </div>
              </div><!--  FIN TABS 4 -->
        </div>
        <div class=\"block-separator col-md-12\"></div>
        <div class=\"col-md-4 col-md-offset-8 text-right\">
                 <a href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("solicitudes_list");
        echo "\" class=\"btn btn-warning btn-sm\">Cancelar</a>
                 <a href=\"#\" id=\"gensol\" class=\"btn btn-primary btn-sm\">Generar Solicitud</a>
        </div>        
         ";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token"), 'widget');
        echo "
       </form>  <!-- End form-->
      </div>                 
        <div class=\"block-separator col-md-12\"></div> 
</div>      
<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12 alert text-left\" id=\"myMessage\">
                        
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-primary btn-sm\" data-dismiss=\"modal\">Aceptar</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 116
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 117
        echo "        ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "

 <script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/js/solfun.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\">
        \$(document).ready(function() {
                ActiveCalendar();
                \$( \"#accordion\" ).accordion({
\t\t    heightStyle: \"content\"
\t\t});\t\t
                \$(\"#gensol\").on(\"click\",function(){
                    GenerarSolicitud();
                });
               
                //Se carga las operadoras afiliadas
                \$(\"#alic\").change(function(){
                    var centro=\$( \"#dch option:selected\" ).val();
                    if(centro == \"\") {
                        \$(\"#alic\").val(\"\");
                        \$(\"#myMessage\").html(\"Debe seleccionar Centro Hípico.\");
                        \$(\"#myModal\").modal(\"show\");
                    } else {
                        var id=\$(\"#alic option:selected\").val();                    
                        if(id != \"\") {
                            var url = Routing.generate('datasolicitudes_operadora_establecimiento',{ 'id': id, 'idCentro': centro });
                            \$(\"#operadora_establecimiento\").html(\"Cargando ...\");
                            \$.ajax({
                                async: false,
                                type: \"POST\",
                                cache: false,
                                url: url,
                                //data: \$(\"#form-info\").serialize(),
                                dataType: \"json\",
                                success: function(response) {
                                    if(response.status == 'true') {
                                        if(response.mensaje != \"\") {
                                            \$(\"#operadora_establecimiento\").html(response.mensaje);
                                        }
                                        // Juegos a Explotar
                                        getRoute(\"#juegos_explotar\",'datasolicitudes_juegoslist',id);
                                        //Recaudos
                                        getRoute(\"#lista_recaudos\",'datasolicitudes_recaudoslist',id);
                                    } else {
                                        \$(\"#myMessage\").html(response.mensaje);                                        
                                        \$(\"#myModal\").modal(\"show\");
                                        
                                        \$(\"#alic\").val(\"\");
                                        \$(\"#operadora_establecimiento\").html(\"\");
                                        \$(\"#juegos_explotar\").html(\"<p>Para ver Lista: Seleccione Tipo de Licencia</p>\");
                                        \$(\"#lista_recaudos\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                                        \$(\"#pago_procesamiento\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                                    }
                                },
                                error: function(){
                                    \$(\"#myMessage\").html(\"Error al cargar los datos. Por favor vuelva a intentar más tarde.\");
                                    \$(\"#myModal\").modal(\"show\");
                                    
                                    \$(\"#alic\").val(\"\");
                                    \$(\"#operadora_establecimiento\").html(\"\");
                                    \$(\"#juegos_explotar\").html(\"<p>Para ver Lista: Seleccione Tipo de Licencia</p>\");
                                    \$(\"#lista_recaudos\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                                    \$(\"#pago_procesamiento\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                                }
                            });                        
                        } else {
                            \$(\"#operadora_establecimiento\").html(\"\");
                            \$(\"#juegos_explotar\").html(\"<p>Para ver Lista: Seleccione Tipo de Licencia</p>\");
                            \$(\"#lista_recaudos\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                            \$(\"#pago_procesamiento\").html(\"<p>Para ver: Seleccione Tipo de Licencia </p>\");
                        }
                    }
                });
       });

function ValidaForm(){
    var resp=1,msg=new Array();
    // Validación de Cita
    var arrdate=\$( \"#date\" ).val().split(\"/\");
    var datets = Date.parse(arrdate[2]+\"-\"+arrdate[1]+\"-\"+arrdate[0]);
    if(isNaN(datets)) {
        msg.push(\"- Debe Seleccionar una Fecha para la Cita<br>\"); 
        resp=0;
    }
    \$(\"input:file\").each(function(index,elem){
       if (\$(this).val()===''){resp=0;msg.push(\"- Faltan Documentos de Recaudos<br/>\");return false;}
    });
    \$(\".date\").each(function(index,elem){
       if (!\$(this).hasClass('disabled')){
          if(\$(this).val()===''){
            resp=0;
            msg.push(\"- Falta Asignar Fechas de Recaudos<br/>\");
            return false;
          }
      }
    });

    if(\$('#juegoe').length){
        if(\$(\"#juegoe option:selected\").val()==='') {resp=0;msg.push(\"- Debe seleccionar los Juegos<br/>\");}
    }
    if(\$(\"#dch option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione el Centro Hípico<br/>\");}
    if(\$(\"#alic option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione una Licencia<br/>\");}
    if(\$(\"#banco option:selected\").val()==='') {resp=0; msg.push(\"- Seleccione un Banco<br/>\");}
    if(\$(\"#reciboNP\").val()==='' || parseInt(\$(\"#reciboNP\").val())===0) {resp=0;msg.push(\"- Falta el Número de Recibo de Pago<br/>\");}
    
    \$(\"#myMessage\").html(\"Aun Faltan Campos Por Rellenar en la Solicitud:<br/>\"+msg.toString().replace(',', '').replace(',', ''));
    \$(\"#myModal\").modal(\"show\");
    return resp===1?true:false;  
}       
</script>          
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:crear.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 119,  210 => 117,  207 => 116,  176 => 89,  170 => 86,  134 => 52,  128 => 50,  125 => 49,  123 => 48,  119 => 47,  111 => 41,  105 => 38,  102 => 37,  100 => 36,  96 => 35,  93 => 34,  85 => 28,  80 => 25,  73 => 23,  67 => 21,  62 => 19,  57 => 18,  55 => 17,  49 => 15,  45 => 14,  32 => 3,  29 => 2,);
    }
}
