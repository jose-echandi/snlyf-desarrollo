<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_pagosP_ed.html.twig */
class __TwigTemplate_61bda2f9103d7ee5fe0f47b2705cc34ec059591455b9cf5808f0d1c7de0633b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>Tipo de Documento</td>
                 <td>Archivo<span class=\"oblig\">(*)</span></td>
                 <td>Fecha de Pago<span class=\"oblig\">(*)</span></td>
                 <td>N de Recibo<span class=\"oblig\">(*)</span></td>
                 <td>Banco<span class=\"oblig\">(*)</span></td>
                 <td>Monto a Pagar<span class=\"oblig\">(*)</span></td>
                 <td></td>
        </tr>
         <tr>
            <td class=\"text-left\">Pago por Procesamiento</td>
            <td class=\"text-center\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pago"]) ? $context["pago"] : $this->getContext($context, "pago")), "archivoAdjunto"), "name"), "html", null, true);
        echo " </td>
            <td class=\"text-center\">";
        // line 14
        echo twig_escape_filter($this->env, _twig_default_filter(twig_date_format_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : $this->getContext($context, "pago")), "fechaDeposito"), "d/m/Y"), "none"), "html", null, true);
        echo "</td>
            <td class=\"text-center\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : $this->getContext($context, "pago")), "numReferencia"), "html", null, true);
        echo "</td>
            <td class=\"text-center\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pago"]) ? $context["pago"] : $this->getContext($context, "pago")), "banco"), "banco"), "html", null, true);
        echo "</td>
            <td class=\"text-center\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : $this->getContext($context, "pago")), "monto"), "html", null, true);
        echo "</td>
            <td class=\"text-center\">
                ";
        // line 20
        echo "                <button class=\"btn btn-warning\" onclick=\"CargaRecaudo('";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : $this->getContext($context, "pago")), "id"), "html", null, true);
        echo "','recaudospago_edit','pagoP');\">Cambiar</button>
            </td>
        </tr>
</table>
            
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_pagosP_ed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 20,  49 => 17,  45 => 16,  41 => 15,  37 => 14,  91 => 22,  77 => 21,  75 => 20,  69 => 17,  64 => 15,  60 => 14,  56 => 13,  52 => 12,  31 => 10,  42 => 7,  36 => 4,  33 => 13,  25 => 2,  19 => 1,  300 => 175,  287 => 165,  260 => 141,  254 => 139,  251 => 138,  223 => 114,  219 => 113,  216 => 112,  211 => 107,  197 => 96,  186 => 87,  182 => 85,  170 => 76,  156 => 64,  150 => 62,  147 => 61,  145 => 60,  140 => 59,  134 => 55,  121 => 44,  115 => 42,  112 => 41,  110 => 40,  106 => 39,  100 => 36,  97 => 35,  89 => 29,  86 => 28,  79 => 26,  73 => 24,  68 => 22,  63 => 21,  61 => 20,  55 => 18,  51 => 17,  46 => 15,  32 => 3,  29 => 9,);
    }
}
