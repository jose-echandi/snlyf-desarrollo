<?php

/* CentrohipicoBundle:SolicitudAfiliacion:paso2.html.twig */
class __TwigTemplate_482005441490a3d84d809f542284536f767357ca10975b5b82771a5903c1b5b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"page-header\">
        <h1>Solicitar Incorporaci&oacute;n Afiliado</h1>
    </div>

    <div class=\"padding-15\" id=\"contenido\">

        <div id=\"texto\"
                >Estimada Operadora, <br>
            Al agregar la incorporación de un nuevo afiliado, deberá adjuntar el contrato de Afiliado-Operadora, y esperar un lapso no mayor de 48 horas para la aprobación de la SUNAHIP.<br><br>
        </div>
        <div id=\"texto\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios</div>

        ";
        // line 16
        if (((isset($context["status"]) ? $context["status"] : $this->getContext($context, "status")) == true)) {
            // line 17
            echo "        <table id=\"tabla_reporte2\">
            <tbody><tr id=\"table_header2\">
                <td colspan=\"4\">Nuevo Afiliado</td>
            </tr>
            <tr>
                <td>Fecha de Solicitud:</td>
                <td colspan=\"3\">";
            // line 23
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "m/d/Y"), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <td>RIF Afiliado:</td>
                <td colspan=\"3\">";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rif"), "html", null, true);
            echo "</td>

            </tr>
            <tr>
                <td>Denominación Comercial:</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "denominacionComercial"), "html", null, true);
            echo "</td>
                <td>Clasificación del Establecimiento:</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clasificacionLocal"), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <td style=\"width:25%;\">Estado:</td>
                <td style=\"width:25%;\">";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "estado"), "nombre"), "html", null, true);
            echo "</td>
                <td style=\"width:25%;\">Ciudad:</td>
                <td style=\"width:25%;\">";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "ciudad"), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <td style=\"width:25%;\">Municipio:</td>
                <td style=\"width:25%;\">";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "municipio"), "nombre"), "html", null, true);
            echo "</td>
                <td style=\"width:25%;\">Urbanización/Sector:</td>
                <td style=\"width:25%;\">";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "urbanSector"), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <td style=\"width:25%;\">Avenida/Calle/Carrera:</td>
                <td style=\"width:25%;\">";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "avCalleCarrera"), "html", null, true);
            echo "</td>
                <td style=\"width:25%;\">Edificio/Casa:</td>
                <td style=\"width:25%;\">";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "edifCasa"), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <td style=\"width:25%;\">Oficina/Apto/No.:</td>
                <td style=\"width:25%;\">";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "ofcAptoNum"), "html", null, true);
            echo "</td>
                <td style=\"width:25%;\">Punto de Referencia:</td>
                <td style=\"width:25%;\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "puntoReferencia"), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <td>Licencia:</td>
                <td colspan=\"3\">Autorización</td>
            </tr>
            ";
            // line 64
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal", "id" => "news_add")));
            echo "
            <tr>
                <td>Autorizaciones Activas <br> Del Operador:</td>
                <td colspan=\"3\">
                    <select required=\"true\" name=\"sunahip_centrohipicobundle_data_operadora_establecimiento[id_s]\" 
                            style=\"width: 220px; padding: 1px;\" onchange=\"isValid(this)\">
                        <option value=\"\">Seleccione</option>
                        ";
            // line 71
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["formLicencias"]) ? $context["formLicencias"] : $this->getContext($context, "formLicencias")));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                // line 72
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : $this->getContext($context, "lic")), "id"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : $this->getContext($context, "lic")), "clasif_licencia"), "html", null, true);
                echo "</option>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "                    </select>
                    <span class=\"oblig\">(*)</span>
                </td>
            </tr>
            <input type=\"hidden\" id=\"sunahip_centrohipicobundle_data_operadora_establecimiento_id\" 
                   name=\"sunahip_centrohipicobundle_data_operadora_establecimiento[id]\" value=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\">
            <input type=\"hidden\" id=\"sunahip_centrohipicobundle_data_operadora_establecimiento_id_operadora\"  
                   name=\"sunahip_centrohipicobundle_data_operadora_establecimiento[id_operadora]\" value=\"";
            // line 81
            echo twig_escape_filter($this->env, (isset($context["operadora"]) ? $context["operadora"] : $this->getContext($context, "operadora")), "html", null, true);
            echo "\">
            <tr>
                <td>Adjunte el contrato firmado con el Afiliado:</td>
                <td colspan=\"3\">";
            // line 84
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contratoFirmado"), 'row');
            echo "<span class=\"oblig\">(*) Solo archivos .PDF</span>
                </td>
            </tr>
            <tr>
                <td>Adjunte la Buena Pro:</td>
                <td colspan=\"3\">";
            // line 89
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "buenaPro"), 'row');
            echo "<span class=\"oblig\">(*) Solo archivos .PDF</span>
                </td>
            </tr>
            <tr>
                <td>Condiciones:</td>
                <td colspan=\"3\"><input type=\"checkbox\" required=\"true\">Acepto las condiciones de SUNAHIP<span class=\"oblig\">(*)</span>
                </td>
            </tr>
            </tbody></table>
        <div class=\"block-separator col-md-12\"></div>
        <div class=\"col-md-offset-8 col-md-6 form-group text-right\">
            <a href=\"";
            // line 100
            echo $this->env->getExtension('routing')->getPath("notification");
            echo "\" class=\"btn btn-warning btn-sm\">Cancelar</a>
            &nbsp; &nbsp; &nbsp; &nbsp;
            <input type=\"submit\" value=\"Solicitar Incorporación\" class=\"btn btn-primary btn-sm\" id=\"form_btttn\" style=\"height: auto;margin: 10px;padding: 5px;\">        
        </div>
        ";
            // line 104
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
            echo "
        ";
        } else {
            // line 106
            echo "        <div class=\"block-separator col-md-12\"></div>
        <div id=\"texto\" style=\"color: red\">
            ";
            // line 108
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "
        </div>
        <div class=\"col-md-offset-8 col-md-6 form-group text-right\">
            <a href=\"";
            // line 111
            echo $this->env->getExtension('routing')->getPath("notification");
            echo "\" class=\"btn btn-warning btn-sm\">Aceptar</a>        
        </div>
        ";
        }
        // line 114
        echo "<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
            </div>
            <div class=\"modal-body\" id=\"myMessage\">

            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-primary btn-sm\" data-dismiss=\"modal\">Aceptar</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 133
    public function block_script_base($context, array $blocks = array())
    {
        // line 134
        echo "    <script type=\"text/javascript\">
        \$('input[type=file]').on('change', function(e){
            var file = \$(this).val();
            if(file.split('.').pop().toLowerCase() != 'pdf'){
                alert('Solo PDF');
                \$(this).val('');
            }
        });
        function isValid(input) {
            value = input.options[input.selectedIndex].value;
            if(value != \"\") {
                var centroHipico = \$(\"#sunahip_centrohipicobundle_data_operadora_establecimiento_id\").val();
                var operadora = \$(\"#sunahip_centrohipicobundle_data_operadora_establecimiento_id_operadora\").val();
                var url = Routing.generate('Centrohipico_solicitud_afiliacion_valid', {'centroHipico': centroHipico,'operadora':operadora,'clasificacion':value });
                \$(\"#form_btttn\").attr(\"disabled\", \"disabled\");
                \$.ajax({
                    async: false,
                    type: \"POST\",
                    cache: false,
                    url: url,
                    dataType: \"json\",
                    success: function(response) {
                        if(response.status == true) {
                            \$(\"#form_btttn\").removeAttr(\"disabled\");
                        } else {
                            \$(\"#myMessage\").html(response.message);
                            \$(\"#myModal\").modal(\"show\");
                            input.value = \"\";
                            \$(\"#form_btttn\").removeAttr(\"disabled\");
                        }
                    },
                    error: function(){
                        \$(\"#myMessage\").html(\"Error al procesar los datos.\");
                        \$(\"#myModal\").modal(\"show\");
                        input.value = \"\";
                        \$(\"#form_btttn\").removeAttr(\"disabled\");
                    }
                });   
            }
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:SolicitudAfiliacion:paso2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 134,  249 => 133,  228 => 114,  222 => 111,  216 => 108,  212 => 106,  207 => 104,  200 => 100,  186 => 89,  178 => 84,  172 => 81,  167 => 79,  160 => 74,  149 => 72,  145 => 71,  135 => 64,  126 => 58,  121 => 56,  114 => 52,  109 => 50,  102 => 46,  97 => 44,  90 => 40,  85 => 38,  78 => 34,  73 => 32,  63 => 27,  56 => 23,  48 => 17,  46 => 16,  32 => 4,  29 => 3,);
    }
}
