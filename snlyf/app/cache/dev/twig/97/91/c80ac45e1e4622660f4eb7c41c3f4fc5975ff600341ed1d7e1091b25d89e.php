<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_9791c80ac45e1e4622660f4eb7c41c3f4fc5975ff600341ed1d7e1091b25d89e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@WebProfiler/Profiler/layout.html.twig");

        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
    }

    // line 6
    public function block_menu($context, array $blocks = array())
    {
        // line 7
        echo "<span class=\"label\">
    <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAABDlBMVEU/Pz////////////////////////////////////////////////////////////////////+qqqr///////////+kpKT///////////////////////////////////+Kior///////////+Ghob///////9kZGT///////////////////////9bW1v///9aWlpZWVn////t7e3////m5ub///9cXFxZWVn////////////////////KysrNzc3///9tbW1WVlZTU1NwcHCnp6dgYGCBgYGZmZl3d3dLS0tMTEyNjY2Tk5NJSUlFRUVERERZWVlCQkJVVVVAQEBCQkJUVFRVVVU/Pz9ERER+LwjMAAAAWHRSTlMAAQIDBQYHCAkLDQ4VFhscHyAiIiMlJjAyNDY3ODk9P0BAREpMTlBdXl9rb3BzdHl6gICChIyPlaOmqKuusLm6v8HFzM3X2tzd4ePn6Onq8vb5+vv9/f3+EYS6xwAAAQFJREFUeNrN0dlSwkAQBdAbA2FTQIIsAmJEA5qIiIoim8oibigI0vz/jygFZEwIw4sP3reeOtVTdRt/G6kwHBYkDvC/EL0HOCBGP4lzwN4UHJGRrMMClOmrzsDH/oYNKBLLc0gA4MwvZtUK6MELiIeDxagvgY4MIdIzxqIVfF6F4WvSSjBpZHyQW6tBO7clIHjRNwO9dDdP5UQWAc9BfWICalSZZzfgBCBsHndNQIEl4o5Wna0s6UYZROcSO3IwMVsZVX9Xfe0CAF7VN+414N7PB68aH7xdxm2+YEXVzmJuLANWVHLbBXvAivqnID0iGqU5IPU0/npMckD49LasyTDlG31Ah7wRFiUBAAAAAElFTkSuQmCC\" alt=\"Routing\"></span>
    <strong>Routing</strong>
</span>
";
    }

    // line 13
    public function block_panel($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 16,  51 => 13,  34 => 5,  31 => 4,  806 => 488,  803 => 487,  792 => 485,  788 => 484,  784 => 482,  771 => 481,  745 => 475,  742 => 474,  723 => 472,  706 => 471,  702 => 469,  698 => 468,  694 => 467,  690 => 466,  686 => 465,  682 => 464,  678 => 463,  675 => 462,  673 => 461,  656 => 460,  645 => 458,  630 => 452,  625 => 450,  621 => 449,  618 => 448,  616 => 447,  602 => 445,  597 => 442,  563 => 410,  545 => 407,  528 => 406,  525 => 405,  523 => 404,  518 => 402,  513 => 400,  246 => 136,  202 => 94,  199 => 93,  196 => 92,  182 => 87,  176 => 86,  173 => 85,  170 => 84,  165 => 83,  163 => 82,  158 => 80,  145 => 74,  136 => 71,  123 => 61,  116 => 57,  109 => 52,  106 => 51,  98 => 45,  92 => 43,  90 => 42,  84 => 40,  68 => 30,  62 => 27,  50 => 22,  47 => 11,  28 => 3,  417 => 143,  411 => 140,  407 => 138,  405 => 137,  398 => 136,  395 => 135,  388 => 134,  384 => 132,  382 => 131,  377 => 129,  374 => 128,  371 => 127,  368 => 126,  365 => 125,  362 => 124,  359 => 123,  356 => 122,  353 => 121,  350 => 120,  347 => 119,  341 => 117,  338 => 116,  333 => 115,  328 => 113,  324 => 112,  315 => 111,  313 => 110,  308 => 109,  305 => 108,  293 => 107,  285 => 100,  281 => 98,  274 => 96,  262 => 93,  249 => 92,  237 => 91,  234 => 90,  232 => 89,  221 => 80,  213 => 78,  210 => 77,  207 => 76,  201 => 74,  186 => 72,  183 => 71,  180 => 70,  177 => 69,  172 => 68,  161 => 58,  159 => 57,  155 => 55,  152 => 54,  147 => 75,  141 => 73,  135 => 46,  133 => 45,  128 => 42,  120 => 38,  117 => 37,  114 => 36,  108 => 33,  101 => 30,  95 => 27,  78 => 19,  75 => 18,  72 => 17,  58 => 25,  44 => 10,  41 => 19,  38 => 7,  204 => 75,  191 => 77,  188 => 90,  185 => 75,  174 => 74,  171 => 73,  167 => 71,  164 => 70,  153 => 77,  138 => 47,  134 => 54,  125 => 41,  121 => 50,  118 => 49,  113 => 48,  111 => 47,  104 => 31,  102 => 41,  96 => 37,  87 => 41,  83 => 33,  64 => 23,  49 => 14,  46 => 13,  43 => 12,  35 => 6,  32 => 6,  27 => 3,  91 => 25,  88 => 24,  82 => 21,  80 => 32,  76 => 34,  73 => 33,  67 => 24,  63 => 18,  61 => 15,  55 => 24,  52 => 12,  45 => 9,  39 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
