<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_recaudos.html.twig */
class __TwigTemplate_8af316b5a7da097aaa39e4a1251605b3e05f3265b32de87a0daab1cd02de882a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>N#</td>
                 <td>Tipo de Documento</td>
                 <td>Buscar Archivo<span class=\"oblig\">(*)</span></td>
                 <td>Fecha de Vencimiento<span class=\"oblig\">(*)</span></td>
                 <td>Consignado<span class=\"oblig\">(*)</span></td>
        </tr>
        ";
        // line 9
        $context["last"] = 1;
        // line 10
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : $this->getContext($context, "list")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            echo "   
          <tr>
                 <td class=\"text-center\">";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "</td>
                 <td class=\"text-left\">";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "recaudo"), "html", null, true);
            echo "</td>
                 <td class=\"text-center\">";
            // line 14
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "mediarecaudo"), 'widget', array("attr" => array("class" => "mediarecaudo")));
            echo "
                                ";
            // line 15
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : null), "recaudoscargados", array(), "any", false, true), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array", false, true), "mediarecaudo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
                // line 16
                echo "                                <span class=\"help-block \">
                                    ";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "mediarecaudo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
                echo "
                                </span>
                                ";
            }
            // line 20
            echo "                  </td>
                 <td class=\"text-center \">";
            // line 21
            $context["disable"] = ((($this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "fechaVencimiento") == 0)) ? ("disabled") : ("readonly"));
            // line 22
            echo "                                ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "fechaVencimiento"), 'widget', array("attr" => array("class" => ("date fechavencimiento " . (isset($context["disable"]) ? $context["disable"] : $this->getContext($context, "disable"))))));
            echo "
                                ";
            // line 23
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : null), "recaudoscargados", array(), "any", false, true), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array", false, true), "fechaVencimiento", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
                // line 24
                echo "                                <span class=\"help-block \">
                                    ";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "fechaVencimiento"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
                echo "
                                </span>
                                ";
            }
            // line 28
            echo "                  </td>
                 
                 ";
            // line 31
            echo "                 ";
            // line 32
            echo "                 <td class=\"text-center\">
                     <input type=\"checkbox\" disabled=\"disabled\" />
                     ";
            // line 34
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "tipodoc"), 'widget', array("attr" => array("class" => "tipodoc"), "data" => "D"));
            echo "
                     ";
            // line 35
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "status"), 'widget', array("attr" => array("class" => "status")));
            echo "
                     ";
            // line 36
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["FDSolicitud"]) ? $context["FDSolicitud"] : $this->getContext($context, "FDSolicitud")), "recaudoscargados"), ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), array(), "array"), "reciboN"), 'widget', array("attr" => array("class" => "hide")));
            echo "
                     ";
            // line 38
            echo "                     <input type=\"hidden\" id=\"recaudos_";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), "html", null, true);
            echo "\" name=\"recaudoLicencia[";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index") - 1), "html", null, true);
            echo "]\" class=\"hide\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "id"), "html", null, true);
            echo "\">
                 </td>
        </tr>
          ";
            // line 41
            $context["last"] = $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "length");
            // line 42
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "</table>
<div id=\"pagoP\">
    ";
        // line 45
        $this->env->loadTemplate("SolicitudesCitasBundle:DataSolicitudes:List_pagosP.html.twig")->display($context);
        // line 46
        echo "</div>
<script type=\"text/javascript\">

    \$(\".mediarecaudo\").change(function(){
        console.info('valor', \$(this).val());
        comprueba_extension(\$(this).val(),\$(this));
    })

    function comprueba_extension(archivo, obj) {
        extensiones_permitidas = new Array(\".pdf\");
        mierror = \"\";
        if (!archivo) {
            //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
            mierror = \"No has seleccionado ningún archivo\";
        }else{
            //recupero la extensión de este nombre de archivo
            extension = (archivo.substring(archivo.lastIndexOf(\".\"))).toLowerCase();
            //alert (extension);
            //compruebo si la extensión está entre las permitidas
            permitida = false;
            for (var i = 0; i < extensiones_permitidas.length; i++) {
                if (extensiones_permitidas[i] == extension) {
                    permitida = true;
                    break;
                }
            }
            if (!permitida) {
                mierror = \"Comprueba la extensión de los archivos a subir. \\nSólo se pueden subir archivos con extensiones: \" + extensiones_permitidas.join();
                obj.val('');
                alert (mierror);
            }else{
//                alert (\"Todo correcto. Voy a submitir el formulario.\");
                //formulario.submit();
                return 1;
            }
        }
        //si estoy aqui es que no se ha podido submitir

        return 0;
    }
</script>
<script type=\"text/javascript\">
    \$(document).ready(function(){
        \$(\"#pago_procesamiento\").html(\$(\"#pagoP\").html());
        \$(\"#F_pagoP\").html(\$(\"#pagoP\").html());
        \$(\"#pagoP\").remove();
        \$(\".disabled\").prop('disabled',true);
        //\$(\".readonly\").prop('readonly',true);
        var Options={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            maxDate: '";
        // line 97
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "',
            onSelect: function(dateText, inst) { 
               var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
               //GenerarSolicitud(datets);
               \$(this).val(dateText);                
           } 
        };
        var Options2={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            minDate: '";
        // line 107
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, "now", "+30 day"), "d/m/Y"), "html", null, true);
        echo "',
            onSelect: function(dateText, inst) { 
               var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
               //GenerarSolicitud(datets);
               \$(this).val(dateText);                
           } 
        };
      \$(\".date\").each(function(index,elem){
          \$(this).datepicker(Options2);
          \$(this).attr('style','cursor: pointer !important');
      });
      \$(\".datePago\").each(function(index,elem){
          \$(this).datepicker(Options);
          \$(this).attr('style','cursor: pointer !important');
          //\$(this).prop('readonly',true);
      });      
    });
</script>
    
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_recaudos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 107,  204 => 97,  151 => 46,  149 => 45,  145 => 43,  131 => 42,  129 => 41,  118 => 38,  114 => 36,  110 => 35,  106 => 34,  102 => 32,  100 => 31,  96 => 28,  90 => 25,  87 => 24,  85 => 23,  80 => 22,  78 => 21,  75 => 20,  69 => 17,  66 => 16,  64 => 15,  60 => 14,  56 => 13,  52 => 12,  31 => 10,  29 => 9,  19 => 1,);
    }
}
