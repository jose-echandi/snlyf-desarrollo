<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_recaudos_ed.html.twig */
class __TwigTemplate_15c4696b497b396fd36e71dbe589db0ff8674090f56baa6385dac7a18fc5f734 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>N#</td>
                 <td>Tipo de Documento</td>
                 <td>Archivo<span class=\"oblig\">(*)</span></td>
                 <td>Fecha de Vencimiento<span class=\"oblig\">(*)</span></td>
                 <td> </td>
        </tr>
        ";
        // line 9
        $context["last"] = 1;
        // line 10
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "recaudoscargados"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            echo "   
          <tr>
                 <td class=\"text-center\">";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "</td>
                 <td class=\"text-left\">";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "recaudoLicencia"), "recaudo"), "html", null, true);
            echo "</td>
                 <td class=\"text-center\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "mediarecaudo"), "name"), "html", null, true);
            echo "</td>
                 <td class=\"text-center \">";
            // line 15
            echo twig_escape_filter($this->env, _twig_default_filter(twig_date_format_filter($this->env, $this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "fechaVencimiento"), "d/m/Y"), "none"), "html", null, true);
            echo "</td>
                 <td class=\"text-center\">
                     <button class=\"btn btn-warning\" onclick=\"CargaRecaudo('";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["row"]) ? $context["row"] : $this->getContext($context, "row")), "id"), "html", null, true);
            echo "','recaudoscargados_edit','recaudos');\">Cambiar</button>
                 </td>
        </tr>
          ";
            // line 20
            $context["last"] = $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "length");
            // line 21
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "</table>
    ";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_recaudos_ed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 22,  77 => 21,  75 => 20,  69 => 17,  64 => 15,  60 => 14,  56 => 13,  52 => 12,  31 => 10,  42 => 7,  36 => 4,  33 => 3,  25 => 2,  19 => 1,  300 => 175,  287 => 165,  260 => 141,  254 => 139,  251 => 138,  223 => 114,  219 => 113,  216 => 112,  211 => 107,  197 => 96,  186 => 87,  182 => 85,  170 => 76,  156 => 64,  150 => 62,  147 => 61,  145 => 60,  140 => 59,  134 => 55,  121 => 44,  115 => 42,  112 => 41,  110 => 40,  106 => 39,  100 => 36,  97 => 35,  89 => 29,  86 => 28,  79 => 26,  73 => 24,  68 => 22,  63 => 21,  61 => 20,  55 => 18,  51 => 17,  46 => 15,  32 => 3,  29 => 9,);
    }
}
