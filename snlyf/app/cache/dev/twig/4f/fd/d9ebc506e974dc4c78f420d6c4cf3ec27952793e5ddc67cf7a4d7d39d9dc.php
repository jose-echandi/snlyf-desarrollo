<?php

/* FOSUserBundle:Admin:listado.html.twig */
class __TwigTemplate_4ffdd9ebc506e974dc4c78f420d6c4cf3ec27952793e5ddc67cf7a4d7d39d9dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>Listado de Usuarios</h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
            <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("nuevo-usuario");
        echo "\" class=\"btn btn-primary btn-sm \"><i class=\"icon-plus-sign\"></i> Agregar usuario</a>
        </div>
        <div class=\"col-sm-2 block-separator\"></div>
    </div>

    <div class=\"col-md-7\" style=\"float:right\">
        <form action=\"\" method=\"get\" class=\"form-horizontal\">                
            <input type=\"search\" name=\"keyword\" style=\"float: left; margin-right: 10px; height: 32px\" id=\"txtKeyword\" aria-controls=\"dt_basic\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request"), "get", array(0 => "keyword"), "method"), "html", null, true);
        echo "\">
            <select class=\"form-control\" name=\"role\" style=\"height: 32px; margin-right: 10px; width: 160px !important; display: inline !important\">
                <option value=\"\">Todos</option>
                ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["roles"]) ? $context["roles"] : $this->getContext($context, "roles")));
        foreach ($context['_seq'] as $context["key"] => $context["role"]) {
            // line 23
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key")), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request"), "get", array(0 => "role"), "method") == (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key")))) {
                echo " selected ";
            }
            echo " >";
            echo twig_escape_filter($this->env, (isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['role'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "            </select>
            <input type=\"submit\" style=\"float:right; height: 32px\" class=\"btn btn-primary\" value=\"Buscar\">
        </form>
        <div class=\"col-sm-5 block-separator\"></div>
    </div>    
    <div id=\"action\">
        <div class=\"left\">
            ";
        // line 32
        if ((twig_length_filter($this->env, (isset($context["usuarios"]) ? $context["usuarios"] : $this->getContext($context, "usuarios"))) > 0)) {
            // line 33
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 34
            echo $this->env->getExtension('knp_pagination')->render((isset($context["usuarios"]) ? $context["usuarios"] : $this->getContext($context, "usuarios")));
            echo "
                </article>
            ";
        }
        // line 37
        echo "        </div>
    </div>

    <div class=\"col-md-12\">
        <table class=\"table records_list table-condensed table-striped table-hover text-center\">
            <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Nombre</th>
                    <th>Perfil</th>
                    <th>Correo</th>
                    <th>Activo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                ";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usuarios"]) ? $context["usuarios"] : $this->getContext($context, "usuarios")));
        foreach ($context['_seq'] as $context["_key"] => $context["usuario"]) {
            // line 54
            echo "                    <tr>
                        <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "username"));
            echo "</td>
                        <td>";
            // line 56
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getNombre", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getNombre"), "Sin datos")) : ("Sin datos")));
            echo " ";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getApellido", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil", array(), "any", false, true), 0, array(), "array", false, true), "getApellido"), "Sin datos")) : ("Sin datos")));
            echo "</td>
                        <td>
                            ";
            // line 58
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") != "")) {
                // line 59
                echo "                                ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_ADMIN")) {
                    // line 60
                    echo "                                    Administrador
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_CENTRO_HIPICO")) {
                    // line 62
                    echo "                                    Usuario Centro Hípico
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_OPERADOR")) {
                    // line 64
                    echo "                                    Usuario Operadora
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_GERENTE")) {
                    // line 66
                    echo "                                    Gerente
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_FISCAL")) {
                    // line 68
                    echo "                                    Fiscal
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_ASESOR")) {
                    // line 70
                    echo "                                    Asesor Legal
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_SUPERINTENDENTE")) {
                    // line 72
                    echo "                                    Superintendente
                                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "perfil"), 0, array(), "array"), "getRoleType") == "ROLE_COORDINADOR")) {
                    // line 74
                    echo "                                    Coordinador
                                ";
                } else {
                    // line 76
                    echo "                                ";
                }
                // line 77
                echo "                            ";
            } else {
                // line 78
                echo "                                Sin Datos
                            ";
            }
            // line 80
            echo "                        </td>
                        <td>";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "email"));
            echo "</td>
                        <td class=\"text-center\">";
            // line 82
            if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "enabled") == "1")) {
                echo "<i class=\"fa fa-check-circle-o\"></i>";
            } else {
                echo "<i class=\"fa fa-times-circle-o\"></i>";
            }
            echo "</td>
                        <td class=\"text-center\">
                            ";
            // line 84
            if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "enabled") == "1")) {
                // line 85
                echo "                                ";
                if ((($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "id") != $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "id")) && $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
                    // line 86
                    echo "                                    <a data-href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cambiar-estado-usuario", array("user_id" => $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "id"), "estado_id" => 0)), "html", null, true);
                    echo "\" data-toggle=\"modal\" data-target=\"#confirm-disable\" href=\"#\"><i class=\"fa fa-minus-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Desactivar\"></i></a>
                                ";
                }
                // line 88
                echo "                            ";
            } else {
                // line 89
                echo "                                <a data-href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cambiar-estado-usuario", array("user_id" => $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "id"), "estado_id" => 1)), "html", null, true);
                echo "\" data-toggle=\"modal\" data-target=\"#confirm-enable\" href=\"#\"><i class=\"fa fa-plus-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Activar\"></i></a>
                            ";
            }
            // line 91
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("editar-usuario", array("user_id" => $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "id"))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Editar\"></i></a>
                        </td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usuario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "            </tbody>
        </table>
    </div>
    <div class=\"block-separator col-sm-12\"></div>

    <div class=\"modal fade\" id=\"confirm-delete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Eliminar usuario
                </div>
                <div class=\"modal-body\">
                    Está seguro de eliminar el usuario?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" class=\"btn btn-danger danger\">Eliminar</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"confirm-enable\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Activar Usuario
                </div>
                <div class=\"modal-body\">
                    ¿Está seguro que desea activar el usuario?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" class=\"btn btn-danger danger\">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"confirm-disable\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Desactivar Usuario
                </div>
                <div class=\"modal-body\">
                    ¿Está seguro que desea desactivar el usuario?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" class=\"btn btn-danger danger\">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 149
    public function block_script_base($context, array $blocks = array())
    {
        // line 150
        echo "    ";
        $this->displayParentBlock("script_base", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        \$('#confirm-delete').on('show.bs.modal', function(e) {
            \$(this).find('.danger').attr('href', \$(e.relatedTarget).data('href'));
        });
        \$('#confirm-enable').on('show.bs.modal', function(e) {
            \$(this).find('.danger').attr('href', \$(e.relatedTarget).data('href'));
        });
        \$('#confirm-disable').on('show.bs.modal', function(e) {
            \$(this).find('.danger').attr('href', \$(e.relatedTarget).data('href'));
        });

    </script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Admin:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  287 => 150,  284 => 149,  227 => 95,  216 => 91,  210 => 89,  207 => 88,  201 => 86,  198 => 85,  196 => 84,  187 => 82,  183 => 81,  180 => 80,  176 => 78,  173 => 77,  170 => 76,  166 => 74,  162 => 72,  158 => 70,  154 => 68,  150 => 66,  146 => 64,  142 => 62,  138 => 60,  135 => 59,  133 => 58,  126 => 56,  122 => 55,  119 => 54,  115 => 53,  97 => 37,  91 => 34,  88 => 33,  86 => 32,  77 => 25,  62 => 23,  58 => 22,  52 => 19,  42 => 12,  32 => 4,  29 => 3,);
    }
}
